// Import styles
import './index.scss';

// Import jquery
import $ from 'jquery';

import Component, {findComponent} from '../../../globals/js/Libraries/component';

// Import breakpoints object
import media from 'globals/js/media';

// Import carousel script
import Slider from '../../../globals/js/Libraries/slider';

class ModelAccessTeaser extends Component {
  constructor(container) {
    super(container);
    const self = this;
    this.container = $(container);
    this.controls = this.container.find('.m245__controls');
    this.inner = this.container.find('.m245__carousel');
    this.dots = this.container.find('.m245__dots');
    this.initComponent();

    if (this.inner.children().length > 2) {
      this.slider = new Slider(this.inner, {
        appendDots: this.dots,
        appendArrows: this.controls,
        slidesToShow: 2,
        slidesToScroll: 2,
        responsive: [
          {
            breakpoint: media.breakpoints.laptop - 1,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 4,
            },
          },
        ],
      });
      $(() => {
        self.slider.mount();
      });
    }
  }
}

findComponent('.m245', ModelAccessTeaser);
