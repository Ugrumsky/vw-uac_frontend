/**
 * Created by Kodix on 01.02.2017.
 */

export function assert(condition, message = 'Assertion failed') {
  if (!condition && process.env.NODE_ENV !== 'production') {
    if (typeof Error !== 'undefined') {
      throw new Error(message);
    }
    throw message; // Fallback
  }
}

export function devlog(variable) {
  if (process.env.NODE_ENV === 'development') {
    console.log(variable);
  }
}
