import React from 'react';

const EventCardBorderSeparator = (props) => {
    return (
        <div className="eventCard__border-separator"></div>
    );
};


export default EventCardBorderSeparator;