import 'globals/dws_globals';
import './index.scss';


/*
  Обязательные копоненты
 */
import '../../../components/StageModules/m341_Showroom_Stage/index';
import '../../../components/ContentModules/m155_Highlight_Module/index';
import '../../../components/NavigationModules/m532_Showroom_Navigation/index';
import '../../../components/FunctionalModules/mk611_CTA_Form_Full_Width/index';
import '../../../components/FunctionalModules/mk613_ModelPreview/index';
import '../../../components/TeaserModules/m211_Next_Steps_Teaser/index';
import '../../../components/TeaserModules/m212_Shopping_Teaser/index';
import '../../../components/TeaserModules/m246_Trim_Selector/index';
import '../../../components/TeaserModules/m207_Small_Image_Teaser/index';
import '../../../components/ContentModules/m101_Intro_Copy/index';
import '../../../components/ContentModules/m102_Copy/index';
import '../../../components/ContentModules/m151_Introcopy/index';
import '../../../components/ContentModules/mk154_Feature_Wrapper/index';


// HMR setup
if (module.hot) {
  module.hot.accept();
}
