import $ from 'jquery'
import * as Media from 'globals/js/media.js';



/*autocard*/
/*disclaimer*/
$(document).on('click', '.js_auto-card__price-disclaimer-toggler', function(e) {
 e.preventDefault();
 e.stopPropagation();
 if ($(e.target).closest('.js_auto-card').hasClass('disclaimer-shown')) {
   hideDisclaimer($(e.target))
 } else {
   showDisclaimer($(e.target))
 }
})

function showDisclaimer(trigger) {
 trigger.closest('.js_auto-card').addClass('disclaimer-shown');
}

function hideDisclaimer(trigger) {
 if (trigger != 'all') {
   trigger.closest('.js_auto-card').removeClass('disclaimer-shown');
 } else {
   $('.js_auto-card').removeClass('disclaimer-shown');
 }
}



/*details*/
$(document).on('click', '.js_auto-card__details-trigger', function(e) {
 //e.preventDefault();
 e.stopPropagation();
 hideDisclaimer('all')
 if ($(e.target).closest('.js_auto-card').hasClass('details-shown')) {
   hideDetails($(e.target))
 } else {
   showDetails($(e.target))
 }
})

function showDetails(trigger) {
  hideDetails('all');
 trigger.closest('.js_auto-card').addClass('details-shown');
}

function hideDetails(trigger) {
 if (trigger != 'all') {
   trigger.closest('.js_auto-card').removeClass('details-shown');
 } else {
   $('.js_auto-card').removeClass('details-shown');
 }
}





/*contacts*/
$(document).on('click','.js_auto-card__contacts-trigger', function(e) {
  e.preventDefault();
  e.stopPropagation();
  showContacts($(e.target))
})

$(document).on('click', '.js_auto-card__dealer-contacts',function(e) {
  e.stopPropagation();
})

function showContacts(trigger){
      trigger.closest('.js_auto-card').addClass('contacts-shown');
}
function hideContacts(trigger){
  if (trigger != 'all'){
    trigger.closest('.js_auto-card').removeClass('contacts-shown');
  }
  else{
    $('.js_auto-card').removeClass('contacts-shown');
  }
}



$(document).on('scroll',function(){
  hideContacts('all');
  hideDetails('all');
  hideDisclaimer('all')
})
/*autocard end*/