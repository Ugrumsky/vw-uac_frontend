import './index.scss';
import React from 'react';


const IconLink = (props) => {

    let iconLink;
    let className = 'vwd5-textlink';
    let iconClass = 'icon vwd5-textlink__icon';
    className += props.className? ` ${props.className}` : '';
    iconClass += props.iconClass ? ` ${props.iconClass}` : '';
    if (props.href){
        iconLink =
        <a className={className} href={props.href}>
            <span className={iconClass}></span>
            {props.children}
            <span className={iconClass}></span>
        </a>
    }
    else if (props.onClick){
        iconLink =
        <span className={className} onClick={props.onClick}>
            <span className={iconClass}></span>
            {props.children}
            <span className={iconClass}></span>
        </span>
    }
    else{
        iconLink =
        <span className={className}>
            <span className={iconClass}></span>
            {props.children}
            <span className={iconClass}></span>
        </span>
    }

    return (
       <div>{iconLink}</div>
    );
};

export default IconLink;
