import './js/_core.js';

// variables, mixins and global styles for whole project
import 'globals/style/dws_common.scss';
// Global js dependencies
import 'components/NavigationModules/mk504_Dealer_ID/index.js';
import 'components/NavigationModules/m502_Mainnavigation/index.js';
import 'components/NavigationModules/m520_Global_Footer/index.js';

