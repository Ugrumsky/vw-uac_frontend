import sticky from '../../../globals/js/Libraries/sticky';
window.sticky = sticky;
/*************Map*****************/
map = false;
let mapCenter = {lat: 55.714916, lng: 37.718094}

export function markerClickHandler(dealer){
    let card = $('#dealerid_' + dealer['ID'])
    scrollToCard(card);

}

function getMarkerIcon(iconUrl){
    return {
        url: iconUrl,
        size: new google.maps.Size(33, 44),
        origin: new google.maps.Point(0, 0),
         anchor: new google.maps.Point(22, 44),
        scaledSize: new google.maps.Size(33, 44),
    }
}
$(function(){
    initMap();
})

function initMap() {
    map = new google.maps.Map(document.getElementById('js_dealer-info-card__map'), {
        center: mapCenter,
        zoom: 10,
        maxZoom: 18,
        scrollwheel: false,
        disableDefaultUI: true
    });

    var marker = new google.maps.Marker({
        position: mapCenter,
        map: map,
        icon: getMarkerIcon('data:image/svg+xml;base64,PHN2ZyBpZD0iTGF5ZXJfMSIgZGF0YS1uYW1lPSJMYXllciAxIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAzMyA0NCI+PGRlZnM+PHN0eWxlPi5jbHMtMXtmaWxsOiMwMDk5ZGE7c3Ryb2tlOiNmZmY7c3Ryb2tlLXdpZHRoOjEuNXB4O30uY2xzLTJ7ZmlsbDojZmZmO308L3N0eWxlPjwvZGVmcz48dGl0bGU+QXJ0Ym9hcmQgMTwvdGl0bGU+PHBhdGggY2xhc3M9ImNscy0xIiBkPSJNMzEuOCwxNi40QTE1LjQsMTUuNCwwLDAsMCwxLDE2LjRDMSwzMC40LDE2LjQsNDMsMTYuNCw0M1MzMS44LDMwLjQsMzEuOCwxNi40Ii8+PGNpcmNsZSBjbGFzcz0iY2xzLTIiIGN4PSIxNi40IiBjeT0iMTYuMzgiIHI9IjQuNSIvPjwvc3ZnPg=='),
        optimized: false,
    });
}

    let mapResizeTimer;
    google.maps.event.addDomListener(window, 'resize', function() {
       clearTimeout(mapResizeTimer)
       mapResizeTimer = setTimeout(function(){
            map.setCenter(mapCenter);
       },500)
    });




/**************Tooltips*******************/
$(document).on('click', function(e) {
 e.stopPropagation();
  let elemToToggle = $(e.target).data('target')
 if ($(e.target).closest('.js_dealer-info-card').hasClass(elemToToggle+ '--shown')) {
    hideTarget($(e.target))
 } else {
   showTarget($(e.target))
 }
})

function showTarget(trigger) {
 trigger.closest('.js_dealer-info-card').addClass(trigger.data('target') + '--shown');
}

function hideTarget(trigger) {
 if (trigger != 'all') {
   trigger.closest('.js_dealer-info-card').removeClass(trigger.data('target') + '--shown');
 } else {
   $('.js_auto-card').removeClass('disclaimer-shown');
 }
}

/*******************Sticky**********************/


$(function(){
  let target = $('.js_dealer-info-card');
  let initialCardTop;
  let posCorrection = 20;

  if($('html').hasClass('is-ie')){
    initialCardTop = target.offset().top;
    target.addClass('sticky-ie')
  }

  $(window).on('scroll',function(){
    if($('html').hasClass('is-ie') ){
      let scrollLength = $(document).scrollTop();
      if (scrollLength > initialCardTop){
        target.css({'top': scrollLength - initialCardTop + posCorrection});
      }
    }
  })

})
