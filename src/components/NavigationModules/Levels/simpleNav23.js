/**
 * Created by ufs on 3/30/2017.
 */
import Component from '../../../globals/js/Libraries/component';
import Media from '../../../globals/js/media.js';
import _debounce from 'lodash/debounce';
import _isNull from 'lodash/isNull';
import $ from 'jquery';
// import sticky from '../../../globals/js/Libraries/vwsticky';
// import _debounce from 'lodash/debounce';


export default class TwoThreeNav extends Component {
  constructor(el) {
    super(el);
    this.scrollStep = 100;
    this.color = false;
    this.activeChild = 0;
    //this.sticky = _isNull(el.nextElementSibling) ? true : !(/navlvl3/g.test(el.nextElementSibling.className));
  }
  init() {
    this.scrollHandler();
    this.controlArrows();

    if (!Media.matches.tablet) {
        this.scrollToActive();
    }

    $(window).resize(_debounce(this.controlArrows.bind(this), 150));
    this.left.click(_debounce(this.scrollLeft.bind(this), 150));
    this.right.click(_debounce(this.scrollRight.bind(this), 150));

    $(window).resize(_debounce(this.scrollToActive.bind(this), 150));
  }

  scrollHandler() {
    this.inner.on('scroll', () => {
      this.controlArrows.bind(this)();
    });
  }

  controlArrows() {
    const leftOffset = this.inner.scrollLeft();
    const rightOffset = this.list.outerWidth() - this.inner.scrollLeft() - this.inner.innerWidth();
    // console.info(leftOffset, rightOffset);
    // console.info(this.list.outerWidth(), this.inner.scrollLeft(), this.inner.innerWidth());
    leftOffset <= 1 ? this.left.hide() : this.left.show();
    rightOffset <= 1 ? this.right.hide() : this.right.show();
  }

  scrollToActive() {
    if (!Media.matches.tablet) {
      let unit = this.inner.find('.is-active');
      if (this.inner[0].scrollWidth > $(window).width() && unit.length > 0) {
          this.inner.scrollLeft(unit.position().left - 45);
      }
    }
  }

  scrollLeft() { // a little bit
    const offsetLeft = this.inner.scrollLeft() - this.scrollStep;
    const newScroll = offsetLeft <= 0 ? 0 : offsetLeft;
    this.inner.animate({scrollLeft: newScroll}, 200);
  }
  scrollRight() { // a little bit
    const offsetRight = this.inner.scrollLeft() + this.scrollStep;
    const gap = this.list.outerWidth() - this.inner.innerWidth();
    const newScroll = offsetRight >= gap ? gap : offsetRight;
    this.inner.animate({scrollLeft: newScroll}, 200);
  }

  scrollTo(n) {
    if(n < this.list.children().length) {
      // console.log($(this.list.children()[n]), $(this.list.children()[n]).position());
      this.list.animate({'scrollLeft': $(this.list.children()[n]).position().left}, 200);
      this.activeChild = n;
      this.checkVisibleControls();
    }
  }
}
