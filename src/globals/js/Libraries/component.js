import $ from 'jquery';
import Page from './page';
import sticky from './vwsticky';
// import rgb2hex from 'rgb-hex';


export default class Component {
  constructor(element) {
    this.componentElement = element;
    this.pageIndex = Array.prototype.indexOf.call(element.parentNode.children, element);
    this.color = true;
    this.sticky = false;
    this.stickyInit = false;
    this.stickyInner = false;

    $(this.componentElement).data('_component', this);
    this.componentElement._component = this;
    $(this.componentElement).trigger('_component:init', this);
  }
  initComponent() {
    this.id = Page.registerModule(this);
    if(this.sticky) {
      sticky.add(this);
    }
  }
  updatePageIndex() {
    this.pageIndex = Array.prototype.indexOf.call(this.componentElement.parentNode.children, this.componentElement);
    Page.sortModules();
  }
}

export function findComponent(selector, Instance = Component) {
  $(() => {
    $(selector).each((index, el) => {
      const thisInstance = new Instance(el);
      if (Instance === Component) {
        thisInstance.initComponent();
      }
    });
  });
}

