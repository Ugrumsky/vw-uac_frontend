
/*disclaimers*/
$(document).on('click', '.js_disclaimer-trigger', function(e){
  e.preventDefault();
  e.stopPropagation();

  $('.js_disclaimer-container').removeClass('is_active');
  
  var targetData = $(this).data('target');
  var target = $('.js_disclaimer-container[data-name="' + targetData +'"]')
  target.addClass('is_active');
})

$(document).on('click', '.js_disclaimer__close-btn', function(){
  $(this).closest('.js_disclaimer-container').removeClass('is_active');
})
$(document).on('click',function(e){
  if($(e.target).closest('.js_disclaimer-container').length == 0){
    $('.js_disclaimer-container:not(.js_break_remove)').removeClass('is_active');
  }
})
$(window).on('scroll',function(){
  $('.js_disclaimer-container:not(.js_break_remove)').removeClass('is_active');
})