// Global js functionality
require("expose-loader?$!jquery");
require("expose-loader?jQuery!jquery");
window.jQuery = window.$;

// import $ from 'jquery';
import 'components/UI_Elements/input/index.js';
import 'components/UI_Elements/select/index.js';
//import 'components/Notification_and_Information_Modules/m644_Information_Layer/index';
import "components/NavigationModules/Levels/Two/index";


import lazyLoadImages from './common/imageLazyLoad';
lazyLoadImages();

// Fixes
import './common/touchfix';
import './common/browserDetect';
import './common/objAssignPolyfill';
import Promise from 'promise-polyfill';
if (!window.Promise) {
    window.Promise = Promise;
}
import 'whatwg-fetch';
// Common page functionality
import Page from './Libraries/page';
//import PowerLayer from './Libraries/powerlayer';

window['Page'] = Page;
//window['PowerLayer'] = PowerLayer;

/* not from npm cause latest versions have conflict with Jquery > 3*/
require("expose-loader?dotdotdot!./Libraries/dotdotdot.js")
require("expose-loader?slick!slick-carousel")

require("expose-loader?selects!components/UI_Elements/select/index.js")
require("expose-loader?datePickers!components/UacComponents/datepicker/index.js")
require("expose-loader?inputs!components/UI_Elements/input/init.js")
require("expose-loader?tabs!components/UacComponents/tabs/index.js")
require("expose-loader?dnd!components/UacComponents/dragNdropUpload/index.js")
require("expose-loader?announcementSliders!components/UacComponents/announcementCard/index.js")

function smoothScroll(posTo, duration){
	$('body, html').stop().animate({scrollTop:posTo}, duration);
}
$(document).on('click', '.js_scroll-to-top-btn', function(e){
  smoothScroll(0,300)
})

$(document).on('click', '.js_system-notification__close-btn', function(e){
sysNotification.doIt.hide()
})

$(function(){
	$(".js_announcement-card__text").dotdotdot({
		watch: true,
	});
})

