import Component, {findComponent} from '../../../../globals/js/Libraries/component';
import Page from '../../../../globals/js/Libraries/page';
import {FormValidator, addValidationOnChange} from '../../../../globals/js/utility/formValidator';

export default class EventCard extends Component {
  constructor(el) {
    super(el);
    this.color = false;
    this.el = el;
    this.$el = $(el);
    this.$foldToggler = this.$el.find('.js_eventCard__fold-toggler');
    this.isInOffersBlock = this.$el.closest('.js_timeline-block-with-offers').length > 0 ? true : false;
    this.$form = this.$el.find('form');
    this.$formSubmit = this.$form.find('input[type="submit"]');
    this.valForm = this.$form.length > 0 ? new FormValidator(this.$form) : null;
    this.$formCancel = this.$el.find('.js_eventCard__cancel-form');
    this.initComponent();
    this.init();
    if (this.$form.length > 1){
    	this.validateForm()
    }
  }

	init() {
		this.addValidation();
		this.$foldToggler.each((i, el)=> {
			$(el).click(()=>{
				this.toggleFold()
			});
		});
		this.$formCancel.each((i, el)=> {
			$(el).click(()=>{
				this.cancelForm()
			});
		});
	}

	toggleFold() {
		if(this.isInOffersBlock) {
			this.$el.closest('.js_timeline-block-with-offers').find('.js_eventCard').removeClass('below-the-fold-shown');
			this.$el.addClass('below-the-fold-shown');
		}
		else{
			this.$el.toggleClass('below-the-fold-shown');
		}
	}

	cancelForm() {
		this.$form[0].reset();
		this.validateForm();
		this.$el.removeClass('below-the-fold-shown');
	}

	addValidation() {
	    $(':input', this.$form ).each((i, inp) => {
	      addValidationOnChange(inp);
	      $(inp).on('change blur keyup', ()=>{
	        this.validateForm.bind(this)();
	      });
	    });
	  }

	validateForm() {
		if(this.valForm.isValid()){
			this.$formSubmit.removeClass('disabled')
		}
		else{
			this.$formSubmit.addClass('disabled')
		}
	}
}

findComponent('.js_eventCard', EventCard);