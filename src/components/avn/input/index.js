import $ from 'jquery';
import _throttle from 'lodash/throttle';

function inputHandling(wrapper, field) {
  if(!field.val()) {
    wrapper.removeClass('is-filled');
    field.removeClass('is-over-delete');
  } else {
    wrapper.addClass('is-filled');
  }
}

$(document).on('focus keyup change', '.input__field', function () {
  const field = $(this);
  const wrapper = field.parent().parent();
  inputHandling(wrapper, field);
}).on('blur', '.input__field', function (event) {
  $(this).parent().parent().removeClass('is-filled');
  $(this).removeClass('is-over-delete');
}).on('mousemove', '.is-filled .input__field', _throttle(function (event) {
  let field = $(this);
  let offset = field.offset();
  if((event.pageX - offset.left) > field.outerWidth() - 45) {
    field.addClass('is-over-delete');
  } else {
    field.removeClass('is-over-delete');
  }
}, 200)).on('click', '.is-over-delete', function () {
  $(this).val('').change();
});

$('.input__clear').on('click', function () {
  $(this).prev().val('').change();
});
