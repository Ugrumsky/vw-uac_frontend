/**
 * Created by ufs on 3/17/2017.
 */
import {findComponent} from '../../../../globals/js/Libraries/component';
import TwoThreeNav from '../simpleNav23';
// Import styles
import './index.scss';

class ThreeNav extends TwoThreeNav {
  constructor(el) {
    super(el);
    this.container = $(el).find('.navlvl3__container');
    this.inner = $(el).find('.navlvl3__inner');
    this.list = $(el).find('.navlvl3__list');
    this.left = $(el).find('.navlvl3__left');
    this.right = $(el).find('.navlvl3__right');
    this.stickyInner = this.container[0];
    this.initComponent();
    this.init();
  }
}


findComponent('.navlvl3', ThreeNav);
