/**
 * Created by Kodix on 16.02.2017.
 * @author ushakovfserg@gmail.com
 */

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['jquery'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // Node. Does not work with strict CommonJS, but
    // only CommonJS-like environments that support module.exports,
    // like Node.
    module.exports = factory(require('jquery'));
  } else {
    // Browser globals (root is window)
    root.returnExports = factory(root.jQuery);
  }
}(this, function ($) {
  return {
    GetEventHandler: function () {
      var strEventCall = arguments[0] || '';

      if (strEventCall !== '') {
        var objEvent = (strEventCall).split(':');

        if (typeof window[objEvent[0]] === 'object') {
          if (typeof window[objEvent[0]][objEvent[1]] === 'function') {
            var sortedArguments = [];

            $.each(arguments, function (key, value) {
              if (key !== 0)
                sortedArguments.push(value);
            });

            window[objEvent[0]][objEvent[1]].apply(null, sortedArguments);
          }
        }
      }
    },
    Form: {
      objDefaultEventNames: {
        SERVICE: 'Service',
        TEST_DRIVE: 'TestDrive',
        CALLBACK: 'Callback',
        FEEDBACK: 'Feedback',
        VACANCY: 'Vacancy',
        AVAILABLE_CAR_INFORMATION: 'AVNInformation',
        RESERVE_AVAILABLE_CAR: 'AVNReserve'
      },
      boolMobile: false,

      IsMobile: function () {
        return this.boolMobile;
      },

      GetEventName: function (strFormCode, strPostfix) {
        if (typeof strPostfix === 'undefined')
          strPostfix = '_Success';

        if (this.IsMobile())
          strPostfix += '_Mobile';

        return ( ( typeof this.objDefaultEventNames[strFormCode] !== 'undefined' ) ? (this.objDefaultEventNames[strFormCode]) : (strFormCode) ) + strPostfix;
      },
      YandexMetrika: {
        strCounterName: false,

        HaveCounter: function () {
          return !(this.strCounterName === false);
        },

        GetCounter: function () {
          return window[this.strCounterName];
        },

        Send: function (strTarget, objParams) {
          if (typeof strTarget !== 'string')
            return false;
          if (strTarget == '')
            return false;
          if (!this.HaveCounter())
            return false;

          if (typeof objParams !== 'object')
            objParams = {};

          var objCounter = this.GetCounter();

          objCounter['reachGoal'](strTarget, objParams);
        }
      },

      IsAllEventsPrevented: function () {
        return (typeof window.dForm === 'object' && typeof window.dForm['boolPreventDefaultEvent'] === 'boolean' && window.dForm['boolPreventDefaultEvents'] === true);
      },
      IsSingleEventPrevented: function (strFormCode) {
        return ( typeof window.dForm === 'object' && typeof window.dForm['arPreventDefaultEvent'] === 'array' && ($.inArray(strFormCode, window.dForm['arPreventDefaultEvent']) > -1) );
      },

      GoogleAnalyticsSend: function (arConfig) {
        var isFull = false;

        if (typeof arConfig !== 'object') // Если конфигурация - не объект
          return false;

        if (typeof arConfig.type === 'undefined') // Если конфигурация не соедржит описание типа события
          return false;

        if (arConfig.type != 'event' && arConfig.type != 'pageview') // Доступные действия
          return false;

        if (arConfig.type != 'pageview') { // Если у нас НЕ просмотр страницы с 1 параметром
          if (typeof arConfig.cat === 'undefined') // То должны быть все остальные параметры
            return false;
          else
            isFull = true;
        }

        if (typeof window.ga !== 'undefined' && typeof window.ga === 'function') { // Один тип отправки
          if (isFull) { // Если у нас не pageview с 1 параметром
            if (typeof arConfig.name === 'undefined') {
              if (typeof arConfig.action === 'undefined')
                window.ga('send', arConfig.type, arConfig.cat);
              else
                window.ga('send', arConfig.type, arConfig.cat, arConfig.action);
            }
            else
              window.ga('send', arConfig.type, arConfig.cat, arConfig.action, arConfig.name);
          }
          else // Если pageview
            window.ga('send', arConfig.type);
        }
        else if (typeof window._gaq !== 'undefined' && typeof window._gaq === 'function') { // Второй тип отправки
          var tmpAr = []; // Массив для отправки в gaq
          var eventType = '_track' + ((arConfig.type).replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function ($1) {
              return $1.toUpperCase();
            })); // Заменяем первый символ на прописной у события

          tmpAr.push(eventType);

          if (isFull) {
            tmpAr.push(arConfig.cat);
            if (typeof arConfig.action !== 'undefined')
              tmpAr.push(arConfig.action);

            if (typeof arConfig.name !== 'undefined')
              tmpAr.push(arConfig.name);
          }

          window._gaq.push(tmpAr);
        }

        return false;
      }
    }
  };
}));
