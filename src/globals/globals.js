// variables, mixins and global styles for whole project
import 'globals/style/common.scss';


import './js/_core.js';


import 'components/NavigationModules/m502_Mainnavigation/index.js';

import 'components/UacComponents/topBar_v2/index.js';
import 'components/UacComponents/tabs/index.js';
import 'components/UacComponents/authBlock/index.js';
import 'components/UacComponents/dragNdropUpload/index.js';
import 'components/UacComponents/checkboxDropdown/index.js';
import 'components/UacComponents/disclaimer/index.js';
import 'components/UacComponents/popup/index.js';
import 'components/UacComponents/topLoader/index.js';
import 'components/UacComponents/sys_notification/index.js';