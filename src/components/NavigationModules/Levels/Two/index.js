import {findComponent} from '../../../../globals/js/Libraries/component';
import TwoThreeNav from '../simpleNav23';
// Import styles
import './index.scss';

class TwoNav extends TwoThreeNav {
  constructor(el) {
    super(el);
    this.container = $(el).find('.navlvl2__container');
    this.inner = $(el).find('.navlvl2__inner');
    this.list = $(el).find('.navlvl2__list');
    this.left = $(el).find('.navlvl2__left');
    this.right = $(el).find('.navlvl2__right');
    this.stickyInner = this.container[0];
    this.initComponent();
    this.init();
  }
}


findComponent('.navlvl2', TwoNav);
