$(function(){
	if($('.js_auth-block-slider').length > 0){
		initAuthBlockSLider()
	}
})

export let initAuthBlockSLider = function (){
	$('.js_auth-block-slider').not('.slick-initialized').slick({
	      dots: true,
	      infinite: false,
	      arrows: false,
	      speed: 300,
	      swipe: false,
	      slidesToShow: 1,
	      adaptiveHeight: true,
	});
}


$(document).on('click','.js_slick-next', function(e) {
	e.preventDefault();
	$(this).closest('.slick-slider').slick('slickNext');
})
$(document).on('click','.js_slick-prev', function(e) {
	e.preventDefault();
	$(this).closest('.slick-slider').slick('slickPrev');
})