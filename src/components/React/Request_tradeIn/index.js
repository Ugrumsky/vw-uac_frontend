import React from 'react';
import {connect} from 'react-redux';
import * as actions from '../../../actions/index';

import EventCard from '../EventCard/index';
import EventCardAboveTheFold from '../EventCard_AboveTheFold/index';
import EventCardBelowTheFold from '../EventCard_BelowTheFold/index';
import EventCardBasicInfo from '../EventCard_BasicInfo/index';
import EventCardAdditionalInfo from '../EventCard_AdditionalInfo/index';
import EventCardDecisionIndicator from '../EventCard_DecisionIndicator/index';
import EventCardHistory from '../EventCard_History/index';
import EventCardDecisionBlock from '../EventCard_DecisionBlock/index';
import EventCardForm from '../EventCard_Form/index';
import EventCardSpecsTable from '../EventCard_SpecsTable/index';
import EventCardBorderSeparator from '../EventCard_BorderSeparator/index';

let classNames = require('classnames');


class RequestTradeIn extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showDeclineCause: false,
            userDecision: 'succes',
            declineCause: false
        };
    }

    resolveStep = (data) => {
        console.log('asd')
        this.props.resolveStep(data);
    };

    handleResetDecision = (e) => {
        this.setState({
            userDecision: 'unresolved',
        });

        this.resolveStep({
            dealerId: this.props.requestData.requestOwnDealer,
            requestId: this.props.requestData.requestId,
            userDecision: 'unresolved',
            declineCause: false,
        });
    };

    render() {
        const requestData = this.props.requestData;
        const requestInfo = requestData.requestInfo;

        const cardTitles = {
            'initial': '',
            'preEstimation': 'Наша предварительная оценка автомобиля',
            'instrumentalEstimation': 'Приглашаем вас на инстр. оценку вашего автомобиля',
            'finalEstimation': 'Наша финальная оценка автомобиля'
        };

        let content;
        switch (requestInfo.requestStep) {
            case 'initial': {
                content = 'Спасибо! Ваша заявка на оценку автомобиля будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!';
                break;
            }
            case 'preEstimation': {
                switch(requestInfo.requestStepStatus){
                    case 'unresolved': {
                        content = <Wrap {...requestData} cardTitles={cardTitles}>
                            <EventCardDecisionBlock
                                radios={{
                                    name: 'declineCause',
                                    btns: [
                                        {
                                            value: '1',
                                            title: 'Меня не устраивает цена',
                                        },
                                        {
                                            value: '2',
                                            title: 'В данный момент меня это не интересует',
                                        }
                                    ]
                                }}
                                title="Пожалуйста, оставьте комментарий о причине отказа"
                                dealerId={this.props.requestData.requestOwnDealer}
                                requestId={this.props.requestData.requestId}
                                resolveStep={this.resolveStep}
                            />
                        </Wrap>;
                        break;
                    }
                    case 'success': {
                        content = <Wrap {...requestData} cardTitles={cardTitles}>
                            <EventCardForm>
                                <EventCardDecisionIndicator
                                    status={requestInfo.requestStepStatus}
                                    text='Вы приняли предложение'
                                    onClick={this.handleResetDecision}/>
                            </EventCardForm>
                        </Wrap>;
                        break;
                    }
                    case 'fail': {
                        content = <Wrap {...requestData} cardTitles={cardTitles}>
                            <EventCardForm>
                                <EventCardDecisionIndicator
                                    status={requestInfo.requestStepStatus}
                                    text='Вы отказались от предложения'/>
                            </EventCardForm>
                        </Wrap>;
                        break;
                    }
                }
                break;
            }
            case 'instrumentalEstimation': {
                switch(requestInfo.requestStepStatus){
                    case 'unresolved': {
                        content = <Wrap {...requestData} cardTitles={cardTitles}>
                            <EventCardDecisionBlock
                                radios={{
                                    name: 'declineCause',
                                    btns: [
                                        {
                                            value: '1',
                                            title: 'Нужно перенести дату и время',
                                        },
                                        {
                                            value: '2',
                                            title: 'Неверные дата и время',
                                        },
                                        {
                                            value: '3',
                                            title: 'В данный момент меня это не интересует',
                                        }
                                    ]
                                }}
                                title="Пожалуйста, оставьте комментарий о причине отказа"
                                dealerId={this.props.requestData.requestOwnDealer}
                                requestId={this.props.requestData.requestId}
                                resolveStep={this.resolveStep}
                            />
                        </Wrap>;
                        break;
                    }
                    case 'success': {
                        content =<Wrap {...requestData} cardTitles={cardTitles}>
                            <EventCardForm>
                                <EventCardDecisionIndicator
                                    status={requestInfo.requestStepStatus}
                                    text='Дата и время подтверждены'
                                    onClick={this.handleResetDecision}/>
                            </EventCardForm>
                        </Wrap>;
                        break;
                    }
                    case 'fail': {
                        content =  <Wrap {...requestData} cardTitles={cardTitles}>
                            <EventCardForm>
                                <EventCardDecisionIndicator
                                    status={requestInfo.requestStepStatus}
                                    text='Вы отказались от приглашения'/>
                            </EventCardForm>
                        </Wrap>;
                        break;
                    }
                }
                break;
            }
            case 'finalEstimation': {
                switch(requestInfo.requestStepStatus){
                    case 'unresolved': {
                        content = <Wrap {...requestData} cardTitles={cardTitles}>
                            <EventCardDecisionBlock
                                radios={{
                                    name: 'declineCause',
                                    btns: [
                                        {
                                            value: '1',
                                            title: 'Меня не устраивает цена',
                                        },
                                        {
                                            value: '2',
                                            title: 'В данный момент меня это не интересует',
                                        }
                                    ]
                                }}
                                title="Пожалуйста, оставьте комментарий о причине отказа"
                                dealerId={this.props.requestData.requestOwnDealer}
                                requestId={this.props.requestData.requestId}
                                resolveStep={this.resolveStep}
                            />
                        </Wrap>;
                        break;
                    }
                    case 'success': {
                        content = <Wrap {...requestData} cardTitles={cardTitles}>
                            <EventCardForm>
                                <EventCardDecisionIndicator
                                    status={requestInfo.requestStepStatus}
                                    text='Вы приняли предложение'
                                    onClick={this.handleResetDecision}/>
                            </EventCardForm>
                        </Wrap>;
                        break;
                    }
                    case 'fail': {
                        content = <Wrap {...requestData} cardTitles={cardTitles}>
                            <EventCardForm>
                                <EventCardDecisionIndicator
                                    status={requestInfo.requestStepStatus}
                                    text='Вы отказались от предложения'/>
                            </EventCardForm>
                        </Wrap>;
                        break;
                    }
                }
                break;
            }
        }

        return (
            <div>{content}</div>
        );
    }
}

const Wrap = (props) => {
    const cardTitles = props.cardTitles;
    const requestInfo = props.requestInfo;
    const basicInfo = requestInfo.requestBasicInfo;
    const requestHistory = props.requestHistory;
    const specItems = requestInfo.requestSpecTableInfo;
    const specPreviews = requestInfo.requestSpecTablePreviews;
    return (
        <EventCard>
            <EventCardAboveTheFold cardTitle={cardTitles[requestInfo.requestStep]}>
                <EventCardBasicInfo basicInfo={basicInfo}/>
                <EventCardAdditionalInfo>
                    {props.children}
                </EventCardAdditionalInfo>
            </EventCardAboveTheFold>
            <EventCardBelowTheFold>
                <EventCardSpecsTable specItems={specItems} previewItems={specPreviews}/>
                <EventCardBorderSeparator />
                <EventCardHistory historyData={requestHistory}/>
            </EventCardBelowTheFold>
        </EventCard>
    );
};

const mapActionsToProps = (dispatch) => {
    return {
        resolveStep: (data) => {
            dispatch(actions.resolveStep(data));
        },
    };
}

RequestTradeIn = connect(null, mapActionsToProps)(RequestTradeIn);

export default RequestTradeIn;
