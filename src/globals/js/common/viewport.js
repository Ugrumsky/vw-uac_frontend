/**
 * Created by ufs on 3/22/2017.
 */

export default function viewport() {
  let e = window;
  let a = 'inner';
  if (!( 'innerWidth' in window )) {
    a = 'client';
    e = document.documentElement || document.body;
  }
  return {
    width: e[a + 'Width'],
    height: e[a + 'Height'],
  };
}
