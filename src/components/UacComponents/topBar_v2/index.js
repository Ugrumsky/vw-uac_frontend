
function hideTopBarSubmenus (anchorItem){
    if (anchorItem){
        anchorItem.removeClass('is_active');
        $('#js_' + anchorItem.data('targettotoggle')).removeClass('is_visible');
    }
    else{
        $('.js_toggle_submenu').removeClass('is_active');
        $('.js_top-bar_submenu').removeClass('is_visible');
    }
}
function showTopBarSubmenu (anchorItem){
    let target = $('#js_' + anchorItem.data('targettotoggle'));
    anchorItem.addClass('is_active');
    
    target.addClass('is_visible');
}

$(document).on('click', '.js_toggle_submenu', function(e){
    let $this = $(this);
    if ($this.hasClass('is_active')) {
        hideTopBarSubmenus();
    } else {
        hideTopBarSubmenus();
        showTopBarSubmenu($this);
    }
    e.preventDefault();
})


$(document).on('click', '.js_top-bar__profile-dialogue-toggler', function(e){
    let $this = $(this);
    let profileBlock = $this.closest('.js_top-bar__profile-block');
    if (profileBlock.hasClass('is_active')) {
        profileBlock.removeClass('is_active')
    } else {
        profileBlock.addClass('is_active')
    }
    e.preventDefault();
})