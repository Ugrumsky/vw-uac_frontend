import React from 'react';

const shortid = require('shortid');

const EventCardSpecsTable = (props) => {
    const specItems = props.specItems.map((specItem, i) => {

        let text;
        if (!specItem.link) {
            text = <div className="event-card-specs-table__table-item--right">{specItem.text}</div>
        }

        else {
            text = <a href="{specItem.link}">{specItem.text}</a>;
        }

        return (
            <div key={shortid.generate()} className="event-card-specs-table__table-item">
                <div className="event-card-specs-table__table-item--left">{specItem.title}</div>
                {text}
            </div>
        );
    });

    let previewItems = '';
    
    if (props.previewItems) {
        previewItems = props.previewItems.map((previewItem, i) => {

            return (
                <div key={shortid.generate()} className="event-card-specs-table__preview-item">
                    <img src={previewItem} alt=""/>
                </div>
            );
        });
    }

    return (
        <div className="event-card-specs-table__container padded-container">
            <div className="event-card-specs-table__table">
                {specItems}
            </div>
            <div className="event-card-specs-table__preview">
                {previewItems}
            </div>
            <a className="vwd5-textlink eventCard__icon-link" href="#">
                <span className="icon icon-base-share vwd5-textlink__icon"></span>Отправить другому дилеру</a>
        </div>
    );

};


export default EventCardSpecsTable;