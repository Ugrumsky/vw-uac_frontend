
export const updateOfferFilterTags = (arr) => {
    return {
        type: 'UPDATE_OFFER_FILTER_TAGS',
        payload: arr
    };
};