export function initAnnouncementSliders(){
	let sliders = $('body').find('.js_announcement-carousel');
	updateSliders(sliders)
	$(window).on('resize', function(){
		updateSliders(sliders)
	})

	function init(sliders){
		sliders.each(function(i, el){
			let announcementCards = $(el).find('.announcement-card');
			if (announcementCards.length > 1){
				$(el).not('.slick-initialized').slick({
	              dots: true,
	              infinite: true,
	              arrows: false,
	              speed: 300,
	              slidesToShow: 1,
	              adaptiveHeight: true,
				}).addClass('carousel slick-mobile');
			}
		})
	}


	function killSliders(sliders){
		sliders.each(function(i, el){
			if ($(el).hasClass('slick-initialized')){
				$(el).slick('unslick')
			}
		})
	}

	function updateSliders(sliders){
		if (window.matchMedia("(max-width: 512px)").matches){
			init(sliders)
		}
		else{
			killSliders(sliders)
		}
	}
}
