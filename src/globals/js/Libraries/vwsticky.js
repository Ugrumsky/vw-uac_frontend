/**
 * Created by ufs on 3/31/2017.
 */

import page from './page';
import toRem from '../utility/to-rem';
import {addClass, removeClass} from '../utility/classesMunipulation';
import _debounce from 'lodash/debounce';
import _throttle from 'lodash/throttle';
import viewport from '../common/viewport';

class VWSticky {
  constructor() {
    this.stack = [];
    this._correction = 0;
    $(window).on('scroll', _throttle(this.update.bind(this), 200));
  }

  get correction() {
    return this._correction;
  }

  set correction(v) {
    const _correctionLatest = this._correction;
    this._correction = parseInt(v);
    if (_correctionLatest !== this._correction) {
      this.update();
    }
  }

  /**
   * Add page module to sticky handler
   * @param module {Component}
   */
  add(module) {
    const self = this;
    $(() => {
      self.stack.push(module.id);
      self.stack.sort((a, b) => a.pageIndex - b.pageIndex);
      self.offsetTop(module);
    });
  }

  /**
   * Returns module sticky element
   * @param module {Component}
   * @returns {HTMLElement}
   */
  getModuleElement(module) {
    return module.stickyInner || module.componentElement;
  }

  /**
   * Sets module offset top
   * @param module {Component}
   */
  offsetTop(module) {
    const el = this.getModuleElement(module);
    const self = this;

    function setOffset() {
      el.style.top = '';
      module.componentElement.style.minHeight = '';
      module.offsetTop = el.getBoundingClientRect().top + document.body.scrollTop;
      module.componentElement.style.minHeight = `${toRem(el.offsetHeight)}rem`;
      self.update();
    }

    setOffset();
    $(window).on('resize', _debounce(setOffset, 200));
  }

  /**
   * Update Modules positions
   */
  update() {
    page.offset = 0;
    let offset = this.correction;
    for (let i = 0, length = this.stack.length; i < length; i++) {
      const module = page.getModule(this.stack[i]);
      const el = this.getModuleElement(module);
      if (document.body.scrollTop > module.offsetTop &&
        (i === length - 1 || document.body.scrollTop < page.getModule(this.stack[i + 1]).offsetTop)) {
        el.style.top = `${toRem(offset)}rem`;
        offset = offset + el.offsetHeight;
        if(module.stickyInit === false) {
          addClass(module.componentElement, 'is-sticked');
          el.style.position = 'fixed';
          el.style.width = '100%';
          if (viewport().width >= 768) {
            el.style.paddingRight = `${toRem(parseInt(window.getComputedStyle(el, null).paddingRight) + 96)}rem`;
          }
          module.stickyInit = true;
        }
      } else {
        if(module.stickyInit === true) {
          removeClass(module.componentElement, 'is-sticked');
          el.style.top = '';
          el.style.position = '';
          el.style.width = '';
          el.style.paddingRight = '';
          module.stickyInit = false;
        }
      }
    }
    page.offset = offset;
  }
}

export default new VWSticky();
