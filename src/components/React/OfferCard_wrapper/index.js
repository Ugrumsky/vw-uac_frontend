import React from 'react';
import {connect} from 'react-redux';
import * as actions from '../../../actions/index';
import Button from '../Button/index';
import RadioBtn from '../RadioBtn/index';
import EventCard from '../EventCard/index';
import EventCardAboveTheFold from '../EventCard_AboveTheFold/index';
import EventCardBelowTheFold from '../EventCard_BelowTheFold/index';
import EventCardBasicInfo from '../EventCard_BasicInfo/index';
import EventCardAdditionalInfo from '../EventCard_AdditionalInfo/index';
import EventCardDecisionIndicator from '../EventCard_DecisionIndicator/index';
import EventCardHistory from '../EventCard_History/index';
import EventCardDecisionBlock from '../EventCard_DecisionBlock/index';
import EventCardForm from '../EventCard_Form/index';
import OfferCard from '../OfferCard/index';
import EventCardSimpleText from '../EventCard_SimpleText/index';
import EventCardFlexContainer from '../EventCard_FlexContainer/index';

const classNames = require('classnames');
const shortid = require('shortid');

class OfferCardWrapper extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            userDecision: 'success',
            declineCause: false
        };
    }

    resolveOffer = (data) => {

        this.props.resolveOffer(data);
    };


    handleRadios = (e) => {
        this.setState({
            declineCause: e.target.value,
            userDecision: 'fail',
        });
    };

    /* toggleBelowTheFoldVisibility = (data) => {
     this.props.toggleBelowTheFoldVisibility({offerId: this.props.offerData.offerId})
     }*/

    render() {
        const radiosObj = {
            name: 'declineCause',
            btns: [
                {
                    value: '1',
                    title: 'Нужно перенести дату и время',
                },
                {
                    value: '2',
                    title: 'Неверные дата и время',
                },
                {
                    value: '3',
                    title: 'В данный момент меня это не интересует',
                },
            ],
        };

        const radios = radiosObj.btns.map((radio, i) => {
            const checked = radio.value == this.state.declineCause ? true : false;
            return (
                <RadioBtn key={shortid.generate()}
                          name={radiosObj.name}
                          value={radio.value}
                          text={radio.title}
                          checked={checked}
                          onChange={(e) => {
                              this.handleRadios(e)
                          }}/>
            );
        });

        let resolveDeclineBtnClass = 'btn_cta';

        if (!this.state.declineCause) resolveDeclineBtnClass += ' disabled';

        const content = <EventCard
            noBelow
            belowTheFoldShown={this.props.offerData.belowTheFoldShown}
            toggleBelowTheFoldVisibility={this.toggleBelowTheFoldVisibility}>
            <EventCardAboveTheFold cardTitle={null} notPadded>
                <OfferCard
                    resolveOffer={this.resolveOffer}
                    toggleTooltip={this.props.toggleOfferCardTooltip}
                    offerData={this.props.offerData}
                    parentOfferListId={this.props.parentOfferListId}/>
            </EventCardAboveTheFold>
            <EventCardBelowTheFold padded>
                <EventCardSimpleText>
                    Пожалуйста, оставьте комментарий о причине отказа
                </EventCardSimpleText>
                <EventCardFlexContainer>
                    {radios}
                </EventCardFlexContainer>
                <EventCardFlexContainer>
                    <Button className={resolveDeclineBtnClass}
                            tagName='div' text='Завершить'
                            onClickContextCombined
                            onClick={() => {
                                this.resolveOffer({
                                    offerId: this.props.offerData.offerId,
                                    userDecision: this.state.userDecision,
                                    declineCause: this.state.declineCause,
                                });
                            }}/>
                    <Button tagName='div'
                            text='Отмена'
                            onClickContextOverride/>
                </EventCardFlexContainer>
            </EventCardBelowTheFold>
        </EventCard>

        return (
            <div className="timeline__requestOffer-wrap">{content}</div>
        );
    }
}

Button.contextTypes = {
    onClick: React.PropTypes.func
}

const mapActionsToProps = (dispatch) => {
    return {
        resolveOffer: (data) => {
            dispatch(actions.resolveOffer(data));
        },
        toggleOfferCardTooltip: (data) => {
            dispatch(actions.toggleOfferCardtooltip(data));
        },
    };
}

OfferCardWrapper = connect(null, mapActionsToProps)(OfferCardWrapper);

export default OfferCardWrapper;
