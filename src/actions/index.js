export {updateOfferFilterTags} from './tagFilter';
export {requestsFetchData} from './requests';

export let changeDealer = (dealerId) => {
  return {
    type: 'CHANGE_DEALER',
    payload: dealerId
  };
};

export let resolveStep = (data) => {
    return {
        type: 'RESOLVE_STEP',
        payload: data
    };
};

export let resolveOffer = (data) => {
    return {
        type: 'RESOLVE_OFFER',
        payload: data
    };
};

export let toggleOfferCardtooltip = (data) => {
    return {
        type: 'TOGGLE_OFFER_CARD_TOOLTIP',
        payload: data
    };
};


export let resolveStepRating = (data) => {
    return {
        type: 'RESOLVE_STEP_RATING',
        payload: data
    };
};

export let filterDeclinedOffers = (data) => {
    return {
        type: 'FILTER_DECLINED_OFFERS',
        payload: data
    };
};


export function fetchData(successFunc, errorFunc, loadingFunc, url, fetchParams) {
    return (dispatch) => {
        dispatch(loadingFunc(true));
        fetch(url, fetchParams)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(loadingFunc(false));

                return response;
            })
            .then((response) => response.json())
            .then((items) => dispatch(successFunc(items)))
            .catch(() => {
                dispatch(errorFunc(true));
                dispatch(loadingFunc(false));
            });
    };
}