export let carModels = {
  "\u041d\u0435\u0442 \u0430\u0432\u0442\u043e\u043c\u043e\u0431\u0438\u043b\u044f": {
    "brand_id": 10045,
    "models": {
      "10045": "\u041d\u0435\u0442 \u0430\u0432\u0442\u043e\u043c\u043e\u0431\u0438\u043b\u044f"
    }
  },
  "Volkswagen": {
    "brand_id": 136,
    "models": {
      "136": "Bora",
      "137": "Bora Variant",
      "155": "Caddy Life",
      "160": "California",
      "167": "Caravelle",
      "208": "Corrado",
      "240": "Derby",
      "255": "EOS",
      "286": "Fox",
      "301": "Golf",
      "302": "Golf Cabriolet",
      "303": "Golf GTI",
      "304": "Golf Plus",
      "305": "Golf Variant",
      "343": "Iltis",
      "356": "Jetta",
      "373": "L 82",
      "395": "LT",
      "397": "Lupo",
      "398": "Lupo 3L",
      "441": "Multivan",
      "455": "New Beetle",
      "456": "New Beetle Cabrio",
      "483": "Passat",
      "484": "Passat CC",
      "485": "Passat Variant",
      "490": "Phaeton",
      "496": "Polo_sedan",
      "497": "Polo Variant",
      "498": "Polo-Classic",
      "566": "Scirocco",
      "576": "Sharan",
      "622": "Taro",
      "635": "Tiguan",
      "638": "Touareg",
      "639": "Touran ",
      "649": "Transporter",
      "675": "Vento",
      "10000": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10046": "Polo",
      "10278": "Passat B4",
      "10279": "Vortex",
      "10280": "T 5",
      "10281": "T 4",
      "10282": "Santana",
      "10283": "Pointer",
      "10284": "Passat FSI",
      "10285": "Passat B5",
      "10286": "Passat B3 Variant",
      "10287": "Passat B3",
      "10288": "Passat B2",
      "10289": "LT 35",
      "10290": "Evrovan",
      "10291": "Crafter",
      "10292": "Caddy",
      "10293": "Passat B6",
      "10319": "Amarok",
      "10340": "Beetle",
      "10336": "Passat Lim",
      "10337": "Polo HB",
      "10338": "Touareg TDI\/FSI ",
      "10339": "Passat Alltrack",
      "836": "Passat B8",
      "10341": "Caddy (\u043a\u043e\u043c\u043c\u0435\u0440\u0447\u0435\u0441\u043a\u0438\u0439)",
      "10343": "Polo GT",
      "10344": "Golf R",
      "10345": "New Amarok",
      "10346": "Golf GTE",
      "10347": "NEW Tiguan"
    }
  },
  "Alpina-BMW": {
    "brand_id": 123,
    "models": {
      "123": "BMW Alpina B10",
      "124": "BMW Alpina B12",
      "125": "BMW Alpina B3",
      "126": "BMW Alpina B5",
      "127": "BMW Alpina B6",
      "128": "BMW Alpina B7",
      "129": "BMW Alpina B8",
      "130": "BMW Alpina D10",
      "131": "BMW Alpina D12",
      "132": "BMW Alpina D3",
      "133": "BMW Alpina Roadster S",
      "723": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Artega": {
    "brand_id": 724,
    "models": {
      "724": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Aston Martin": {
    "brand_id": 232,
    "models": {
      "232": "DB 7",
      "233": "DB 9",
      "234": "DB S",
      "524": "Rapide",
      "669": "Vanquish",
      "670": "Vantage",
      "725": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Audi": {
    "brand_id": 2,
    "models": {
      "2": "100",
      "16": "200",
      "54": "80",
      "58": "90",
      "64": "A1",
      "65": "A1 Sportback",
      "66": "A2",
      "67": "A3",
      "68": "A3 Cabriolet",
      "69": "A3 Sportback",
      "70": "A4 allroad quattro",
      "71": "A4 Avant",
      "72": "A4 Cabriolet",
      "73": "A4 Limousine",
      "74": "A5 Cabriolet",
      "75": "A5 Coupe",
      "76": "A5 Sportback",
      "77": "A6 allroad ",
      "78": "A6 Avant",
      "79": "A6 Limousine",
      "80": "A8",
      "81": "A8 12-Zylinder",
      "511": "Q5",
      "512": "Q7",
      "513": "Q7 V12",
      "518": "R8",
      "538": "RS 2",
      "539": "RS 6 Avant",
      "540": "RS 6 Limousine",
      "544": "S2",
      "546": "S3",
      "547": "S3 Sportback",
      "548": "S4 Avant",
      "549": "S4 Limousine",
      "551": "S5 Cabriolet",
      "552": "S5 Coupe",
      "553": "S5 Sportback",
      "554": "S6 Avant",
      "555": "S6 Limousine",
      "558": "S8",
      "598": "Sportquattro",
      "653": "TT Coupe",
      "654": "TT Roadster",
      "655": "TT RS Coupe",
      "656": "TT RS Roadster",
      "657": "TTS Coupe",
      "658": "TTS Roadster",
      "811": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10047": "A4",
      "10048": "A6",
      "10310": "A7",
      "10311": "A7 Sportback",
      "10313": "S6",
      "10315": "RS 5 Coupe"
    }
  },
  "Autobianchi": {
    "brand_id": 105,
    "models": {
      "105": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Bentley": {
    "brand_id": 94,
    "models": {
      "94": "Arnage",
      "114": "Azure",
      "197": "Contiental GT",
      "198": "Continental",
      "200": "Continental Flying Spur",
      "439": "Mulsanne",
      "726": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "BMW": {
    "brand_id": 1,
    "models": {
      "1": "1 \u0441\u0435\u0440\u0438\u044f",
      "22": "3 \u0441\u0435\u0440\u0438\u044f",
      "42": "5 \u0441\u0435\u0440\u0438\u044f",
      "48": "6 \u0441\u0435\u0440\u0438\u044f",
      "51": "7 \u0441\u0435\u0440\u0438\u044f",
      "53": "8 \u0441\u0435\u0440\u0438\u044f",
      "400": "M3",
      "401": "M5",
      "402": "M6",
      "690": "X1",
      "691": "X3",
      "692": "X5",
      "693": "X5 M",
      "694": "X6 M",
      "695": "X8",
      "714": "Z3",
      "715": "Z4",
      "716": "Z8",
      "727": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10051": "318",
      "10052": "328i",
      "10053": "520i",
      "10054": "525i",
      "10055": "740"
    }
  },
  "Brilliance": {
    "brand_id": 728,
    "models": {
      "728": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Bugatti": {
    "brand_id": 423,
    "models": {
      "423": "Miscellaneouse",
      "729": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10056": "Veyron"
    }
  },
  "Buick": {
    "brand_id": 181,
    "models": {
      "181": "Century",
      "375": "LaCrosse",
      "383": "Le Sabre",
      "478": "Park Avenue",
      "526": "Regal",
      "730": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "BYD": {
    "brand_id": 10057,
    "models": {
      "10324": "F3"
    }
  },
  "Cadillac": {
    "brand_id": 122,
    "models": {
      "122": "BLS",
      "173": "Catera",
      "226": "CTS",
      "235": "De Ville",
      "250": "Eldorado",
      "258": "Escalade",
      "574": "Seville",
      "601": "SRX",
      "607": "STS",
      "705": "XLR",
      "731": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Caterham": {
    "brand_id": 732,
    "models": {
      "732": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Chery": {
    "brand_id": 10041,
    "models": {
      "10041": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10058": "Tiggo",
      "10059": "For a",
      "10060": "Amulet",
      "10061": "Aveo"
    }
  },
  "Chevrolet": {
    "brand_id": 99,
    "models": {
      "10326": "Avalanche",
      "110": "Aveo",
      "161": "Camaro",
      "165": "Captiva",
      "175": "Cavalier",
      "211": "Corvette",
      "223": "Cruze",
      "256": "Epica",
      "266": "Evanda",
      "274": "Express ",
      "344": "Impala",
      "365": "Kalos",
      "379": "Lanos",
      "382": "Lazetti",
      "396": "Lumina",
      "404": "Malibu",
      "409": "Matiz",
      "433": "Monte Carlo",
      "463": "Nubira",
      "531": "Rezzo",
      "543": "S-12",
      "583": "Silverado",
      "602": "SSR",
      "609": "Suburban",
      "620": "Tahoe",
      "644": "TrailBlazer",
      "646": "Trans Sport",
      "733": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "858": "Niva",
      "10001": "Spark",
      "10062": "Viva",
      "10335": "Cobalt"
    }
  },
  "Chrysler": {
    "brand_id": 24,
    "models": {
      "24": "302 M",
      "186": "Cirrus",
      "196": "Concorde",
      "220": "Crossfire",
      "316": "Grand Voyager",
      "389": "LHS",
      "453": "Neon",
      "508": "PT Cruiser",
      "569": "Sebring",
      "642": "Town&Country",
      "682": "Viper",
      "688": "Voyager",
      "734": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10298": "300 C"
    }
  },
  "Citroen": {
    "brand_id": 119,
    "models": {
      "119": "Berlingo",
      "145": "C1",
      "146": "C2",
      "147": "C3",
      "148": "C3 Pluriel",
      "150": "C4",
      "151": "C5",
      "152": "C6",
      "154": "C8",
      "178": "C-Crosser",
      "245": "DS5",
      "268": "Evasion",
      "360": "Jumper",
      "361": "Jumpy",
      "452": "Nemo",
      "563": "Saxo",
      "696": "Xara Picasso",
      "706": "XM",
      "707": "Xsara",
      "735": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10063": "C3 Picasso",
      "10064": "C4 Picasso"
    }
  },
  "Dacia ": {
    "brand_id": 736,
    "models": {
      "736": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Daewoo": {
    "brand_id": 267,
    "models": {
      "267": "Evanda",
      "366": "Kalos",
      "380": "Lanos",
      "386": "Leganza",
      "410": "Matiz",
      "445": "Musso",
      "457": "Nexia",
      "464": "Nubira",
      "532": "Rezzo",
      "619": "Tacuma",
      "737": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10017": "Sens",
      "10065": "Espero",
      "10066": "Tico"
    }
  },
  "Daihatsu": {
    "brand_id": 229,
    "models": {
      "229": "Cuore",
      "408": "Materia",
      "585": "Sirion",
      "626": "Terios",
      "712": "YRV",
      "738": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Dodge": {
    "brand_id": 157,
    "models": {
      "157": "Caliber",
      "166": "Caravan",
      "231": "Dakota",
      "246": "Durango",
      "310": "Grand Caravan",
      "359": "Journey",
      "454": "Neon",
      "458": "Nitro",
      "519": "Ram-Serie",
      "605": "Stratus",
      "683": "Viper",
      "739": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10067": "3110",
      "10068": "Avenger",
      "10069": "Intrepid",
      "10070": "Magnum"
    }
  },
  "Faw": {
    "brand_id": 10071,
    "models": {
      "10071": "Vita"
    }
  },
  "Ferrari": {
    "brand_id": 30,
    "models": {
      "30": "348",
      "32": "355",
      "33": "360 Modena",
      "39": "456 M",
      "45": "599",
      "254": "Enzo",
      "614": "Superamerica",
      "740": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10072": "Maranello"
    }
  },
  "Fiat": {
    "brand_id": 44,
    "models": {
      "44": "502",
      "117": "Barchetta",
      "140": "Brava",
      "142": "Bravo",
      "185": "Cinquecento",
      "215": "Coupe",
      "217": "Croma",
      "244": "Doblo",
      "317": "Grande Punto",
      "391": "Linea",
      "406": "Marea",
      "440": "Multipla",
      "475": "Palio",
      "477": "Panda",
      "510": "Punto",
      "516": "Qubo",
      "568": "Scudo",
      "570": "Sedici",
      "571": "Seicento",
      "604": "Stilo",
      "625": "Tempra",
      "661": "Ulysee",
      "741": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10073": "Albea",
      "10074": "Ducato"
    }
  },
  "Ford": {
    "brand_id": 201,
    "models": {
      "201": "Contour",
      "213": "Cougar",
      "222": "Crown Victoria",
      "260": "Escape",
      "261": "Escort",
      "269": "Excursion",
      "273": "Explorer",
      "278": "Fiesta",
      "281": "Focus",
      "282": "Focus C-Max",
      "291": "Fusion",
      "297": "Galaxy",
      "323": "GT",
      "363": "Ka",
      "371": "Kuga",
      "412": "Maverick",
      "430": "Mondeo",
      "447": "Mustang",
      "509": "Puma",
      "523": "Ranger",
      "567": "Scorpio",
      "623": "Taurus",
      "640": "Tourneo",
      "648": "Transit",
      "681": "Windstar",
      "742": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "808": "\u0441\u0435\u0440\u0438\u044f F",
      "10015": "S-Max",
      "10016": "RS",
      "10075": "Sierra"
    }
  },
  "GAZ": {
    "brand_id": 10039,
    "models": {
      "10039": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10076": "Saiber",
      "10077": "Sobol",
      "10078": "3302",
      "10079": "31105",
      "10080": "3110",
      "10081": "31029",
      "10082": "2752",
      "10083": "2705",
      "10084": "2411",
      "10085": "2410",
      "10086": "22171",
      "10087": "3102"
    }
  },
  "Geely": {
    "brand_id": 10088,
    "models": {
      "10088": "MK",
      "10089": "Otaka"
    }
  },
  "GM": {
    "brand_id": 86,
    "models": {
      "86": "Alero",
      "88": "Allante",
      "102": "Atztek",
      "104": "Aurora",
      "118": "Beretta",
      "121": "Blazer",
      "135": "Bonville",
      "141": "Bravada",
      "156": "Cadillac",
      "162": "Camaro",
      "174": "Catera",
      "182": "Century",
      "183": "Cevalier",
      "209": "Corsa",
      "212": "Corvette",
      "227": "CTS",
      "241": "DeVille",
      "251": "Eldorado",
      "253": "Envoy",
      "259": "Escalade",
      "275": "EXT",
      "279": "Firebird",
      "308": "Grand AM",
      "320": "GrandPrix",
      "335": "Hummer H1",
      "336": "Hummer H2",
      "337": "Hummer H3",
      "345": "Impala",
      "350": "Intrigue",
      "384": "Le Sabre",
      "405": "Malibu",
      "413": "Maverick",
      "419": "Metro",
      "431": "Montana",
      "434": "Monte Carlo",
      "479": "Park Avenue",
      "507": "Przim",
      "527": "Regal",
      "529": "Rendevous",
      "560": "Safari",
      "575": "Seville",
      "581": "Silhouette",
      "610": "Suburban",
      "611": "Sunfire",
      "621": "Tahoe",
      "647": "Trans Sport",
      "676": "Venture",
      "679": "Vibe",
      "713": "Z 30",
      "743": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10090": "Pontiac"
    }
  },
  "Great Wall": {
    "brand_id": 10091,
    "models": {
      "10091": "Safe"
    }
  },
  "Hafei": {
    "brand_id": 10092,
    "models": {
      "10092": "Brio"
    }
  },
  "Honda": {
    "brand_id": 83,
    "models": {
      "83": "Accord",
      "187": "Civic",
      "195": "Concerto",
      "224": "CR-V",
      "225": "CRX",
      "290": "FR-V",
      "324": "GT Coupe",
      "334": "HR-V",
      "348": "Insight",
      "355": "Jazz",
      "387": "Legend",
      "392": "Logo",
      "462": "NSX",
      "466": "Odyssee",
      "486": "Passport",
      "501": "Prelude",
      "545": "S2002",
      "578": "Shuttle",
      "744": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10014": "Pilot",
      "10093": "Torneo",
      "10094": "Civic Ferio",
      "10095": "Avancier",
      "10096": "Domani",
      "10097": "Fit",
      "10098": "Integra",
      "10099": "Partner",
      "10100": "Sonata",
      "10101": "StepWGN",
      "10102": "Stream",
      "10103": "Acura"
    }
  },
  "Hummer": {
    "brand_id": 328,
    "models": {
      "328": "H1",
      "329": "H2",
      "330": "H3",
      "745": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Hyundai": {
    "brand_id": 82,
    "models": {
      "82": "Accent",
      "101": "Atos",
      "216": "Coupe",
      "249": "Elantra",
      "299": "Galloper",
      "300": "Getz",
      "318": "Grandeur",
      "327": "H Series",
      "338": "i10",
      "339": "i20",
      "340": "i30",
      "353": "ix35",
      "354": "ix55",
      "381": "Lantra",
      "411": "Matrix",
      "562": "Santa Fe",
      "587": "Sonata",
      "588": "Sonica",
      "627": "Terracan",
      "645": "Trajet",
      "702": "XG",
      "746": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "814": "Tucson",
      "10013": "Genesis",
      "10104": "Tager",
      "10105": "Verna",
      "10299": "Grand Starex",
      "10317": "Solaris",
      "10330": "i40",
      "10331": "Equus"
    }
  },
  "IBC": {
    "brand_id": 747,
    "models": {
      "747": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Infiniti": {
    "brand_id": 10040,
    "models": {
      "10040": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10106": "FX",
      "10107": "FX 35",
      "10108": "M35x",
      "10312": "QX",
      "10314": "G"
    }
  },
  "Isuzu": {
    "brand_id": 96,
    "models": {
      "96": "Ascender",
      "112": "Axiom",
      "535": "Rodeo",
      "630": "TF",
      "652": "Trooper",
      "748": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10109": "Midi"
    }
  },
  "IZH": {
    "brand_id": 10110,
    "models": {
      "10110": "21251",
      "10111": "2126",
      "10112": "21261",
      "10113": "2715"
    }
  },
  "Jaguar": {
    "brand_id": 608,
    "models": {
      "608": "S-Type",
      "701": "XF",
      "703": "XJ",
      "704": "XK",
      "709": "X-Type",
      "749": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Jeep": {
    "brand_id": 184,
    "models": {
      "184": "Cherokee",
      "193": "Commander",
      "194": "Compass",
      "311": "Grand Cherokee",
      "488": "Patriot",
      "689": "Wrangler",
      "750": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10114": "Liberty"
    }
  },
  "Kia": {
    "brand_id": 168,
    "models": {
      "168": "Carens",
      "171": "Carnival",
      "179": "Cee`d",
      "188": "Clarus",
      "358": "Joice",
      "403": "Magentis",
      "417": "Mentor",
      "470": "Opirus",
      "493": "Picanto",
      "500": "Pregio",
      "504": "Pride",
      "533": "Rio",
      "572": "Sephia",
      "577": "Shuma",
      "589": "Sorento",
      "590": "Soul",
      "593": "Spectra",
      "597": "Sportage",
      "674": "Venga",
      "751": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10115": "Avella Delta",
      "10116": "Ceed",
      "10117": "Avella",
      "10118": "Cerato",
      "10329": "Quoris"
    }
  },
  "Lada": {
    "brand_id": 6,
    "models": {
      "6": "110",
      "7": "111",
      "8": "112",
      "364": "Kalina",
      "459": "Niva",
      "461": "Nova",
      "752": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10119": "217230",
      "10120": "2111",
      "10121": "2112",
      "10122": "2113",
      "10123": "2114",
      "10124": "21140",
      "10125": "2115",
      "10126": "21703",
      "10127": "21074",
      "10128": "Oka",
      "10129": "Priora",
      "10130": "Samara",
      "10131": "Sputnik",
      "10132": "21102",
      "10133": "2170",
      "10134": "2101",
      "10135": "1111",
      "10136": "2109",
      "10137": "1119",
      "10138": "2110",
      "10139": "2103",
      "10140": "2104",
      "10141": "2105",
      "10142": "2107",
      "10143": "1118",
      "10144": "2108",
      "10145": "21093",
      "10146": "21099",
      "10147": "2106",
      "10348": "Granta"
    }
  },
  "Lamborghini": {
    "brand_id": 242,
    "models": {
      "242": "Diablo",
      "298": "Gallardo",
      "443": "Murcielago",
      "753": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Lancia": {
    "brand_id": 236,
    "models": {
      "236": "Dedra",
      "238": "Delta",
      "368": "Kappa",
      "399": "Lybra",
      "444": "Musa",
      "492": "Phedra",
      "632": "Thesis",
      "711": "Ypsilon",
      "718": "Zeta",
      "754": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Land Rover": {
    "brand_id": 237,
    "models": {
      "237": "Defender",
      "243": "Discovery",
      "287": "Freelander",
      "521": "Range Rover",
      "522": "Range Rover Sport",
      "755": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Lexus": {
    "brand_id": 257,
    "models": {
      "257": "ES",
      "321": "GS",
      "352": "IS",
      "393": "LS",
      "541": "RX",
      "564": "SC",
      "756": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10148": "LX",
      "10149": "RX 330"
    }
  },
  "Lifan": {
    "brand_id": 10150,
    "models": {
      "10150": "Breez"
    }
  },
  "Lincoln": {
    "brand_id": 111,
    "models": {
      "111": "Aviator",
      "199": "Continental",
      "394": "LS",
      "425": "MKS",
      "426": "MKT",
      "427": "MKX",
      "428": "MKZ",
      "451": "Navigator",
      "641": "Town Car",
      "757": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Lotus": {
    "brand_id": 159,
    "models": {
      "159": "California",
      "190": "Club Racer",
      "252": "Elise",
      "263": "Esprit",
      "265": "Europa",
      "271": "Exige",
      "758": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "LuAZ": {
    "brand_id": 10151,
    "models": {
      "10151": "969M"
    }
  },
  "Maserati": {
    "brand_id": 28,
    "models": {
      "28": "3200",
      "306": "Gran Sport",
      "307": "Gran Turismo",
      "415": "MC 12",
      "515": "Quattroporte",
      "600": "Spyder",
      "759": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Maybach": {
    "brand_id": 760,
    "models": {
      "760": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Mazda": {
    "brand_id": 9,
    "models": {
      "9": "121",
      "15": "2",
      "21": "3",
      "29": "323",
      "41": "5",
      "46": "6",
      "47": "6 Sport",
      "50": "626",
      "144": "BT-50",
      "230": "CX-7",
      "239": "DEMIO",
      "437": "MPV",
      "448": "MX-3",
      "449": "MX-5",
      "502": "Premacy",
      "506": "Protege",
      "542": "RX-8",
      "651": "Tribute",
      "700": "XEDOS",
      "761": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "809": "\u0441\u0435\u0440\u0438\u044f \u0412",
      "810": "\u0441\u0435\u0440\u0438\u044f \u0415",
      "10152": "Xedos 6",
      "10153": "323F",
      "10154": "929",
      "10155": "Capella",
      "10156": "Familia",
      "10157": "Verisa",
      "10158": "323 F",
      "10300": "CX-9",
      "10349": "408",
      "10350": "208"
    }
  },
  "Mercedes": {
    "brand_id": 667,
    "models": {
      "667": "Vaneo",
      "678": "Viano",
      "685": "Vito",
      "687": "V-Klasse",
      "762": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "790": "\u043a\u043b\u0430\u0441\u0441 CL",
      "791": "\u043a\u043b\u0430\u0441\u0441 CLC",
      "792": "\u043a\u043b\u0430\u0441\u0441 CLK",
      "793": "\u043a\u043b\u0430\u0441\u0441 CLS",
      "794": "\u043a\u043b\u0430\u0441\u0441 E",
      "795": "\u043a\u043b\u0430\u0441\u0441 G",
      "796": "\u043a\u043b\u0430\u0441\u0441 GL",
      "797": "\u043a\u043b\u0430\u0441\u0441 GLK",
      "798": "\u043a\u043b\u0430\u0441\u0441 M",
      "799": "\u043a\u043b\u0430\u0441\u0441 R",
      "800": "\u043a\u043b\u0430\u0441\u0441 S",
      "801": "\u043a\u043b\u0430\u0441\u0441 SL",
      "802": "\u043a\u043b\u0430\u0441\u0441 SLK",
      "803": "\u043a\u043b\u0430\u0441\u0441 SLR",
      "804": "\u043a\u043b\u0430\u0441\u0441 \u0410",
      "805": "\u043a\u043b\u0430\u0441\u0441 \u0412",
      "806": "\u043a\u043b\u0430\u0441\u0441 \u0421",
      "10159": "ML500",
      "10160": "ML 320"
    }
  },
  "MG": {
    "brand_id": 631,
    "models": {
      "631": "TF",
      "719": "ZR",
      "720": "ZS",
      "721": "ZT",
      "763": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "MINI": {
    "brand_id": 202,
    "models": {
      "202": "Cooper",
      "203": "Cooper Clubman",
      "204": "Cooper S",
      "468": "One",
      "469": "One Cabrio",
      "764": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Mitsubishi": {
    "brand_id": 170,
    "models": {
      "170": "Carisma",
      "191": "Colt",
      "248": "Eclipse",
      "296": "Galant",
      "319": "Grandis",
      "374": "L200",
      "377": "Lancer",
      "422": "Mirage",
      "435": "Montero",
      "472": "Outlander",
      "474": "Pajero\/Montero",
      "591": "Space Star",
      "592": "Space Wagon",
      "765": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "807": "\u0440\u044f\u0434 L",
      "10161": "Pajero\/Monte",
      "10162": "Delica",
      "10163": "Lancer Cedia",
      "10164": "Pajero",
      "10301": "ASX"
    }
  },
  "Morgan": {
    "brand_id": 766,
    "models": {
      "766": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Nissan": {
    "brand_id": 31,
    "models": {
      "31": "350Z",
      "34": "370Z",
      "89": "Almera",
      "91": "Altima",
      "228": "Cube",
      "289": "Frontier",
      "325": "GT-R",
      "414": "Maxima",
      "420": "Micra",
      "442": "Murano",
      "450": "Navara",
      "460": "Note",
      "473": "Outros",
      "487": "Pathfinder",
      "489": "Patrol",
      "495": "Pixo",
      "514": "Qashquai",
      "573": "Serena",
      "613": "Sunny",
      "624": "Teana",
      "628": "Terrano",
      "629": "Terrano II",
      "633": "Tida",
      "668": "Vanette",
      "708": "X-Trail",
      "767": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10043": "Primera",
      "10165": "Expert",
      "10166": "Avenir",
      "10167": "Cefiro",
      "10168": "March",
      "10169": "Pulsar",
      "10170": "Skyline",
      "10171": "Titan",
      "10172": "Wingroad",
      "10173": "Bluebird",
      "10327": "Cubic Cube",
      "10328": "Juke"
    }
  },
  "Opel": {
    "brand_id": 85,
    "models": {
      "85": "Agila",
      "93": "Antara",
      "97": "Astra",
      "98": "Astra Caravan",
      "158": "Calibra",
      "163": "Campo",
      "192": "Combo",
      "210": "Corsa",
      "214": "Coupe",
      "288": "Frontera",
      "349": "Insignia",
      "418": "Meriva",
      "436": "Movano",
      "467": "Omega",
      "580": "Signum",
      "584": "Sintra",
      "594": "Speedster",
      "634": "Tigra",
      "671": "Vectra",
      "672": "Vectra Caravan",
      "686": "Vivaro",
      "717": "Zafira",
      "768": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10174": "Kadett",
      "10175": "Rekord"
    }
  },
  "Peugeot": {
    "brand_id": 3,
    "models": {
      "3": "1007",
      "4": "106",
      "5": "107",
      "18": "206",
      "19": "207",
      "23": "3008",
      "25": "306",
      "26": "307",
      "27": "308",
      "35": "4007",
      "36": "406",
      "37": "407",
      "43": "5008",
      "49": "607",
      "55": "806",
      "56": "807",
      "120": "Bipper",
      "138": "Boxer",
      "272": "Expert",
      "481": "Partner",
      "520": "Ranch",
      "769": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10176": "605"
    }
  },
  "Pontiac": {
    "brand_id": 134,
    "models": {
      "134": "Bonneville",
      "280": "Firebird",
      "292": "G3",
      "293": "G5",
      "294": "G6",
      "295": "G8",
      "309": "Grand Am",
      "313": "Grand Prix",
      "432": "Montana",
      "586": "Solstice",
      "612": "Sunfire",
      "637": "Torrent",
      "680": "Vibe",
      "770": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Porsche": {
    "brand_id": 59,
    "models": {
      "59": "911",
      "60": "928",
      "63": "968",
      "139": "Boxster",
      "172": "Carrera ",
      "176": "Cayenne",
      "177": "Cayman",
      "476": "Panamera",
      "771": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Proton": {
    "brand_id": 772,
    "models": {
      "772": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10177": "415"
    }
  },
  "Renault": {
    "brand_id": 108,
    "models": {
      "108": "Avantime",
      "189": "Clio",
      "262": "Espace",
      "312": "Grand Espace",
      "314": "Grand Scenic",
      "367": "Kangoo",
      "369": "Koleos",
      "376": "Laguna",
      "416": "Megane",
      "429": "Modus",
      "517": "R19",
      "561": "Safrane",
      "565": "Scenic",
      "643": "Trafic",
      "660": "Twingo",
      "673": "Vel Satis",
      "773": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10010": "Symbol",
      "10012": "Logan",
      "10019": "Sandero",
      "10332": "Duster",
      "10333": "Fluence",
      "10334": "Latitude",
      "10351": "Sandero Stepway"
    }
  },
  "Rolls Royce": {
    "brand_id": 206,
    "models": {
      "206": "Corniche",
      "480": "Park Ward ",
      "491": "Phantom",
      "582": "Silver Seraph",
      "774": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Rover": {
    "brand_id": 17,
    "models": {
      "17": "200",
      "20": "25",
      "38": "45",
      "52": "75",
      "421": "Mini",
      "606": "Streetwise",
      "775": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Saab": {
    "brand_id": 61,
    "models": {
      "61": "9-3",
      "62": "9-5",
      "776": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10178": "9000"
    }
  },
  "Santana": {
    "brand_id": 777,
    "models": {
      "777": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Saturn": {
    "brand_id": 10179,
    "models": {
      "10179": "Vue"
    }
  },
  "Seat": {
    "brand_id": 87,
    "models": {
      "87": "Alhambra",
      "90": "Altea",
      "95": "Arosa",
      "205": "Cordoba",
      "270": "Exeo",
      "341": "Ibiza",
      "347": "Inca",
      "388": "Leon",
      "636": "Toledo",
      "778": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10181": "Cordoba Vario"
    }
  },
  "Shuanghan": {
    "brand_id": 779,
    "models": {
      "779": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Skoda": {
    "brand_id": 276,
    "models": {
      "276": "Fabia",
      "277": "Felicia",
      "465": "Octavia",
      "499": "Praktik",
      "537": "Roomster",
      "615": "Superb",
      "780": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10044": "Yeti",
      "10182": "120 LS",
      "10183": "Fabia Combi",
      "10184": "Octavia Tour",
      "10302": "Octavia II"
    }
  },
  "Smart": {
    "brand_id": 219,
    "models": {
      "219": "crossblade",
      "284": "forfour",
      "285": "fortwo",
      "534": "roadster",
      "781": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "SsangYong": {
    "brand_id": 84,
    "models": {
      "84": "Actyon",
      "370": "Korando",
      "372": "Kyron",
      "446": "Musso",
      "530": "Rexton",
      "536": "Rodius",
      "782": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Subaru": {
    "brand_id": 115,
    "models": {
      "115": "B9 Tribeca",
      "283": "Forester",
      "346": "Impreza",
      "362": "Justy",
      "385": "Legacy",
      "471": "Outback",
      "650": "Tribeca",
      "783": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Suzuki": {
    "brand_id": 92,
    "models": {
      "92": "Alto",
      "116": "Baleno",
      "264": "Esteen",
      "315": "Grand Vitara",
      "342": "Ignis",
      "357": "Jimmy",
      "390": "Liana",
      "596": "Splash",
      "616": "Swift",
      "617": "SX4",
      "666": "Wagon",
      "684": "Vitara",
      "784": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10303": "Grand Vitara III"
    }
  },
  "Toyota": {
    "brand_id": 40,
    "models": {
      "40": "4Runner",
      "103": "Auris",
      "107": "Avalon",
      "109": "Avensis",
      "113": "Aygo",
      "164": "Camry",
      "169": "Carina",
      "180": "Celica",
      "207": "Corolla",
      "247": "Echo",
      "331": "Hiace",
      "332": "Highlander",
      "333": "Hilux",
      "351": "IQ",
      "378": "Land Cruiser",
      "407": "Mark X",
      "438": "MR 2",
      "482": "Paseo",
      "494": "Picnic",
      "503": "Previa",
      "505": "Prius",
      "525": "RAV 4",
      "528": "Reiz",
      "579": "Sienna",
      "603": "Starlet",
      "618": "Tacoma",
      "659": "Tundra",
      "662": "Urban",
      "677": "Verso",
      "710": "Yaris",
      "785": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10011": "LC 200",
      "10018": "Prado",
      "10185": "Land Cruiser Prado",
      "10186": "Lite Ace",
      "10187": "Marino",
      "10188": "Mark II",
      "10189": "Opa",
      "10190": "Platz",
      "10191": "Premio",
      "10192": "Raum",
      "10193": "Land Cruiser 100",
      "10194": "Sienta",
      "10195": "Sprinter",
      "10196": "Sprinter Carib",
      "10197": "Supra",
      "10198": "Tercel",
      "10199": "Windom",
      "10200": "Vista",
      "10201": "Vista Ardeo",
      "10202": "Ist",
      "10203": "Scepter",
      "10204": "Vitz",
      "10205": "Corona",
      "10206": "Allion",
      "10207": "Carib",
      "10208": "Chaser",
      "10209": "Ipsum",
      "10210": "Corolla Fielder",
      "10211": "Sparky",
      "10212": "Corolla Levin",
      "10213": "Corolla Runx",
      "10214": "Corolla Verso",
      "10215": "Allex",
      "10216": "Corona Premio",
      "10217": "Corona SF",
      "10218": "Cresta",
      "10219": "Crown",
      "10220": "Crown Athlete",
      "10221": "Estima Emina",
      "10222": "Funcargo",
      "10223": "Harier",
      "10224": "Hilux Surf",
      "10225": "Corolla Spacio",
      "10304": "Land Cruiser 200",
      "10305": "Sequoia II",
      "10306": "Tundra II"
    }
  },
  "Triumph": {
    "brand_id": 786,
    "models": {
      "786": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "Vauxhall": {
    "brand_id": 787,
    "models": {
      "787": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "VAZ": {
    "brand_id": 1039,
    "models": {
      "1039": "Niva",
      "10002": "2108",
      "10003": "2115",
      "10004": "2113",
      "10005": "2107",
      "10006": "2104",
      "10007": "2106",
      "10008": "2105",
      "10020": "Lada Priora",
      "10021": "LADA Kalina",
      "10022": "LADA Samara",
      "10023": "LADA Revolution",
      "10024": "1111 \u041e\u041a\u0410",
      "10025": "2101",
      "10026": "2102",
      "10027": "2103",
      "10028": "2109",
      "10029": "2110",
      "10030": "2111",
      "10031": "2112",
      "10032": "2114",
      "10033": "2121 4X4",
      "10034": "2123",
      "10035": "2129",
      "10036": "2131",
      "10037": "2328",
      "10038": "2329",
      "10042": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10227": "21124-28",
      "10228": "21074",
      "10229": "21093",
      "10230": "21102",
      "10231": "21103",
      "10232": "21104",
      "10233": "21110",
      "10234": "21114",
      "10235": "Patriot",
      "10236": "21123",
      "10237": "21713",
      "10238": "21124",
      "10239": "21011",
      "10240": "21130",
      "10241": "21134",
      "10242": "21140",
      "10243": "2120",
      "10244": "21213",
      "10245": "21214",
      "10246": "21218",
      "10247": "21703",
      "10248": "21070",
      "10249": "21723",
      "10250": "1119",
      "10251": "20102",
      "10252": "106",
      "10253": "1111",
      "10254": "11113",
      "10255": "1117",
      "10256": "11174",
      "10257": "1118",
      "10258": "21015",
      "10259": "11184",
      "10260": "21065",
      "10261": "11193",
      "10262": "21010",
      "10263": "21013",
      "10264": "210210",
      "10265": "21043",
      "10266": "21051",
      "10267": "21053",
      "10268": "11194",
      "10269": "21061",
      "10270": "21063",
      "10271": "11183"
    }
  },
  "VIS": {
    "brand_id": 10272,
    "models": {
      "10272": "2345",
      "10273": "23472"
    }
  },
  "Volvo": {
    "brand_id": 149,
    "models": {
      "149": "C30",
      "153": "C70",
      "218": "Cross Country",
      "550": "S40",
      "556": "S60",
      "557": "S70",
      "559": "S80",
      "663": "V40",
      "664": "V50",
      "665": "V70",
      "697": "XC 60",
      "698": "XC 70",
      "699": "XC 90",
      "789": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c",
      "10274": "740",
      "10275": "440",
      "10276": "850"
    }
  },
  "Vortex": {
    "brand_id": 10277,
    "models": {
      "10277": "Estina"
    }
  },
  "Wartburg": {
    "brand_id": 10226,
    "models": {
      "10226": "1.3"
    }
  },
  "Wiesmann": {
    "brand_id": 788,
    "models": {
      "788": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "ZAZ": {
    "brand_id": 10294,
    "models": {
      "10294": "1102",
      "10295": "Chance"
    }
  },
  "ZIL": {
    "brand_id": 10296,
    "models": {
      "10296": "130"
    }
  },
  "\u0410\u0417\u041b\u041a": {
    "brand_id": 10316,
    "models": {
      "10316": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "\u0418\u0416": {
    "brand_id": 10320,
    "models": {
      "10320": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "\u041c\u043e\u0441\u043a\u0432\u0438\u0447": {
    "brand_id": 10297,
    "models": {
      "10297": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "\u0423\u0410\u0417": {
    "brand_id": 10318,
    "models": {
      "10318": "\u0414\u0440\u0443\u0433\u0430\u044f \u043c\u043e\u0434\u0435\u043b\u044c"
    }
  },
  "\u0414\u0440\u0443\u0433\u0430\u044f": {
    "brand_id": 9999,
    "models": {
      "9999": "\u043d\u0435 \u0432 \u0441\u043f\u0438\u0441\u043a\u0435"
    }
  }
}