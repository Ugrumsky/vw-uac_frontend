
//popups
$(function() {

  $('.js_popup-trigger').on('click',function(e){
    e.preventDefault();
    e.stopPropagation();

    $('.js_popup').removeClass('is_active');
    
    var targetData = $(this).data('target');
    var target = $('.js_popup[data-name="' + targetData +'"]')
    target.addClass('is_active');
    $('body').addClass('scroll-lock');
  })

  $('.js_popup-close-btn').on('click', function() {
    $(this).closest('.js_popup').removeClass('is_active');
    $('body').removeClass('scroll-lock');
  })

})
