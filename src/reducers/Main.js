
const initialState = {
    currentSelectedDealerId: null,
    declinedOffersAreHidden: false,
    offerCardTooltipInfo: false,
    lastVisit: 1497370514,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case 'CHANGE_DEALER':
            return Object.assign({}, state, {
                currentSelectedDealerId: action.payload
            });
        case 'FILTER_DECLINED_OFFERS':
            return Object.assign({}, state, {
                declinedOffersAreHidden: !state.declinedOffersAreHidden
            });
        case 'TOGGLE_OFFER_CARD_TOOLTIP':
            return Object.assign({}, state, {
                offerCardTooltipInfo: action.payload
            });
        default:
            return state;
    }
}