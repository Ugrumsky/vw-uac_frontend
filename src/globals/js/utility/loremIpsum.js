/**
 * Created by ufs on 3/22/2017.
 */

import lorems from '../../data/lorem.json';

const len = lorems.length;


module.exports = {
  default(max = 0) {
    const randomLorem = lorems[Math.floor(Math.random() * len)];
    const maxLength = (!max || max > randomLorem.length) ? randomLorem.length - 1 : max;
    const strLength = parseInt(Math.floor(Math.random() * maxLength) + maxLength*0.4);
    return randomLorem.slice(0, strLength);
  },
  list(max = 0) {
    const randomLoremArr = lorems[Math.floor(Math.random() * len)].split('.');
    const maxLength = (!max || max > randomLoremArr.length) ? randomLoremArr.length - 1 : max;
    const arrLength = parseInt(Math.floor(Math.random() * maxLength) + maxLength*0.4);
    return randomLoremArr.slice(0, arrLength);
  },
};
