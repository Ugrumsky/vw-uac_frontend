import Component, {findComponent} from '../../../globals/js/Libraries/component';
import Page from '../../../globals/js/Libraries/page';
import $ from 'jquery';


export default class TopLoader extends Component {
  constructor(el) {
    super(el);
    this.color = false;
    this.el = el;
    this.$el = $(el);
    this.$runner = this.$el.find('#js_top-loader__bar');
    }
    show(){
        this.$el.removeClass('is_hidden');
    }

    hide(){
        this.$el.addClass('is_hidden');
    }
    
    setBarWidth(percentComplete) {
        this.$runner.css({
            width: percentComplete * 100 + '%'
        });
    }
  }

  findComponent('#js_top-loader__container', TopLoader);