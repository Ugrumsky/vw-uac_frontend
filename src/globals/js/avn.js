require ('../../../node_modules/chosen-js/chosen.jquery.js');
require ('expose-loader?mCustomScrollbar!../../../node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js');
require ('expose-loader?noUiSlider!../../../node_modules/nouislider/distribute/nouislider.js');
require ('expose-loader?wNumb!../../../node_modules/wnumb/wNumb.js');
require ('../../../node_modules/jquery-lazyload/jquery.lazyload.js');
require ('../../../node_modules/jquery-mask-plugin/dist/jquery.mask.min.js');
require ('../../../node_modules/object-fit-images/dist/ofi.min.js');
require('../../components/avn/auto-card/index.js');
require('../../components/avn/disclaimer/index.js');
require('../../components/avn/range_slider/index.js');
require('../../components/avn/popup/index.js');
require('../../components/avn/accordion/index.js');



$(document).on('click', '.js_scroll-to-top-btn', function(e){
  $('body, html').stop().animate({scrollTop:0}, '300');
})

$(document).on('scroll touchmove',function(){
  if($(this).scrollTop() > window.innerHeight){
  $('.js_scroll-to-top-btn').addClass('is_visible');
  }
  else{
    $('.js_scroll-to-top-btn').removeClass('is_visible');
  }
})


function isIE(userAgent) {
  userAgent = userAgent || navigator.userAgent;
  return userAgent.indexOf("MSIE ") > -1 || userAgent.indexOf("Trident/") > -1 || userAgent.indexOf("Edge/") > -1;
}

$(function(){
	if (isIE()){
		$('html').addClass('ie-browser');
	}
})




//chosen-selects

$(".js_chosen_select").chosen({
  disable_search_threshold: 100,
})
mobileChosenUpdate()

function mobileChosenUpdate(){
  $(".js_chosen_select").each(function(){
    if (typeof($(this).data("chosen")) == 'undefined'){
      $(this).parent().addClass('no-chosen-inited');
      $(this).parent().find('.js_simple-select--chosen-not-inited').text($(this).find("option:selected").text())
    }
    $(this).on('change',function(){
      $(this).parent().find('.js_simple-select--chosen-not-inited').text($(this).find("option:selected").text())
    })
  })
}


// cookies lib
window.getCookie = function(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

window.setCookie = function (name, value, options) {
  options = options || {};

  var expires = options.expires;

  if (typeof expires == "number" && expires) {
    var d = new Date();
    d.setTime(d.getTime() + expires * 1000);
    expires = options.expires = d;
  }
  if (expires && expires.toUTCString) {
    options.expires = expires.toUTCString();
  }

  value = encodeURIComponent(value);

  var updatedCookie = name + "=" + value;

  for (var propName in options) {
    updatedCookie += "; " + propName;
    var propValue = options[propName];
    if (propValue !== true) {
      updatedCookie += "=" + propValue;
    }
  }

  document.cookie = updatedCookie;
}




//custom scroll
function customScrollInit () {
    $(".js_custom-scroll").mCustomScrollbar({
        advanced:{
            updateOnContentResize: true,
            setTop: 0,
            axis: "y"
        },
    });
}
$(function(){
  customScrollInit();
})


$(function(){

  $("img.lazy").lazyload({
   threshold: 400
  });
})




$("#result_container").on("DOMNodeInserted", function () {

 $("img.lazy").lazyload({
   threshold: 400
 });
});