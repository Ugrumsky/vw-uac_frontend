import $ from 'jquery';
import _debounce from 'lodash/debounce';
import Media from '../../../../globals/js/media.js';
import './index.scss';
import Modernizr from 'modernizr';

function updateMainstageHeight() {
  let wH = $(window).height();
  let wW = $(window).width();
  let stageItems = $('.vw_m301_stage_item_img_container');
  let stageH = wH * 0.8;
  let minStageH = 480;

  if(wW > Media.breakpoints.laptop) {
    stageItems.height(stageH < minStageH ? minStageH : stageH);
  } else {
    stageItems.css('height', '');
  }
}

$(() => {
  if (!Modernizr.cssvhunit) {
    $(window).resize(_debounce(updateMainstageHeight, 100));
    $(document).ready(updateMainstageHeight);

    if(process.env.NODE_ENV === 'development') {
      console.warn('vhunit polyfilled: m301_Homepage_Stage');
    }
  }
});


import 'globals/js/fit-text.js';

$('.js_responsive_header').kdxStageText();
