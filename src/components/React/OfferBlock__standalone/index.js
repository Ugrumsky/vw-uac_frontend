import React from 'react';
import {connect} from 'react-redux';
import * as actions from '../../../actions/index';
import _filter from 'lodash/filter';
import _sortBy from 'lodash/sortBy';
import _countBy from 'lodash/countBy';
import _intersection from 'lodash/intersection';
import ToggleSwitch from '../ToggleSwitch/index';
import OfferList from '../OfferList/index';
import OfferTagFilter from '../OfferTagFilter/index';

let classNames = require('classnames');

const mapStateToProps = (state) => {
    return {
        offerList: state.OfferList,
        main: state.Main,
        tags: state.TagFilter
    };
}
const mapActionsToProps = (dispatch) => {
    return {
        filterDeclinedOffers: (data) => {
            dispatch(actions.filterDeclinedOffers(data));
        },
    };
};

@connect(mapStateToProps, mapActionsToProps)
class OfferBlockStandalone extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    handleToggleSwitch = () => {
        this.props.filterDeclinedOffers();
    };


    render() {
        const offerList = this.props.offerList;
        let content = null;
        let offers;
        /*filter by block type*/
        if (this.props.dealerId) {
            offers = _filter(offerList, (o) => {
                return o.offerOwnDealer == this.props.dealerId;
            });
        }
        if (this.props.requestId) {
            offers = _filter(offerList, (o) => {
                return o.offerOwnRequest == this.props.requestId;
            });
        }
        if (this.props.lastNum) {
            offers = _sortBy(offerList, [function (o) {
                return o.offerCreationTime;
            }]);
            offers = offers.slice(0, this.props.lastNum);
        }

        /*filter Declined*/
        if (offers.length != 0) {
            let hasDeclined = _filter(offers, (o) => {
                return o.offerUserDecisionStatus == 'fail';
            });

            let declinedToggler;
            if (hasDeclined.length != 0) {
                declinedToggler = <ToggleSwitch
                    label='Скрыть отклоненные'
                    checked={this.props.main.declinedOffersAreHidden}
                    onChange={() => {
                        this.handleToggleSwitch()
                    }}/>
            }
            else {
                declinedToggler = null;
            }

            if (this.props.main.declinedOffersAreHidden) {

                offers = _filter(offers, (o) => {
                    return o.offerUserDecisionStatus !== 'fail';
                });
            }

            /*get filter numbers*/

            let jointTagsArr = [];
            offers.forEach((offer) => {
                jointTagsArr = [...jointTagsArr, ...offer.offerTags];
            })
            const offerTagsCount = (_countBy(jointTagsArr));

            /*Tag Filtering*/
            let selectedTagsArr = [];
            this.props.tags.forEach((tag) => {
                if (tag.isChecked) {
                    selectedTagsArr.push(tag.value);
                }
            })
            if (selectedTagsArr.length > 0) {
                offers = offers.filter((item) => {

                    return _intersection(item.offerTags, selectedTagsArr).length > 0;
                });
            }
            content = <div>
                <div className="headline-extended-flex mb20">
                    <div className="h3">Предложения</div>
                    {declinedToggler}
                </div>
                <OfferTagFilter
                    tags={this.props.tags}
                    offerTagsCount={offerTagsCount}/>
                <OfferList offers={ offers}/>
            </div>;

        }


        return (
        offers.length != 0 && this.props.withHeadline ?
            <OfferBlockStandaloneTitleWrap>
                {content}
            </OfferBlockStandaloneTitleWrap>:
            <div>
                {content}
            </div>


        );
    }
}

export default OfferBlockStandalone;

const OfferBlockStandaloneTitleWrap = (props) => {

    return (
        <div
            className="timeline__block timeline__block_basic timeline__block_full-width no-left-col-mobile">
            <div className="timeline__block-left-part">
            </div>
            <div className="timeline__block-right-part">
                <div className="timeline__block-content">
                    {props.children}
                </div>
            </div>
        </div>
    );

}
