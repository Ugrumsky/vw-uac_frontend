
import {tagList} from '../globals/mocks/reactMocks.js';

const initialState = tagList;

export default (state = initialState, action) => {
    switch (action.type) {
        case 'UPDATE_OFFER_FILTER_TAGS':
            return action.payload
            break;

        default:
            return state;
    }
}

