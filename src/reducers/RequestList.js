const dotProp = require('dot-prop-immutable');
import {requestList} from '../globals/mocks/reactMocks.js';

export default (state = requestList, action) => {
    switch (action.type) {
        case 'RESOLVE_STEP':
            return {
                ...dotProp.set(state, `${action.payload.requestId}.requestInfo.requestStepStatus`, action.payload.userDecision)
            }

            break;

        case 'RESOLVE_STEP_RATING':
            return {
                ...dotProp.merge(state, `${action.payload.requestId}.requestInfo`, {requestRating: action.payload.rating,
                    requestStepStatus: 'success'})
            }

            break;

        default:
            return state;
    }
}

