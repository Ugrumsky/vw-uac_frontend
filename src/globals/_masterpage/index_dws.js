import $ from 'jquery';

require('../dws_globals');
require('./index.scss');


if (module.hot) {
  module.hot.accept();
}

// show errors on form elements
$('.js_masterpage_add_error').children().addClass('has-error');

