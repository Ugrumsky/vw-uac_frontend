import Component, {findComponent} from '../../../globals/js/Libraries/component';
import Page from '../../../globals/js/Libraries/page';
import $ from 'jquery';


export default class SysNotification extends Component {
  constructor(el) {
    super(el);
    this.color = false;
    this.el = el;
    this.$el = $(el);
    this.$closeBtn = this.$el.find('.js_system-notification__close-btn');
    this.$content = this.$el.find('.js_system-notification__content');
    this.init();
    }
    init() {
        this.$closeBtn.click(
            (e) => {
                this.hide();
            });
    }

    show(){
        this.$el.addClass('is_visible');
    }

    hide(){
        this.$el.removeClass('is_visible');
    }

    updateContent(newContent){
        this.$content.empty();
        this.$content.append(newContent);
    }

    hideOnTimeout(delay){
        setTimeout((e)=>{
            this.hide();
        }, delay)
    }
  }

  findComponent('#js_system-notification__container', SysNotification);