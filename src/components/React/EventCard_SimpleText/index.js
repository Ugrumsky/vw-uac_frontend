import React from 'react';

const EventCardSimpleText = (props) => {
    return (
        <div className="eventCard__simple-text">
            {props.children}
        </div>
    );
};


export default EventCardSimpleText;
