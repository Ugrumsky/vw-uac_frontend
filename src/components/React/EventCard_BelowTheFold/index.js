import React from 'react';

const EventCardBelowTheFold = (props) => {

    let innerClass= 'eventCard__above-the-fold-inner';

    if (props.padded) innerClass += ' padded-container';

    return (
        <div className="eventCard__below-the-fold">
            <div className={innerClass}>
                {props.children}
            </div>
        </div>
    );
};


export default EventCardBelowTheFold;