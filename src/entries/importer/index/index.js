
import 'globals/globals';
import './index.scss';
/***********React************/

import React from 'react';
import ReactDOM, {render} from 'react-dom';
import {defaultStore} from '../../../reducers/App.js';
import App from '../../../components/React/App/index.js';
import {Provider} from 'react-redux';


// HMR setup
if (module.hot) {
    module.hot.accept();
}

render(
    <Provider store={defaultStore}>
      <App />
    </Provider>
    ,
    document.getElementById('root')
)



/***********React************/


$(function(){
  announcementSliders.initAnnouncementSliders();
})
