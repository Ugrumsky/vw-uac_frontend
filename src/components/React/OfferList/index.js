import React from 'react';
import {connect} from 'react-redux'
import _filter from 'lodash/filter';
import * as actions from '../../../actions/index';
import OfferCardWrapper from '../OfferCard_wrapper/index';
import Media from '../../../globals/js/media.js';
import Slider from 'react-slick';
import OfferCardTooltip from '../OfferCard_Tooltip';
import Button from '../Button';

const shortid = require('shortid');

const mapStateToProps = (state) => {
    return {
        offerList: state.OfferList,
        tagList: state.TagFilter
    };
}

const mapActionsToProps = (dispatch) => {
    return {
        filterDeclinedOffers: (data) => {
            dispatch(actions.filterDeclinedOffers(data));
        },
        updateOfferFilterTags: (value) => {
            dispatch(actions.updateOfferFilterTags(value));
        },
    };
};

@connect(mapStateToProps, mapActionsToProps)

class OfferList extends React.Component {

    constructor(props) {
        super(props);

    }

    componentWillMount() {
        this.offerListId = shortid.generate();
    }

    componentDidMount() {
        document.addEventListener('mobileChanged', () => {
            this.forceUpdate();
        });
    }

    resetFilters = () => {
        const tags = this.props.tagList.map((tag, i)=>{
            tag['isSelected'] = false;
            tag['isChecked'] = false;
            console.log(tag);
            return tag
        })
        this.props.filterDeclinedOffers();
        this.props.updateOfferFilterTags(tags)
    }


    render() {
        let offerListClassName = 'Offerlist';
        if (this.props.activeTooltipParentId == this.offerListId) {
            offerListClassName += ' tooltip-is-shown'
        }
        let offers;

        /*if it's not in RequestCard - Offerblock_standalone does the filtering*/
        if (!this.props.offers && this.props.requestId) {
            offers = _filter(this.props.offerList, (o) => {
                return o.offerOwnRequest == this.props.requestId;
            });
        }
        else {
            offers = this.props.offers;
        }

        let content = offers.map((offer, i) => {

            return (
                <div key={shortid.generate()} className='offerCard_slide'>
                    <OfferCardWrapper offerData={offer} parentOfferListId={this.offerListId}/>
                </div>

            );
        });

        const sliderSettings = {
            dots: true,
            infinite: false,
            arrows: false,
            speed: 300,
            initialSlide: 0,
            transformEnabled: false,
            useTransform: false,
            //centerMode: true,
            slidesToShow: 1,
            //centerPadding: '2rem',
            variableWidth: true,
            //adaptiveHeight: true,
            //slidesToShow: 2,
            //swipeToSlide: true,
        };

        if (content.length == 0) {
            content = <div className="gridcontainer midpage-message fade-in">
                <div className="grid_12 txt_center">
                    <div className="midpage-message__text">Подходящих предложений не найдено
                    </div>
                    <Button
                        className="midpage-message__btn btn_cta"
                        tagName="div"
                        text="Сбросить фильтры"
                    onClick={()=>{this.resetFilters()}}/>
                </div>
            </div>
        }
        else if (Media.isMobile && content.length > 1) {
            content = <Slider {...sliderSettings}>
                {content}
            </Slider>;
        }

        let head;

        if (this.props.hasPreText) {
            head = <div className="timeline__block-no-card-content">
                <div className="timeline__block-content-simpleText">Вы получили {offers.length} новых предложения
                </div>
            </div>;
        }


        return (
            <div className={offerListClassName} id={this.offerListId}>
                <OfferCardTooltip parentOfferListId={this.offerListId}/>
                {head}
                <div className='offerCard_sliderContainer carousel'>
                    {content}
                </div>

            </div>
        );
    }
}

export default OfferList;