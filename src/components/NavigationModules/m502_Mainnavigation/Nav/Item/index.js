export default function itemParse(el) {
  return {
    el: el,
    active: /(^| )is-active($| )/g.test(el.className),
    href: el.getElementsByClassName('vw_m502_btn')[0].href,
    icon: el.getElementsByClassName('vw_m502_icon')[0].className
        .replace(new RegExp('.*(icon-navigation-.*)', 'g'), '$1'),
    label: el.getElementsByClassName('vw_m502_link_label')[0].innerHTML,
  };
}
