/**
 * Created by Kodix on 07.03.2017.
 */
// Carousel script
import 'imports-loader?jQuery=jquery!slick-carousel';
import 'slick-carousel/slick/slick.scss';

// Libs
import $ from 'jquery';
import _defaultsDeep from 'lodash/defaultsDeep';
import _isArray from 'lodash/isArray';
import _filter from 'lodash/filter';

// UI Elements
import dot from 'components/UI_Elements/pagination/dots';
import arrow from 'components/UI_Elements/pagination/arrows';

export default (() => {
  const DEFAULTS = {
    // arrows config
    arrows: true,
    appendArrows: false,
    prevArrow: arrow.left,
    nextArrow: arrow.right,
    // dots config
    dots: true,
    appendDots: false,
    dotsClass: 'vwd5_dots',
    customPaging(slider, i) {
      return $(dot).text(i + 1);
    },
    mobileFirst: true,
    cssEase: 'cubic-bezier(0.215, 0.61, 0.355, 1)',
    centerMode: false,
    centerPadding: '0',
    /* dont forget to append dots and arrows
     * using appendDots and appendArrows
     */
  };

  const SLIDER_EVENTS = [
    'afterChange',
    'beforeChange',
    'breakpoint',
    'destroy',
    'edge',
    'init',
    'reInit',
    'setPosition',
    'swipe',
    'lazyLoaded',
    'lazyLoadError',
  ];

  function checkEvent(eventName) {
    let eventNameSplitted = eventName.split(' ');
    if (!_isArray(eventNameSplitted)) {
      eventNameSplitted = [eventNameSplitted];
    }

    eventNameSplitted = _filter(eventNameSplitted, (e) => {
      if (SLIDER_EVENTS.indexOf(e) !== -1) {
        return true;
      } else {
        console.warn('[SLIDER]: Event with this name not found');
        return false;
      }
    });
    return eventNameSplitted.length > 0 ? eventNameSplitted.join(' ') : false;
  }

  /**
   *
   * @this Slider
   * @param type
   * @param eventName
   * @param callback
   */
  function eventHandle(type, eventName, callback) {
    const checkedEvent = checkEvent(eventName);
    if (checkedEvent) {
      switch (type) {
        case 'on':
          this.element.on(checkedEvent, callback);
          break;
        case 'one':
          this.element.one(checkedEvent, callback);
          break;
        case 'off':
          this.element.off(checkedEvent, callback);
          break;
        default:
          console.warn('[SLIDER]: Event assign type not found');
      }
    }
  }

  class Slider {
    constructor(element, options) {
      this.element = element;
      this.options = _defaultsDeep(options, DEFAULTS);
      this.active = false;
    }

    mount() {
      if (!this.active) {
        this.active = true;
        this.element.slick(this.options);
      }
    }

    unmount() {
      if (this.active) {
        this.active = false;
        this.element.slick('unslick');
      }
    }

    reInit() {
      this.unmount();
      this.mount();
    }

    updateOption(name, value, ui = false) {
      if(this.active) {
        this.element.slick('slickSetOption', name, value, ui);
      }
    }

    on(eventName, callback) {
      eventHandle.call(this, 'on', eventName, callback);
    }

    one(eventName, callback) {
      eventHandle.call(this, 'one', eventName, callback);
    }

    off(eventName, callback) {
      eventHandle.call(this, 'off', eventName, callback);
    }

    current() {
      if (this.active) {
        return this.element.slick('slickCurrentSlide');
      } else {
        return false;
      }
    }

    goTo(index) {
      if (this.active) {
        this.element.slick('slickGoTo', index);
      }
    }

    syncTo(slider) {
      // todo get active set active
      const self = this;
      if (this.active) {
        if (slider.current() !== false && slider.current() !== this.current()) {
          this.goTo(slider.current());
        }
        if (slider instanceof Slider) {
          slider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
            self.goTo(nextSlide);
          });
        } else {
          console.warn(`Argument must be Slider Instance`);
        }
      }
    }
  }

  return Slider;
})();

