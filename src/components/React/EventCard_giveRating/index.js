import React from 'react';

let classNames = require('classnames');
const shortid = require('shortid');

class EventCardGiveRating extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            currentRating: this.props.currentRating,
            isActive: this.props.isActive,
        };
    }

    handleMouseEnter = (index) => {

        this.setState({
            currentRating: index + 1,
        });

    };

    handleMouseLeave = (index) => {
        if (this.state.isActive) {
            this.setState({
                currentRating: 0,
            });
        }
    };

    handleMouseClick = (index) => {
        this.props.onClick(index);
        this.setState({
            isActive: false,
        })

    };

    render() {
        let stars = [];

        for (let i = 0; i < this.props.maxRating; i++) {

            let starClass = 'giveRating__item hasicon icon-base-favourite';

            if (i < this.state.currentRating) {
                starClass += ' marked';
            }

            stars.push(
                this.state.isActive ?
                    <li key={shortid.generate()} className={starClass}
                        onMouseEnter={() => {
                            this.handleMouseEnter(i)
                        }}
                        onMouseLeave={() => {
                            this.handleMouseLeave(i)
                        }}
                        onClick={() => {
                            this.handleMouseClick(i)
                        }}></li> :
                    <li key={shortid.generate()} className={starClass}></li>
            );
        }

        let title;

        if (this.props.stepStatus  == 'success' && !this.state.isActive) {
            title = 'Ваша оценка:';
        }
        else {
            title = 'Пожалуйста, оставьте вашу оценку!';
        }


        return (
            <div className="eventCard__flex-container">
                <span className="eventCard__simple-inline-text">{title}</span>
                <div className="giveRating__container">
                    <input type="hidden" value=""/>
                    <ul className="giveRating">
                        {stars}
                    </ul>
                </div>
            </div>
        );
    }
}


export default EventCardGiveRating;
