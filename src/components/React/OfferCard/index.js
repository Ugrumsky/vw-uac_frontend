import React from 'react';
import * as actions from '../../../actions/index';
const classNames = require('classnames');
const wNumb = require('wnumb');
import {connect} from 'react-redux';
import carImg from '../../../_temp_images/passat-alltrack.png';

const moneyFormat = wNumb({
    mark: '.',
    thousand: ' ',
    suffix: ' руб.',
});

class OfferCard extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    handleDeclineBtnClick = () => {
        this.context.onClick();
    };

    handleAcceptBtnClick = () => {
        this.context.onClick(false);
        this.props.resolveOffer({
            offerId: this.props.offerData.offerId,
            userDecision: 'success',
            declineCause: false,
        });
    };

    toggleTooltip = (data) => {
        this.props.toggleOfferCardtooltip(data)
    }

    render() {
        const offerData = this.props.offerData;
        const status = offerData.offerUserDecisionStatus;
        let acceptBtnClass = 'offer-card__btn offer-card__btns-accept';
        let declineBtnClass = 'offer-card__btn offer-card__btns-decline';
        let overlayClass = 'offer-card__overlay';
        let statusIndicatorClass = classNames('offer-card__booking', offerData.offerBookingStatus);

        if (status == 'success') {
            acceptBtnClass += ' is_active';
            overlayClass += ' is_visible';
        }
        else if (status == 'fail') {
            declineBtnClass += ' is_active';
        }

        const statusIndicatorText = {
            'dealer': 'в наличии',
            'warehouse': 'на центральном складе',
            'warehouse-dealer': 'на складе дилера',
            'assembly': 'сборка',
        }


        return (
            <div className="offer-card__container">
                <div className={overlayClass}>
                    <div className="offer-card__overlay-content">
                        <div className="offer-card__overlay-text">Отличный выбор! Персональный менеджер скоро свяжется с
                            вами!
                        </div>
                    </div>
                </div>
                <div className="offer-card__inner">
                    <div className="offer-card__inner-left-col">
                        <div className="offer-card__status-block">
                            <div className={statusIndicatorClass}>
                                <div
                                    className="offer-card__booking-status">{statusIndicatorText[offerData.offerBookingStatus]}</div>
                                <div className="offer-card__booking-date">{offerData.offerBookingDate}</div>
                            </div>
                            <div className="offer-card_car-title">{offerData.offerModelName}</div>
                        </div>
                        <div className="offer-card__price-block">
                            <div className="offer-card__col">
                                <div
                                    className="offer-card__price offer-card__price_crossed">{moneyFormat.to(offerData.offerPrice)}</div>
                                <div className="offer-card__price">{moneyFormat.to(offerData.offerDiscountPrice)}</div>
                            </div>
                            <div className="offer-card__col">
                                <div className="offer-card__price-tooltip-trigger"
                                     onClick={(event) => {
                                         const offsetTop = $(event.target).closest('.js_eventCard').position().top + $(event.target).closest('.offerCard_sliderContainer').position().top
                                         this.props.toggleTooltip({
                                             'showTooltip': true,
                                             'coords': {
                                                 'top': event.target.offsetTop,
                                                 'left': event.target.offsetLeft
                                             },
                                             'parentOfferList': this.props.parentOfferListId,
                                             'offsetTop': offsetTop,
                                             'offerId': offerData.offerId,
                                         })
                                     }}
                                     /*onMouseLeave={(event) => {
                                         this.props.toggleTooltip({
                                             'showTooltip': false
                                         })
                                     }}*/
                                >?</div>
                            </div>
                        </div>
                        <div className="offer-card__picture-block"><img src={carImg} alt=""/></div>
                        <div className="offer-card__btns-block">
                            <div className={acceptBtnClass} onClick={this.handleAcceptBtnClick}>
                                <div className="offer-card__btn-icon hasicon icon-base-checkmark"></div>
                                <div className="offer-card__btn-text">Принять</div>
                            </div>
                            <div className={declineBtnClass} onClick={this.handleDeclineBtnClick}>
                                <div className="offer-card__btn-icon hasicon icon-base-close"></div>
                                <div className="offer-card__btn-text">Отклонить</div>
                            </div>
                        </div>
                    </div>
                    <div className="offer-card__inner-right-col">
                        <div className="offer-card__picture-block"><img src={carImg} alt=""/></div>
                    </div>
                </div>
            </div>
        );
    }
}

OfferCard.contextTypes = {
    onClick: React.PropTypes.func,
}

export default OfferCard;


