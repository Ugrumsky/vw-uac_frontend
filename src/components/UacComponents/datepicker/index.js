const flatpickr = require('flatpickr');
const Russian = require("flatpickr/dist/l10n/ru.js").ru;
// or.. import {ru} from "flatpickr/dist/l10n/ru.js"

document.addEventListener('DOMContentLoaded', (event) => {
	initDatepickers();
});

let timePickerConfig = {
	"animate": false,
	"locale": Russian,
	enableTime: true,
	noCalendar: true,
	time_24hr: true,
}
let datePickerConfig = {
	"animate": false,
	"locale": Russian,
	'dateFormat': 'd.m.Y'
}

export let initDatepickers = function(){
	let touchsupport = ('ontouchstart' in window) || (navigator.maxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0);
	let elements = $('.js_datepicker');
	elements.each(function(){
		if(!$(this).hasClass('inited')){
			$(this).addClass('inited');
			if (!touchsupport) {
				if($(this).data('timepicker')){
				    $(this).flatpickr(timePickerConfig);
				}
				else{
				    $(this).flatpickr(datePickerConfig);
				}
			  }
			else{
				$(this).closest('.input-datepicker__container').addClass('native-datepicker')
			}
		}
	})
} 

$(document).on('change', '.input-datepicker__container input',  function(){
	let cont = $(this).closest('.input-datepicker__container');
	cont.removeClass('placeholder')
})


$(document).on('change', '.input-datepicker__touch-self',  function(){
	let val = $(this).val();
	let inp = $(this).parent().find('.input-datepicker__touch');
	inp.val(val);

	let finalValInput = $(this).parent().find('.js_final-val');
	finalValInput.val($(this).val()).trigger('change');

})


$(document).on('change', '.js_datepicker',  function(){
	let finalValInput = $(this).parent().find('.js_final-val');
	finalValInput.val($(this).val()).trigger('change');

})


$(document).on('click', '.js_datepicker',  function(){
	$(this)[0]._flatpickr.open(); //Костыли-костылики
})
