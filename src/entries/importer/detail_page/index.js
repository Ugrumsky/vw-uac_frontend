import 'globals/globals';
import './index.scss';
import $ from 'jquery';

require('../../../globals/js/avn.js');


import Tabs from '../../../components/avn/tabs/index.js';

Tabs()

$(function () {
  $('.js_phone-mask').mask('+7 (000) 000-00-00',{placeholder: "+7 (___) ___-__-__"});
})


$('#js_car-details-main-carousel').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true
});


function initSlider() {
  if(window.matchMedia("(max-width: 511px)").matches) {
    $('.js_slick-mobile').not('.slick-initialized').slick({
        dots: true,
        infinite: true,
        arrows: false,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true,
    });
  } else {
    if ($('.js_slick-mobile.slick-initialized').length > 0){
      $('.js_slick-mobile').slick('unslick')
    }
  }
}

initSlider();

$(window).on('resize', initSlider);


$(document).on('click','.js_car-details-main-carousel__slide-options-disclaimer-trigger',function(){
$(this).parent().toggleClass('is_visible');
})


$(document).on('click','.js_scroll-to-form', function(e) {
  e.preventDefault();
  var coord = $("#order-now-block").offset().top - 70;
    $('body, html').stop().animate({scrollTop:coord}, '300');
})





/*no-dealer*/





require('../../../../node_modules/js-marker-clusterer');
require ('../../../../node_modules/chosen-js/chosen.jquery.js');


let userCityId ;

if(typeof(getCookie('avnDealerCity')) == 'undefined'){
  userCityId = 'all';
}
else{
  userCityId = getCookie('avnDealerCity')
}

/*
import {dealerData} from '../../../globals/mocks/dealerMocks.js';
window.dealerData = dealerData;
 userCityId = 'all'
*/


let markersAll = [];
let dealersAll = [];
let showAllDealers = userCityId == 'all' ? true : false;
let markerCluster;
let currentSelectedDealerId = '';

/*************GOOGLE***************/

map = false;
markers = {};

export function markerClickHandler(dealer){
    $('[name=dealer]').val(dealer.XML_ID);
    $('#js_dealers_map').find('.js_map-confirm-dealer-selection').remove()
    if( currentSelectedDealerId == dealer.XML_ID){
       makeMapConfirm(dealer, true);
    }
    else{
      makeMapConfirm(dealer, false);
    }

}

export function setMarker(lat, lng, title){
    if(typeof(lat) == 'undefined'){
        lat = 55.714916;
    }
    if(typeof(lng) == 'undefined'){
        lng = 37.718094;
    }
    if(typeof(title) == 'undefined'){
        title = '';
    }
    if(typeof(map) == 'undefined'){
        return false;
    }
    let markerIcon =  {
        
        url:'data:image/svg+xml;base64,PHN2ZyBpZD0iTGF5ZXJfMSIgZGF0YS1uYW1lPSJMYXllciAxIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAzMyA0NCI+PGRlZnM+PHN0eWxlPi5jbHMtMXtmaWxsOiMwMDk5ZGE7c3Ryb2tlOiNmZmY7c3Ryb2tlLXdpZHRoOjEuNXB4O30uY2xzLTJ7ZmlsbDojZmZmO308L3N0eWxlPjwvZGVmcz48dGl0bGU+QXJ0Ym9hcmQgMTwvdGl0bGU+PHBhdGggY2xhc3M9ImNscy0xIiBkPSJNMzEuOCwxNi40QTE1LjQsMTUuNCwwLDAsMCwxLDE2LjRDMSwzMC40LDE2LjQsNDMsMTYuNCw0M1MzMS44LDMwLjQsMzEuOCwxNi40Ii8+PGNpcmNsZSBjbGFzcz0iY2xzLTIiIGN4PSIxNi40IiBjeT0iMTYuMzgiIHI9IjQuNSIvPjwvc3ZnPg==',
       size: new google.maps.Size(33, 44),
       //scaledSize: new google.maps.Size(33,44),
        origin: new google.maps.Point(0, 0),
        //anchor: new google.maps.Point(0, 44),
        scaledSize: new google.maps.Size(33, 44),
    }
    var marker = new google.maps.Marker({
        position: {lat: parseFloat(lat), lng: parseFloat(lng)},
        title: title,
        icon: markerIcon,
        optimized: false,
    });
    return marker;
}

    function drawDealerMarkers(map) {
      if (typeof(map) == 'undefined') {
        return false;
      }
      $.each(dealerData, function(cityId, data){
         if (cityId != ''){
            markers[cityId] = [];
            $.each(data.DEALERS, function(i, dealer){
                marker = setMarker(dealer.LATITUDE, dealer.LONGITUDE, dealer.NAME);
                marker.addListener('click', function() {
                    markerClickHandler(dealer)
                });
                markers[cityId].push(marker);
                markersAll.push(marker)
                dealersAll.push(dealer)
            });
          }
      });

    }

    function clearMap(){
      $.each(markers, function(i,d){
        d.setMap(null);
      })
      markers = [];
    }

export function getBoundsFromMarkers(mrks) {
    if(typeof(mrks) == 'undefined'){
        mrks = [];
    }
    var bound = new google.maps.LatLngBounds();
    $.each(mrks, function(i, marker){
        bound.extend(marker.getPosition());
    });
    return bound;
}

function setMapForCity(cityId){
    var dealers =  cityId == 'all' ? markersAll : markers[cityId];
    if(typeof(dealers) == 'undefined'){
        return false;
    }
    var bounds = getBoundsFromMarkers(dealers);
    map.fitBounds(bounds);
}

function initMap() {
    map = new google.maps.Map(document.getElementById('js_dealers_map'), {
        center: {lat: 55.714916, lng: 37.718094},
        zoom: 10,
        maxZoom: 16,
        scrollwheel: false,
    });
    drawDealerMarkers(map);
     markerCluster = new MarkerClusterer(map, markersAll, {
        styles:[
                {   
                    url: 'data:image/svg+xml;base64,PHN2ZyBpZD0iTGF5ZXJfMSIgZGF0YS1uYW1lPSJMYXllciAxIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAzMyA0NCI+PGRlZnM+PHN0eWxlPi5jbHMtMXtmaWxsOiMwMDk5ZGE7c3Ryb2tlOiNmZmY7c3Ryb2tlLXdpZHRoOjEuNXB4O30uY2xzLTJ7ZmlsbDojZmZmO308L3N0eWxlPjwvZGVmcz48dGl0bGU+QXJ0Ym9hcmQgMTwvdGl0bGU+PHBhdGggY2xhc3M9ImNscy0xIiBkPSJNMzEuOCwxNi40QTE1LjQsMTUuNCwwLDAsMCwxLDE2LjRDMSwzMC40LDE2LjQsNDMsMTYuNCw0M1MzMS44LDMwLjQsMzEuOCwxNi40Ii8+PGNpcmNsZSBjbGFzcz0iY2xzLTIiIGN4PSIxNi40MiIgY3k9IjE2LjM3IiByPSI5LjgiLz48L3N2Zz4=',
                    height: 43,
                    width: 32,
                    anchor: [-10, 0],
                    textColor: '#000',
                    textSize: 11,
                    backgroundPosition: '0 0',
                    iconAnchor: [16,44]
                  }
                ]
    });
      
}


$(function(){
  if($('#js_dealers_map').length > 0){
    initMap();
    setMapForCity(userCityId)
    makeDealerSelect(dealersAll);

    $(".js_dealer-select").chosen({
      disable_search_threshold: 15,
      no_results_text: "Ничего не найдено.",
    })
    mobileChosenUpdateDwa()
    checkForLastOrOnlyDealer()
  }
})

    function checkForLastOrOnlyDealer(){
      let dealerSelect = $('#dealer-select select')
      if (userCityId != 'all'){
        if (Object.keys(dealerData[userCityId]['DEALERS']).length == 1){
           for (key in dealerData[userCityId]['DEALERS'] ){
            dealerSelect.val(key)
            currentSelectedDealerId = key;
            $("#dealer-select .js_chosen_select").trigger("chosen:updated");
            mobileChosenUpdateDwa()
           }
        }
      }
  
    }
    function makeDealerSelect(obj) {

      let options = `<option value="default" disabled="" selected="" hidden="">Выберите дилера из списка или укажите его на карте</option>`;
      for (key in obj) {
        options = options + `<option class="select__item" value="${obj[key].XML_ID}">${obj[key].CITY + ', '  + obj[key].NAME}</option>`;
      }
      
      let select = `
        <div class="select-container js_error_container placeholder">
            <div class="simple-select--chosen-not-inited js_simple-select--chosen-not-inited"></div>
            <div class="custom-dropdown__select-arrow"></div>
            <select class="js_chosen_select js_dealer-select required" name="dealer">
            ${options}
            </select>
            <span class="select-container__help-block error-container">Пожалуйста, выберите дилера</span>
          </div>
        `
      $('#dealer-select').append(select);
    }

    $(document).on('change', '#dealer-select select', function() {
      let dealer;
      $(this).closest('.js_error_container').removeClass('placeholder')
      let val = $(this).val();
          for (i in dealerData){
            for(j in dealerData[i]['DEALERS']){
              if(dealerData[i]['DEALERS'][j]['XML_ID'] == val){
                dealer = dealerData[i]['DEALERS'][j];
            }
            }

          }
      mapPanToDealer(val)
      $('#js_dealers_map').find('.js_map-confirm-dealer-selection').remove();

      currentSelectedDealerId = val;
      setTimeout(function(){
        makeMapConfirm(dealer, true);;
      },500)

    })

    function mapPanToDealer(dealerId){
    for (i in dealerData){
            for(j in dealerData[i]['DEALERS']){
              if(dealerData[i]['DEALERS'][j]['XML_ID'] == dealerId){
                position = {
                  lat: parseFloat(dealerData[i]['DEALERS'][j]['LATITUDE']),
                  lng: parseFloat(dealerData[i]['DEALERS'][j]['LONGITUDE'])
                }
            }
            }

          }
          map.panTo(position);
          map.setZoom(16);
    }

     function mobileChosenUpdateDwa(){
      $(".js_dealer-select").each(function(){
        if (typeof($(this).data("chosen")) == 'undefined'){
          $(this).parent().addClass('no-chosen-inited');
          $(this).parent().find('.js_simple-select--chosen-not-inited').text($(this).find("option:selected").text())
        }
        $(this).on('change',function(){
          $(this).parent().find('.js_simple-select--chosen-not-inited').text($(this).find("option:selected").text())
        })
      })
    }


    function makeMapConfirm(item, dealerSelected) {

      var mapContainer = $('#js_dealers_map');
      var button = dealerSelected ? `
              <div class="mt20 js_choose-dealer-reset" data-dealerid="${item['XML_ID']}">
                <div class="btn block-centered" role="button"><span class="btn__text">Выбрать Другого</span></div>
              </div>` : `              
              <div class="mt20 js_choose-dealer-confirm" data-dealerid="${item['XML_ID']}">
                <div class="btn block-centered" role="button"><span class="btn__text">Выбрать</span></div>
              </div>`;
      var modalConfirm = `
      <div class="${dealerSelected ? 'js_map-confirm-dealer-selection map-confirm-dealer-selection dealer-selected' :'js_map-confirm-dealer-selection map-confirm-dealer-selection'}">
          <div class="map-confirm-dealer-selection__content">
              <div class="map-confirm-dealer-selection__close-btn hasicon icon-navigation-close js_map-confirm-dealer-selection__close-btn"></div>
              <div class="map-confirm-dealer-selection__name">${item['NAME']}</div>
              <div class="map-confirm-dealer-selection__address">${item['ADDRESS']}</div>
              <a href="${item['PHONE'][0].toString().replace(/-/g, '').replace(/ /g, '').replace(/\(/g,"").replace(/\)/g,"")}" class="map-confirm-dealer-selection__address">${item['PHONE'][0]}</a>
             ${button}
          </div>
      </div>`

      mapContainer.append(modalConfirm)

    }



  $(document).on('click', '.js_choose-dealer-confirm', function() {
      let dealerId = $(this).data('dealerid');
        currentSelectedDealerId = dealerId;
        $('#dealer-select select').val(dealerId).trigger('change')
        $("#dealer-select .js_chosen_select").trigger("chosen:updated");
        mobileChosenUpdateDwa()
        $(this).removeClass('js_choose-dealer-confirm').addClass('js_choose-dealer-reset')
        $(this).find('.btn__text').text('Выбрать другого');
        mapPanToDealer(dealerId)

    })

  $(document).on('click', '.js_choose-dealer-reset', function() {
        $('#dealer-select select').val('default')
        $("#dealer-select .js_chosen_select").trigger("chosen:updated");
        $('#js_dealers_map').find('.js_map-confirm-dealer-selection').remove();
        currentSelectedDealerId = '';
        mobileChosenUpdateDwa()
        setMapForCity(userCityId);
  })

    $(document).on('click', '.js_map-confirm-dealer-selection__close-btn', function() {
      $(this).closest('.js_map-confirm-dealer-selection').hide();

    })



/*CC overlay*/
    $('.js_creditcalc-trigger').on('click', function(){
      let content = $('#js_creditcalc-overlay').find('.popup__content');
      let frame = `<iframe src="${ccLink}" frameborder="0"></iframe>`;
      content.empty();
      content.append(frame);
    })

