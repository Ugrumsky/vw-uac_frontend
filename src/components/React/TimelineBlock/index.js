import React from 'react';
import {connect} from 'react-redux'
import {parsePhone} from '../../../globals/js/utility/miscUtils.js';
import Moment from 'moment';


let classNames = require('classnames');

const mapStateToProps = (state) => {
    return {
        requestList: state.DealerInfo,
        lastVisit: state.Main.lastVisit
    };
}
@connect(mapStateToProps, null)
class TimelineBlock extends React.Component {

    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {

    }


    render() {
        const props = this.props;
        const requestInfo = props.requestData.requestInfo;
        const date = Moment.unix(requestInfo.requestDate).format("DD.MM.YYYY hh:mm");
        const headText = props.hasHead ?
            <div className="timeline__block-title">{requestInfo.requestHeadText}</div> : null;

        const requestTypeClass = requestInfo.requestType ? `timeline__block_${requestInfo.requestType}` : '';

        let timelineBlockClass = classNames({
            'timeline__block': true,
            'timeline__block_basic': true,
            'timeline__block_full-width': requestInfo.requestType == 'offer',
            /*      'timeline__block_faux-left': false,
             'timeline__block_no-inner-pd': false,
             'timeline__block_no-left-col': false,*/
            'timeline__block-status-indicator_active': !(requestInfo.requestDate < props.lastVisit),
        }, requestTypeClass, props.className);

        return (
            <div className={timelineBlockClass}>
                <div className="timeline__block-left-part">
                    <div className="timeline__block-left-part-inner">
                        <div className="timeline__block-status-indicator">
                            <div className="timeline__block-status-indicator-circle"></div>
                            <div className="timeline__block-status-indicator-icon"></div>
                        </div>
                    </div>
                </div>
                <div className="timeline__block-right-part">
                    <div className="timeline__block-head">
                        <div className="timeline__block-time-indicator">{date}</div>
                        {headText}
                    </div>
                    <div className="timeline__block-content">
                        {props.children}
                    </div>
                </div>
            </div>
        );
    }
}

export default TimelineBlock;