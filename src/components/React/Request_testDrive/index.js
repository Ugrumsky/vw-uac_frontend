import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../../actions/index';

import EventCard from '../EventCard/index';
import EventCardAboveTheFold from '../EventCard_AboveTheFold/index';
import EventCardBelowTheFold from '../EventCard_BelowTheFold/index';
import EventCardBasicInfo from '../EventCard_BasicInfo/index';
import EventCardAdditionalInfo from '../EventCard_AdditionalInfo/index';
import EventCardDecisionIndicator from '../EventCard_DecisionIndicator/index';
import EventCardHistory from '../EventCard_History/index';
import EventCardDecisionBlock from '../EventCard_DecisionBlock/index';
import EventCardRatingBlock from '../EventCard_RatingBlock/index';
import EventCardForm from '../EventCard_Form/index';


let classNames = require('classnames');


class RequestTestDrive extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            currentRating: this.props.requestData.requestInfo.requestRating,
            showDeclineCause: false,
            userDecision: 'success',
            declineCause: false
        };
    }

    resolveStep = (data) => {
        this.props.resolveStep(data);
    };

    handleResetDecision = (e) => {
        this.setState({
            userDecision: 'unresolved',
        });

        this.resolveStep({
            dealerId: this.props.requestData.requestOwnDealer,
            requestId: this.props.requestData.requestId,
            userDecision: 'unresolved',
            declineCause: false,
        });
    };

    handleRatingClick = (index) => {
        this.setState({
            currentRating: index + 1,
        });
    };

    resolveStepRating = (data) => {
        this.props.resolveStepRating(data);
    };

    render() {
        const requestData = this.props.requestData;
        const requestInfo = requestData.requestInfo;

        const cardTitles = {
            'initial': 'Приглашаем вас пройти тест-драйв!',
            'invitation': 'Приглашаем вас пройти тест-драйв!',
            'passed': 'Тест-драйв пройден!',
            'rated': 'Тест-драйв пройден!'
        };

        let content;

        switch (requestInfo.requestStep) {
            case 'initial': {
                content = 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!'
                break;
            }
            case 'invitation': {
               switch (requestInfo.requestStepStatus) {
                   case 'unresolved' : {
                       content = <Wrap {...requestData} cardTitles={cardTitles}>
                           <EventCardDecisionBlock
                               radios={{
                                   name: 'declineCause',
                                   btns: [
                                       {
                                           value: '1',
                                           title: 'Нужно перенести дату и время',
                                       },
                                       {
                                           value: '2',
                                           title: 'Неверные дата и время',
                                       },
                                       {
                                           value: '3',
                                           title: 'В данный момент меня это не интересует',
                                       }
                                   ]
                               }}
                               title="Пожалуйста, оставьте комментарий о причине отказа"
                               dealerId={this.props.requestData.requestOwnDealer}
                               requestId={this.props.requestData.requestId}
                               resolveStep={this.resolveStep}
                           />
                       </Wrap>;
                       break;
                   }
                   case 'success' : {
                       content =  <Wrap {...requestData} cardTitles={cardTitles}>
                           <EventCardForm>
                               <EventCardDecisionIndicator
                                   status={requestInfo.requestStepStatus}
                                   text='Мы будем ждать вас в назначенное время'
                                   onClick={this.handleResetDecision}/>
                           </EventCardForm>
                       </Wrap>;
                       break
                   }
                   case 'fail' : {
                       content =  <Wrap {...requestData} cardTitles={cardTitles}>
                           <EventCardForm>
                               <EventCardDecisionIndicator
                                   status={requestInfo.requestStepStatus}
                                   text='Вы отказались от тест-драйва'/>
                           </EventCardForm>
                       </Wrap>
                       break
                   }
               }
                break;
            }
            case 'passed': {
                switch (requestInfo.requestStepStatus) {
                    case 'unresolved' : {
                        content = <Wrap {...requestData} cardTitles={cardTitles}>
                            <EventCardRatingBlock
                                requestData={requestData}
                                currentRating={this.state.currentRating}
                                isActive={true}
                                onClick={this.handleRatingClick}
                                btnClickHandler={this.resolveStepRating}
                            />
                        </Wrap>;
                        break;
                    }
                    case 'success' : {
                        content = <Wrap {...requestData} cardTitles={cardTitles}>
                            <EventCardRatingBlock
                                requestData={requestData}
                                currentRating={this.state.currentRating}
                                isActive={false}
                            />
                        </Wrap>;
                        break;
                    }
                }
            }
        }


        return (
            <div>{content}</div>
        );
    }
}

const Wrap = (props) => {
    const cardTitles = props.cardTitles;
    const requestInfo = props.requestInfo;
    const basicInfo = requestInfo.requestBasicInfo;
    const requestHistory = props.requestHistory;

    return (
        <EventCard>
            <EventCardAboveTheFold cardTitle={cardTitles[requestInfo.requestStep]}>
                <EventCardBasicInfo basicInfo = {basicInfo}/>
                <EventCardAdditionalInfo>
                    {props.children}
                </EventCardAdditionalInfo>
            </EventCardAboveTheFold>
            <EventCardBelowTheFold>
                <EventCardHistory historyData = {requestHistory } />
            </EventCardBelowTheFold>
        </EventCard>
    );
};

const mapActionsToProps = (dispatch) => {
    return {
        resolveStepRating: (data) => {
            dispatch(actions.resolveStepRating(data));
        },
        resolveStep: (data) => {
            dispatch(actions.resolveStep(data));
        },
    };
};

RequestTestDrive = connect(null, mapActionsToProps)(RequestTestDrive);

export default RequestTestDrive;