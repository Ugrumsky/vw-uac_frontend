
export function initSlidingTabs(target){
  /* Tabs */
  let slidingTabsContainer = target.find('.js_running-underline__container-inner');
  let slidingTabsUnderline = slidingTabsContainer.find('.js_running-underline__runner');
  let slidingTabsItems = slidingTabsContainer.find('.running-underline__item');
  let slidingTabsItemsActive = slidingTabsItems.filter('.is_active');
  // let underlineMoveTimeout;

setTabsUnderline(slidingTabsItemsActive);
  slidingTabsItems.on('click', function(e) {
    setActiveTabItem(e.target);
    slidingTabsItemsActive = slidingTabsItems.filter('.is_active');
    setTabsUnderline(slidingTabsItemsActive);
  });

  function enableAnimationOnTabs() {
    setTimeout(function() {
      slidingTabsContainer.addClass('animation-is-enabled');
    }, 350);
  }

  function setTabsUnderline(target) {
    let left = Math.round($(target).position().left);
    let width = Math.round($(target).outerWidth());
    slidingTabsUnderline.css({
      'left': left + 'px',
      'width': width + 'px',
    });
  }

  function setActiveTabItem(target) {
    slidingTabsItems.removeClass('is_active');
    $(target).addClass('is_active');
  }
  enableAnimationOnTabs();

  var resizeTimer;
  $(window).on('resize', function(){
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function(){
      setTabsUnderline(slidingTabsItemsActive);
    }, 200)
  })

}

/*Tabs*/
$(function(){
    Tabs()
})
var getHeaderHeight = 70;
export let Tabs = (cont = $('body')) => {
  $(cont).find('.js_tabs').not('.inited').each( (i, cont) => {
    initSlidingTabs($(cont))
    let $tabs;
    let $btns;
    let $prevBtn;
    let $nextBtn;
    let index = 0;
    let changeTab = i => {
      let tabsTopOffset = $(cont).offset().top;
/*
      // скроллим к верхней границе табов при переключении
     if ($(window).scrollTop() > tabsTopOffset + 30) {
        $('body').animate({
          scrollTop: tabsTopOffset - getHeaderHeight() - 10
        });
      }
*/
      if (i >= $tabs.length || i < 0)
        return;

      let $tabContent = $tabs.eq(i);

      $tabContent
        .removeClass('hidden')
        .addClass('active')
        .siblings().removeClass('active').addClass('hidden');


      $(cont).trigger('tab:toggle', {
        number: i,
        $target: $tabContent
      });
      
      index = i;

      if (index == 0)
        $prevBtn.css('visibility', 'hidden');
      else
        $prevBtn.css('visibility', 'visible');

      if (index == $tabs.length - 1)
        $nextBtn.css('visibility', 'hidden');
      else
        $nextBtn.css('visibility', 'visible');
      
      if ($tabContent.find('.slick-initialized').length > 0){
        $tabContent.find('.slick-initialized')[0].slick.setOption('','',true)
      }

    };


    $tabs = $(cont).find('.js_tabs__content').find('.js_tabs__tab');
    $btns = $(cont).find('.js_tabs__head').find('.js_tabs__btn');
    $prevBtn = $(cont).find('.js_tabs__prev-btn');
    $nextBtn = $(cont).find('.js_tabs__next-btn');
    //$btns.first().addClass('active');
    $prevBtn.css('visibility', 'hidden');
    $tabs.not(':first-child').addClass('hidden');

    $btns.each( (i,btn) => {
      $(btn).on('click', () => changeTab(i) );
    });

    $nextBtn.on('click', (e) => {
      e.preventDefault();
      changeTab(index+1);
    });
    $prevBtn.on('click', (e) => {
      e.preventDefault();
      changeTab(index-1); 
    });

    $(cont).addClass('inited');
  })
}