import React from 'react';
const classNames = require('classnames');


export default (props, context) => {
    const CustomTag = props.tagName || 'div';
    let classes = classNames('btn', props.className);

    let onClick;

    if (props.onClickContextOverride) {
        onClick = context.onClick;
    }
    else if (props.onClickContextCombined) {
        onClick = () => {
            props.onClick();
            context.onClick();
        };
    }
    else {
        onClick = props.onClick;
    }
    return (
        <CustomTag className={classes} role="button" href={props.href} onClick={onClick}>
            <span className="btn__text">{props.text}</span>
        </CustomTag >
    )
}