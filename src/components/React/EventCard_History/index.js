import React from 'react';

const shortid = require('shortid');

const EventCardHistory = (props) => {

    const historyItems = props.historyData.map(function(historyItem, i) {
        return (
            <div key={shortid.generate()} className="event-card-history__item">
                <div className="event-card-history__left-part">
                    <div className="event-card-history__left-part-inner">
                        <div className="event-card-history__status-indicator">
                            <div className="event-card-history__status-indicator-circle"></div>
                            <div className="event-card-history__status-indicator-icon"></div>
                        </div>
                    </div>
                </div>
                <div className="event-card-history__right-part">
                    <div className="event-card-history__right-part-inner">
                        <div className="event-card-history__head">
                            <div className="event-card-history__time">{historyItem.requestHistoryItemDate}</div>
                        </div>
                        <div className="event-card-history__content">{historyItem.requestHistoryItemText}</div>
                    </div>
                </div>
            </div>
        );
    })

    return (
        <div className="event-card-history__container">
            <div className="event-card-history__item event-card-history__item--spacer">
                <div className="event-card-history__left-part">
                    <div className="event-card-history__left-part-inner">
                        <div className="event-card-history__status-indicator">
                            <div className="event-card-history__status-indicator-circle"></div>
                            <div className="event-card-history__status-indicator-icon"></div>
                        </div>
                    </div>
                </div>
                <div className="event-card-history__right-part">
                    <div className="event-card-history__right-part-inner">
                        <div className="event-card-history__head">
                            <div className="event-card-history__time"></div>
                        </div>
                        <div className="event-card-history__content"></div>
                    </div>
                </div>
            </div>
            {historyItems}
        </div>
    );

};

export default EventCardHistory;
