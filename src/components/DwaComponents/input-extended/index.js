import $ from 'jquery';
// import _throttle from 'lodash/throttle';
import {initPhoneField} from './init';

function inputHandling(wrapper, field) {
  if(!field.val() ) {
    wrapper.removeClass('is-filled');
  } else {
    if(field.is(':focus')) {
      wrapper.addClass('is-filled');
    }
  }
}

/* @this HTMLElement */
function changeEventHandle() {
  const field = $(this);
  const wrapper = field.parent().parent();
  inputHandling(wrapper, field);
}


$(document).on('focus keyup change', '.input__field', /* @this HTMLElement */ function() {
  changeEventHandle.call(this);
}).on('blur', '.input__field', /* @this HTMLElement */ function() {
  $(this).parent().parent().removeClass('is-filled');
});

$(document).on('mousedown', '.input__clear', /* @this HTMLElement */ function() {
  let input = $(this).prev();
  if(typeof input[0].inputmask !== 'undefined') {
    input.val(input[0].inputmask.getemptymask()).change();
  } else {
    input.val('').change();
  }
});


initPhoneField();
