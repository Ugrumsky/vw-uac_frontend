/**
 * Created by ufs on 3/17/2017.
 */
import _debounce from 'lodash/debounce';
import Slider from '../../../globals/js/Libraries/slider';
import Accordion from '../m108_Accordion/index';
import Component, {findComponent} from '../../../globals/js/Libraries/component';

// UI Elements
import {arrowTheme} from 'components/UI_Elements/pagination/arrows';

const arrow = arrowTheme('table');

// Import styles
import './index.scss';
let tIterator = 0;

function calcColNumbers(wrapper) {
  const wrapperWidth = wrapper.outerWidth();
  return Math.floor(wrapperWidth/135);
}

class Table extends Component {
  constructor(el) {
    super(el);
    this.element = $(el);
    this.tableColInner = this.element.find('.m104-thead__inner');
    this.tableColControls = this.element.find('.m104-thead__controls');
    this.tableRowsInner = this.element.find('.m104-tbody__inner');
    this.accordionWrapper = this.element.find('.m104__mtable');
    this.tableIndex = tIterator;
    tIterator++;
    const colNumber = calcColNumbers(this.tableColInner);
    this.element.attr('data-tindex', this.tableIndex);
    this.slider = new Slider(this.tableColInner, {
      appendArrows: this.tableColControls,
      dots: false,
      slidesToShow: colNumber,
      infinite: false,
      swipe: false,
      prevArrow: arrow.left,
      nextArrow: arrow.right,
    });
    this.rowsSliders = this.tableRowsInner.toArray().map((el) => new Slider($(el), {
      dots: false,
      arrows: false,
      slidesToShow: colNumber,
      infinite: false,
      swipe: false,
    }));
    this.accordion = null;
    this.accordionInited = false;
    this.sliderInited = false;
    this.initComponent();
    if(this.accordionWrapper) {
      this.initAccordion();
    }
    this.initSlider();
  }
  initSlider() {
    const self = this;
    if(!this.sliderInited) {
      this.slider.mount();
      for(let i = 0, length = this.rowsSliders.length; i < length; i++) {
        this.rowsSliders[i].mount();
        this.rowsSliders[i].syncTo(this.slider);
      }
      $(window).resize(_debounce(function() {
        const slidesToShow = calcColNumbers(self.tableColInner);
        self.slider.updateOption('slidesToShow', slidesToShow, true);
        for(let i = 0, length = self.rowsSliders.length; i < length; i++) {
          self.rowsSliders[i].updateOption('slidesToShow', slidesToShow, true);
        }
      }));
      this.sliderInited = true;
    }
  }
  initAccordion() {
    if(!this.accordionInited) {
      this.accordion = new Accordion(this.accordionWrapper[0]);
      this.accordionInited = true;
    }
  }
}

findComponent('.m104', Table);
