import React from 'react';
const shortid = require('shortid');

const RequestTags = (props) => {
    const tags = props.requestData.requestInfo.requestTagsAdded;

    const content = tags.map((tag)=>{
        return(
            <div
                key={shortid.generate()}
                className="tagFilter__tag-item-presentation tagFilter__tag-item-presentation_request">{tag}</div>
        )
    })

    let head;

    if (props.hasPreText) {
        head = <div className="timeline__block-no-card-content">
            <div className="timeline__block-content-simpleText">Вы добавили {tags.length} новых интереса
            </div>
        </div>;
    }

    return (
        <div>
            {head}
            {content}</div>

    );
}

export default RequestTags;


