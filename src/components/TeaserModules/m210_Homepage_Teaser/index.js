import Component, {findComponent} from '../../../globals/js/Libraries/component';
import Slider from '../../../globals/js/Libraries/slider';
import Media from '../../../globals/js/media';
// Import styles
import './index.scss';

class HomepageTeaser extends Component {
    constructor(el) {
        super(el);
        const self = this;
        this.color = false;

        this.inner = $(el).find('.m210__inner');
        this.dots = $(el).find('.m210__dots');
        this.controls = $(el).find('.m210__controls');
        this.slides = this.inner.children();

        this.slider = new Slider(this.inner, {
            slidesToShow: 1,
            arrows: false,
            dots: true,
            appendArrows: this.controls,
            appendDots: this.dots,
            responsive: [
            {
                breakpoint: Media.breakpoints.tablet - 1,
                settings: {
                    slidesToShow: 3,
                    arrows: true,
                    dots: false,
                },
            },
        ]});

        this.initComponent();

        $(() => {
            self.initSlider();
            // $(window).resize(_debounce(() => self.initSlider(), 200));
        });
    }

    initSlider() {
        this.slider.mount();
    }
}

findComponent('.m210', HomepageTeaser);
