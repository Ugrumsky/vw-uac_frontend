import 'globals/globals';
import './index.scss';


import * as authBlock from '../../../components/UacComponents/authBlock/index';

// HMR setup
if (module.hot) {
  module.hot.accept();
}

window.initAllTheThings = function(){
	authBlock.initAuthBlockSLider();
	selects.initSelects();
	datePickers.initDatepickers();
	inputs.initPhoneField();
	dnd.initDndUpload()
}