import React from 'react';

const EventCardAdditionalInfo = (props) => {
    return (
        <div className="eventCard__additional-info">
            {props.children}
        </div>
    );
};

export default EventCardAdditionalInfo;
