import $ from 'jquery';

//statusbar fixing
$(function() {
    var topStatusBar = $('.js_status-bar--details');
    if (topStatusBar.length != 0){
      var topStatusBarPos = topStatusBar.offset().top;
      $(window).on('scroll', function() {
        if ($(window).scrollTop() > topStatusBarPos || $(window).width() < 768 && $(window).scrollTop() > 50) {
          topStatusBar.addClass('is-fixed');
          $('.js_fixed-statusbar-spacer').show()
        } else {
          topStatusBar.removeClass('is-fixed');
          $('.js_fixed-statusbar-spacer').hide()
        }
      })
      var getTopStatusBarPosTimeout;
      $(window).on('resize', function() {
        clearTimeout(getTopStatusBarPosTimeout);
        getTopStatusBarPosTimeout = setTimeout(function() {
          topStatusBarPos = $('.js_status-bar--details').offset().top;
        }, 200)
      })
    }
  })