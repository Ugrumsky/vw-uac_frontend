
import select2 from './select2_custom.js'

import Component, {findComponent} from '../../../globals/js/Libraries/component';
import Page from '../../../globals/js/Libraries/page';

export default class TagFilter extends Component {
  constructor(el) {
    super(el);
    this.color = false;
    this.el = el;
    this.$el = $(el);
    this.$parent = this.$el.closest('.js_tagFilter__container')
    this.$valAggregator = this.$parent.find('.js_tagFilter__val-aggregator');
    this.enabledFilters =[];
    this.init();
    this.placeholder = 'Новый интерес';

  }

  init() {

      this.$el.select2(config);

      $(document).on('blur', '.select2-search__field',()=>{
        this.updatePlaceHolder();
      })

      this.$el.on('change', (evt) => {
        
        let data =this.getData();

        window.tagFilterData.forEach((i)=>{
          i.selected = false;
          data.forEach((j)=>{
            if(i.id == j.id){
              i.selected = true;
            }
          })
        })

        //this.updatePlaceHolder();
      });

      this.$parent.on('change', 'input[type="checkbox"]', (e)=>{

        window.tagFilterData.forEach(function(item,i){
          if (item.id == e.target.value){
            item.isChecked = e.target.checked
          }
        })

        this.destroySelect();
        this.reinitSelect();
        this.updateFilterVals();

        //this.updatePlaceHolder();

      })

    this.updatePlaceHolder();

  }

  destroySelect() {
    this.$el.select2('destroy').empty();
  }

  reinitSelect() {
    this.$el.select2(config);
    this.updatePlaceHolder();
  }

  getData() {
   return this.$el.select2('data')

  }

  updateFilterVals() {
    const checkedCheckboxes = this.$parent.find('input[type="checkbox"]:checked');

    this.enabledFilters =[]

     checkedCheckboxes.each((i,item) => {
      this.enabledFilters.push(item.value);
     })
     console.log(this.enabledFilters)
  }

  updatePlaceHolder() {
    setTimeout(() => {
      this.$parent.find('.select2-search__field').attr('placeholder', this.placeholder);
    },0)
  }

}

findComponent('.js_tagFilter__input', TagFilter);





window.tagFilterData = [
    {
      id: 'value1',
      text: 'value1',
      selected: true,
      isDisabled: false,
      isChecked: false,
    },
        {
      id: 'value2',
      text: 'value2 321 312 3213 124qwe wqe213123',
      selected: true,
      isDisabled: true,
      isChecked: false,
    },
        {
      id: 'value3',
      text: 'value3',
      selected: false,
      isDisabled: false,
      isChecked: false,
    },
        {
      id: 'value4',
      text: 'value4',
      selected: false,
      isDisabled: false,
      isChecked: false,
    },    {
      id: 'value5',
      text: 'value5',
      selected: false,
      isDisabled: false,
      isChecked: false,
    },
  ]

let config = {
  //tags: true,
  placeholder: {
    id: "-1",
    text: 'Новый интерес',
    selected:'selected'
  },
  templateSelection: template,
  data: tagFilterData

}

function template(data, container) {
  let checkbox = `<input type="checkbox" id=${data.text} value=${data.text} ${data.isDisabled ? 'disabled' :''} ${data.isChecked ? 'checked' :''}>`
  let tmp = `<div class= "tagFilter__tag-item" data-id=${data.text}>
    ${data.isDisabled ? '': '<label>'}
      ${checkbox}
      <div class="tagFilter__tag-item-presentation">
        ${data.text}
      </div>
    ${data.isDisabled ? '': '</label>'}
  </div>`

  return $(tmp)
}
