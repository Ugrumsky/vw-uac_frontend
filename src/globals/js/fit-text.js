import jQuery from 'jquery';
import media from 'globals/js/media.js';
import toRem from 'globals/js/utility/to-rem.js';
import _debounce from 'lodash/debounce';


(($, window, document) => {
    // undefined is used here as the undefined global variable in ECMAScript 3 is
    // mutable (ie. it can be changed by someone else). undefined isn't really being
    // passed in so we can ensure the value of it is truly undefined. In ES5, undefined
    // can no longer be modified.

    // window and document are passed through as local variables rather than global
    // as this (slightly) quickens the resolution process and can be more efficiently
    // minified (especially when both are regularly referenced in your plugin).

    // Create the defaults once
    const pluginName = 'kdxStageText';
    const defaults = {
        max_fz: false,
        breakpoints: {
            phone: 46,
            fablet: 74,
            tablet: 96,
            laptop: 140,
            desktop: 186,
            widescreen: 216,
        },
        steps: 4,
    };

    // The actual plugin constructor
    class Plugin {
        constructor(element, options) {
            this.element = $(element);
            this.parent = this.element.parent();

            this.settings = $.extend({}, defaults, options);
            this._defaults = defaults;
            this._name = pluginName;

            this.init();
        }

        init() {
            const self = this;
            this.parent.css('overflow', 'hidden');

            this.clone = this.element.clone();
            this.clone.css({
                'position': 'absolute',
                'width': 'auto',
                'display': 'block',
                'left': '0',
                'pointer-events': 'none',
                'white-space': 'nowrap',
                'visibility': 'hidden',
            });
            this.element.after(this.clone);

            $(window).on('resize', _debounce(function() {
                self.getMax(media.device);
            }, 200))
            ;
            document.addEventListener('DOMContentLoaded', () => {
                self.getMax(media.device);
            });
        }

        getMax(deviceName) {
            this.clone.css({
                'font-size': '',
            });

            const computedFontSize = parseFloat(this.clone.css('font-size'));
            let affordableFontSize = computedFontSize;
            const fontStep = (this.settings.breakpoints[deviceName] - computedFontSize) / this.settings.steps;

            if (this.clone.width() > this.parent.width()) {
                this.element.css({
                    'font-size': '',
                });
                return;
            }

            for (let i = 1; i <= this.settings.steps; i++) {
                this.clone.css('font-size', computedFontSize + i * fontStep);
                if (this.clone.width() > this.parent.width()) {
                    break;
                }
                affordableFontSize = computedFontSize + i * fontStep;
            }

            this.element.css({
                'font-size': `${toRem(affordableFontSize)}rem`,
            });
        }
    }

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function(options) {
        this.each(
            /* @this HTMLElement */
            function() {
                if (!$.data(this, `plugin_${pluginName}`)) {
                    $.data(this, `plugin_${pluginName}`, new Plugin(this, options));
                }
            });
    };
})(jQuery, window, document);
