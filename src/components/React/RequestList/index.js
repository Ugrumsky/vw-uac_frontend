import React from 'react';
import {connect} from 'react-redux'
import _filter from 'lodash/filter';
import _sortBy from 'lodash/sortBy';
import _reverse from 'lodash/reverse';

import TimelineBlock from '../TimelineBlock/index';
import RequestVisit from '../Request_visit/index';
import RequestCall from '../Request_call/index';
import RequestManager from '../Request_manager/index';
import RequestCredit from '../Request_credit/index';
import RequestTestDrive from '../Request_testDrive/index';
import RequestTradeIn from '../Request_tradeIn/index';
import RequestTags from '../Request_tags/index';
import OfferList from '../OfferList/index';

const mapStateToProps = (state) => {
    return {
        requestList: state.RequestList,
    };
}

@connect(mapStateToProps, null)
class RequestList extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }


    render() {
        let obj = this.props.requestList;
        let hasHead = false;

        if (this.props.dealerId) {
            obj = _filter(obj, (o) => {
                return o.requestOwnDealer == this.props.dealerId;
            });
        }
        else if (this.props.lastNum) {
            hasHead = true;
            obj = _sortBy(obj, [function (o) {
                return o.requestInfo.requestDate;
            }]);
            obj = _reverse(obj);
            obj = obj.slice(0, this.props.lastNum);
        }


        const requests = Object.values(obj).map(function (request, i) {
            let content;
            switch (request.requestInfo.requestType) {
                case 'visit':
                    content = <RequestVisit requestData={request}/>;
                    break;

                case 'call':
                    content = <RequestCall requestData={request}/>;
                    break;

                case 'manager':
                    content = <RequestManager requestData={request}/>;
                    break;

                case 'credit':
                    content = <RequestCredit requestData={request}/>;
                    break;

                case 'testdrive':
                    content = <RequestTestDrive requestData={request}/>;
                    break;

                case 'tradein':
                    content = <RequestTradeIn requestData={request}/>;
                    break;

                case 'offer':
                    content = <OfferList requestId={request.requestId} hasPreText/>
                    break;

                case 'tags':
                    content = <RequestTags requestData={request} hasPreText/>
                    break;

            }
            return (
                <TimelineBlock
                    key={request.requestId}
                    requestData={request}
                    hasHead={hasHead}>
                    {content}
                </TimelineBlock>
            );
        })

        return (
            <div>
                <div
                    className="timeline__block timeline__block_basic timeline__block_full-width timeline__block_faux-left timeline__block_no-inner-pd">
                    <div className="timeline__block-left-part">
                    </div>
                    <div className="timeline__block-right-part">
                        <div className="timeline__block-content">
                            <div className="headline-extended-flex">
                                <div className="h3 mb20">События</div>

                            </div>
                        </div>
                    </div>
                </div>

                {requests}


            </div>
        );
    }
}


export default RequestList;