/**
 * Created by ufs on 3/17/2017.
 */
import Component, {findComponent} from '../../../globals/js/Libraries/component';
// import Page from '../../../globals/js/Libraries/page';
// import _throttle from 'lodash/throttle';
import _isNull from 'lodash/isNull';
// Import styles
import './index.scss';

/*slideToggle results are overriden by styles on desktop*/

export default class ToggleStepList extends Component {
  constructor(el) {
    super(el);
    this.$el = $(el);
    this.$toggle_el = this.$el.find('.mob-list');
    this.$list_container = this.$el.find('.mk571__container');
    this.$tab_heads = this.$el.find('.mk571__unit');
    this.color = false;
    this.activeTab = null;
    this.maxActiveTab = null;
    this.initComponent();
    this.init();
    this.changeValue();
    this.setInitialTab();
  }

  init() {
    const $container = this.$list_container;
    this.$el.find('.mk571__link').click((e)=> {
      e.preventDefault();
    });
    $container.slideUp();
    this.$toggle_el.click(
      (e) => {
        if (this.$el.hasClass('active')) {
          $container.slideUp();
          this.$el.removeClass('active');
        } else {
          $container.slideDown();
          this.$el.addClass('active');
        }
      });
    this.$tab_heads.click(
      (e) => {
        e.stopPropagation();
        const index = $(e.target).closest('.mk571__unit').index();
        if(index <= this.maxActiveTab) {
          this.goToTab(index);
          $container.slideUp();
          this.$el.removeClass('active');
        } else {
          if (process.env.NODE_ENV === 'development') {
            console.log('you can`t go there');
          }
        }
      });
  }

  changeValue() {
    const $textBox = this.$toggle_el.find('.mob-list__text');
    let $valueBox = this.$list_container.find('.mk571__unit_active .mk571__content');
    let textValue = $valueBox.text();
    $textBox.text(textValue);
  }

  setInitialTab() {
    this.activeTab = this.$el.find('.mk571__unit_active').index() || 0;
    this.maxActiveTab = _isNull(this.maxActiveTab) ? this.activeTab : this.maxActiveTab;
    this.goToTab(this.activeTab);
    // this.$el.next().
  }

  goToTab(i) {
    // const index = this.activeTab;
    this.$el.next().find('.mk571_stepList_content-item').removeClass('is_visible');
    this.$el.next().children().eq(i).addClass('is_visible');
    this.activeTab = i;
    if(this.maxActiveTab < this.activeTab) {
      this.maxActiveTab = this.activeTab;
    }
    this.updateTabClasses();
    this.changeValue();
    //this.scrollToTarget(this.$el);
  }

  next() {
    this.activeTab++;
    this.goToTab(this.activeTab);
    return this.activeTab;
  }

  prev() {
    this.activeTab--;
    this.goToTab(this.activeTab);
    return this.activeTab;
  }

  updateTabClasses() {
    const tabHeads = this.$tab_heads;
    // console.info(tabHeads);
    for(let i = 0; this.$tab_heads.length > i; i++) {
      const el = tabHeads[i];
      const $el = $(el);
      const active = this.activeTab;
      const maxActive = this.maxActiveTab;
      $el.removeClass('mk571__unit_done');
      $el.removeClass('mk571__unit_active');
      if(i <= maxActive && i !== active) {
        $el.addClass('mk571__unit_done');
      } else if(i === active) {
        $el.addClass('mk571__unit_active');
      }
    }
  }

  isLastTab() {
    return this.$tab_heads.length - 1 <= this.activeTab;
  }

  scrollToTarget(target){
    $('html, body').animate({
        scrollTop: target.offset().top - 53
    }, 300);
  }
}

findComponent('.mk571', ToggleStepList);
