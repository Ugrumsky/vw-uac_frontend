export let vwCarModels = {
  "beetle": "Beetle",
  "golf": "Golf Plus",
  "jetta": "Jetta 2014",
  "passat_sedan": "Passat B7",
  "passat_alltrack": "Passat Alltrack",
  "passat_cc": "Passat CC",
  "passat_variant": "Passat Variant",
  "phaeton": "Phaeton",
  "polo_sedan": "Polo седан",
  "polo": "Polo хэтчбек",
  "tiguan": "Tiguan",
  "touareg": "Touareg Edition X",
  "touran": "Touran",
  "tiguan_nf": "Новый Tiguan"
}