/**
 * Created by ufs on 3/16/2017.
 */


import './index.scss';
import Component, {findComponent} from '../../../globals/js/Libraries/component';
import Slider from '../../../globals/js/Libraries/slider';
import {arrowTheme} from '../../UI_Elements/pagination/arrows/index';

const ARROW_THEME_NOBG = arrowTheme('nobg');

class ModelPreview extends Component {
  constructor(element) {
    super(element);
    this.element = $(element);
    this.innerImage = this.element.find('.mk613__image');
    this.innerColor = this.element.find('.mk613__colors__inner');
    this.innerColorControls = this.element.find('.mk613__colors__controls');
    this.innerColorLabel = this.element.find('.mk613__color-label');

    const additionalSlidersConf = {
      fade: true,
      infinite: false,
      dots: false,
      arrows: false,
      swipe: false,
    };
    this.imageSlider = new Slider(this.innerImage, additionalSlidersConf);
    this.colorSlider = new Slider(this.innerColor, {
      slidesToShow: parseInt(this.innerColor.width() / 70),
      appendArrows: this.innerColorControls,
      dots: false,
      prevArrow: ARROW_THEME_NOBG.left,
      nextArrow: ARROW_THEME_NOBG.right,
    });
    this.colorLabelSlider = new Slider(this.innerColorLabel, additionalSlidersConf);
    this.initComponent();
    this.init();
  }

  init() {
    const self = this;
    this.imageSlider.mount();
    this.colorSlider.mount();
    $(window).resize(() => {
      self.colorSlider.updateOption('slidesToShow', parseInt(self.innerColor.width() / 70), true);
    });
    this.colorLabelSlider.mount();
    this.innerColor.find('.mk613-color__check').change(
      /**
       * @this HTMLElement
       */
      function() {
        self.imageSlider.goTo(this.value);
        self.colorLabelSlider.goTo(this.value);
      });
  }
}

findComponent('.mk613', ModelPreview);
