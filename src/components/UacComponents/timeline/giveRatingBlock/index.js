import Component, {findComponent} from '../../../../globals/js/Libraries/component';
import Page from '../../../../globals/js/Libraries/page';

export default class GiveRating extends Component {
  constructor(el) {
    super(el);
    this.color = false;
    this.el = el;
    this.$el = $(el);
    this.items = this.$el.find('.giveRating__item');
    this.input = this.$el.find('input');
    this.hasJustRated = false;
    this.currentRating;
    this.vertical = false;
    this.init();
  }

  init() {
  this.$el.on('mouseover', '.giveRating__item', (e) => {
      if (this.hasJustRated) {
        return;
      } else {
        this.items.removeClass('marked');
        if (this.vertical) {
          $(e.target).add($(e.target).nextAll()).addClass('marked');
        } else {
          $(e.target).add($(e.target).prevAll()).addClass('marked');
        }
      }
    })

    this.$el.on('mouseleave', () => {
      if (!this.hasJustRated){
        this.items.removeClass('marked');
      }
      return
    })

    this.$el.on('mouseleave click', () => {

      this.currentRating = this.items.filter('.marked').length;

      if (this.vertical) {
        this.items.slice(this.currentRating - 1, this.items.length).addClass('marked');
      } else {
        this.items.slice(0, this.currentRating).addClass('marked');
      }
      
      if (this.currentRating == 0){
        this.currentRating = ''
      }
      this.hasJustRated = false;
      this.input.val(this.currentRating).trigger('change');

    })

    this.$el.on('click', () => {
      this.setRating()
    })
 }
   setRating() {
      this.hasJustRated = true;
      if (this.currentRating == 0){
        this.currentRating = ''
      }
      this.input.val(this.currentRating).trigger('change');
    }
}

findComponent('.js_giveRating', GiveRating);