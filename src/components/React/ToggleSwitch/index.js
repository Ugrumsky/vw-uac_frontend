import React from 'react';

let classNames = require('classnames');


class ToggleSwitch extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {

        return (
            <div className="uac_toggle_switch">
                <label className="uac_toggle_switch__label">
                    <input className="uac_toggle_switch__input" type="checkbox" checked = {this.props.checked} onChange={()=>{this.props.onChange()}}/>
                        <div className="uac_toggle_switch__presentation">
                            <div className="uac_toggle_switch__label-text">{this.props.label}</div>
                            <div className="uac_toggle_switch__states">
                                <span className="uac_toggle_switch__state"></span>
                            </div>
                            <div className="uac_toggle_switch__label-text"></div>
                        </div>
                </label>
            </div>
        );
    }
}


export default ToggleSwitch;
