import detectRTC from 'detectrtc';
import {addClass} from '../utility/classesMunipulation';

(() => {
    'use strict';
    detectRTC.load(() => {
        addClass(document.documentElement, `is-${detectRTC.browser.name.toLowerCase()}`);
    });
})();
