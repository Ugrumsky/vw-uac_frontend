/**
 * Created by UFS on 3/12/2017.
 */

import colors from '../../data/colors.json';
import _omit from 'lodash/omit';
import $ from 'jquery';

let instance = null;


class Page {
  constructor() {
    if (!instance) {
      instance = this;
    }
    this.modules = [];
    this.modulesRef = [];
    this.props = {};
    this.offset = 0;
    document.addEventListener('DOMContentLoaded', () => document.body.style.visibility = '');
    return instance;
  }

  getModule(id) {
    return this.modulesRef[id] !== undefined ? this.modulesRef[id] : false;
  }

  registerModule(module) {
    const id = this.modulesRef.length;
    this.modules.push(module);
    this.modulesRef.push(module);
    this.sortModules();
    return id;
  }

  setProp(name, value, handle, context) {
    if (typeof context === 'undefined') {
      context = handle;
    }
    this.props[name] = this.props[name] || {};
    this.props[name].listeners = this.props[name].listeners || [];
    this.props[name].value = value;
    if (handle && this.props[name].listeners.indexOf(handle.bind(context)) === -1) {
      this.props[name].listeners.push(handle.bind(context));
    }

    for (let i = 0; i < this.props[name].listeners.length; i++) {
      this.props[name].listeners[i](this.props[name].value);
    }
  }

  subscribeProp(name, handle, context) {
    this.props[name] = this.props[name] || {};
    this.props[name].listeners = this.props[name].listeners || [];
    if (handle && this.props[name].listeners.indexOf(handle.bind(context)) === -1) {
      this.props[name].listeners.push(handle.bind(context));
    }
    if (this.props[name].hasOwnProperty('value')) {
      for (let i = 0; i < this.props[name].listeners.length; i++) {
        this.props[name].listeners[i](this.props[name].value);
      }
    }
  }

  getProp(name) {
    return this.props[name] !== undefined ? this.props[name] : false;
  }

  /**
   *
   * @param {Element} el
   * @returns {Promise}
   */
  getModuleByEl(el) {
    return new Promise((resolve, reject) => {
      if(!el || !el.length) reject('undefined DOM element');
      if(typeof $.data(el[0], '_component') === 'undefined') {
        $(el).on('_component:init', function() {
          resolve($.data(el[0], '_component'));
        });
      } else {
        resolve($.data(el[0], '_component'));
      }
    });
  }

  removeProp(name) {
    _omit(this.props, name);
  }

  sortModules() {
    this.modules = this.modules.sort((a, b) => a.pageIndex - b.pageIndex);
    this.applyColors();
  }

  /**
   * swap dom elements of modules
   * @param module1 {Component}
   * @param module2 {Component}
   */
  swapModules(module1, module2) {
    const el1 = module1.componentElement;
    const el2 = module2.componentElement;
    // save the location of el2
    const parent2 = el2.parentNode;
    const next2 = el2.nextSibling;
    // special case for el1 is the next sibling of el2
    if (next2 === el1) {
      // just put el1 before el2
      parent2.insertBefore(el1, el2);
    } else {
      // insert el2 right before el1
      el1.parentNode.insertBefore(el2, el1);

      // now insert el1 where el2 was
      if (next2) {
        // if there was an element after el2, then insert el1 right before that
        parent2.insertBefore(el1, next2);
      } else {
        // otherwise, just append as last child
        parent2.appendChild(el1);
      }
    }

    module1.updatePageIndex();
    module2.updatePageIndex();
  }

  applyColors() {
    let color = colors.white;
    for (let i = 0; i < this.modules.length; i++) {
      const module = this.modules[i];
      const isHidden = module.componentElement.offsetWidth === 0 && module.componentElement.offsetHeight === 0;
      if (module.color && !isHidden) {
        module.componentElement.style.backgroundColor = color;
        color = color === colors.white ? colors.grey : colors.white;
      } else {
        color = colors.white;
      }
    }
  }
}

export default new Page();


