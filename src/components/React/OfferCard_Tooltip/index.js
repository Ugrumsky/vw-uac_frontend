import React from 'react';
import {connect} from 'react-redux';
import * as actions from '../../../actions/index';
import _find from 'lodash/find';
import _debounce from 'lodash/debounce';
const classNames = require('classnames');
const wNumb = require('wnumb');
const shortid = require('shortid');

const moneyFormat = wNumb({
    mark: '.',
    thousand: ' ',
    suffix: ' руб.',
});

const mapStateToProps = (state) => {
    return {
        offerList: state.OfferList,
        main: state.Main,
    };
}
const mapActionsToProps = (dispatch) => {
    return {
        toggleOfferCardTooltip: (data) => {
            dispatch(actions.toggleOfferCardtooltip(data));
        },
    };
}

@connect(mapStateToProps, mapActionsToProps)

class OfferCardTooltip extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        document.addEventListener('click', this.handleClickOutside);
        window.addEventListener('resize', this.handleResize);
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.handleClickOutside);
        window.removeEventListener('resize', this.handleResize);
    }

    handleResize = () => {
        this.props.toggleOfferCardTooltip({
            'showTooltip': false,
        });
    };

    handleClickOutside = (event) => {
        if (this.props.main.offerCardTooltipInfo.showTooltip &&
            this.wrapper &&
            !this.wrapper.contains(event.target) &&
            !$(event.target).hasClass('offer-card__price-tooltip-trigger')) {
            this.props.toggleOfferCardTooltip({
                'showTooltip': false,
            });
        }
    };


    render() {
        const props = this.props;

        let tooltipClassName = 'offer-card-tooltip';
        let tooltipTop = -9999;
        let tooltipLeft = -9999;
        let tooltipTailLeft = 140; // half of tooltip width (sorry)

        let offerPrice = null;
        let offerDiscountPrice = null;
        let priceDetails = null;
        const triggerSize = 18;
        const tailHeight = 16;


        if (props.main.offerCardTooltipInfo.showTooltip &&
            /* check if click.target parent == tooltip.parent */
            props.main.offerCardTooltipInfo.parentOfferList == this.props.parentOfferListId) {
            const targetCoords = props.main.offerCardTooltipInfo.coords;
            const offsetTop = props.main.offerCardTooltipInfo.offsetTop;
            const offerId = props.main.offerCardTooltipInfo.offerId;
            const offerData = _find(props.offerList, (o) => {
                return o.offerId == offerId;
            });

            offerPrice = moneyFormat.to(offerData.offerPrice);
            offerDiscountPrice = moneyFormat.to(offerData.offerDiscountPrice);
            priceDetails = offerData.offerPriceTooltipDetails.map((detail, i) => {
                const detailTitle = detail.href ?
                    <a href={detail.href}>{detail.title}</a> : <span>{detail.title}</span>;
                const detailText = moneyFormat.to(detail.text);
                return (
                    <div className="offer-card__info-table-row" key={shortid.generate()}>
                        <div className="offer-card__info-table-row_left">{detailTitle}</div>
                        <div className="offer-card__info-table-row_right">- {detailText}</div>
                    </div>
                );
            });
            tooltipClassName += ' is_visible';
            tooltipTop = targetCoords.top + offsetTop + triggerSize + tailHeight;
            tooltipLeft = targetCoords.left - this.wrapper.offsetWidth / 2 + triggerSize / 2 + 5; // 5 - magicNum(unknown offset)
            if (tooltipLeft + this.wrapper.offsetWidth > $(window).width()) {
                tooltipLeft = tooltipLeft - tooltipLeft / 2 - 20;
            }
            tooltipTailLeft = targetCoords.left - tooltipLeft + triggerSize / 2 - 7;// 7 - magicNum(unknown offset)
        }
        return (

            <div
                key={shortid.generate()}
                ref={(wrapper) => {
                    this.wrapper = wrapper;
                }}
                className={tooltipClassName}
                style={{
                    top: tooltipTop,
                    left: tooltipLeft
                }}>
                <div className="offer-card-tooltip-tail"
                     style={{
                         left: tooltipTailLeft
                     }}></div>
                <div className="offer-card-tooltip_inner">
                    <div className="offer-card__info-item">
                        <div className="offer-card__info-item-title">Первоначальная цена</div>
                        <div
                            className="offer-card__info-item-text offer-card__info-item-text_crossed"> {offerPrice}</div>
                    </div>
                </div>
                <div className="offer-card-tooltip_separator"></div>
                <div className="offer-card-tooltip_inner">
                    <div className="offer-card__info-item">
                        <div className="offer-card__info-item-title">Предложение дилера</div>
                        <div className="offer-card__info-item-text">{offerDiscountPrice}</div>
                    </div>
                </div>
                <div className="offer-card-tooltip_inner">
                    <div className="offer-card__info-table">
                        {priceDetails}
                    </div>
                </div>
            </div>

        );
    }
}

export default OfferCardTooltip;

