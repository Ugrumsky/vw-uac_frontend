import React from 'react';

const EventCardDecisionIndicator = (props) => {

    if (props.status == 'success') {
        return (
            <div className="eventCard__flex-container eventCard__flex-container--nowrap">
                <div className="eventCard__icon-decision eventCard__icon-decision--positive is_active hasicon icon-base-checkmark"></div>
                <span className="eventCard__simple-inline-text">{props.text}
                    <span className="eventCard__functional-linklike-btn" onClick={props.onClick}>Отмена</span>
                </span>
            </div>
        );
    }

    else {
        return (
            <div className="eventCard__flex-container eventCard__flex-container--nowrap">
                <div className="eventCard__icon-decision eventCard__icon-decision--negative is_active hasicon icon-base-checkmark"></div>
                <span className="eventCard__simple-inline-text">{props.text}</span>
            </div>
        );
    }

};


export default EventCardDecisionIndicator;