import React from 'react';

const EventCardBasicInfo = (props) => {
    const content = props.basicInfo.map(function(infoItem, i){
        return (
            <div key = {i} className="eventCard__basic-info-item">
                <div className="eventCard__basic-info-item-title">{infoItem.title}</div>
                <div className="eventCard__basic-info-item-content">{infoItem.text}</div>
            </div>
        );
    });
    return (
        <div className="eventCard__basic-info">
            {content}
        </div>
    );
};

export default EventCardBasicInfo;
