/*
 * JS/JSX loader config
 */

import path from 'path';

function escape(text) {
  return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
}

module.exports = function (CONFIG) {
  return {
    test: /\.js[x]?$/,
    exclude: /node_modules/,
    use: [
      'imports-loader?$=jquery,jQuery=jquery',
      {
        loader: 'babel-loader',
        options: {
          presets: ['es2015',  'react',
              'stage-0'],
          plugins: ['transform-runtime',
              'add-module-exports',
              'transform-decorators-legacy'],
          cacheDirectory: true,
        }
      }
    ]
  };
};
