import React from 'react';

const Radiobtn = (props) => {

    return (
        <label className="radiobtn">
            <input className="radiobtn__control required"
                   type="radio"
                   name={props.name}
                   value={props.value}
                   checked={props.checked}
                   onClick={props.onChange}/>
            <div className="radiobtn__mask"></div>
            <span className="radiobtn__title">{props.text}</span>
        </label>
    );

}


export default Radiobtn;
