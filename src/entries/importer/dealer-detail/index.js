import 'globals/globals';
import './index.scss';
import _ from 'lodash';

// Mandatory components


// HMR setup
if (module.hot) {
  module.hot.accept();
}

$(function(){
  initMap()
})




function initMap() {
  var dealer = {lat: 55.714916, lng: 37.718094};
  var map = new google.maps.Map(document.getElementById('dealer-detail__map-container'), {
    zoom: 16,
    center: dealer
  });
  var marker = new google.maps.Marker({
    position: dealer,
    map: map
  });
  google.maps.event.addDomListener(window, 'resize', function() {
    map.setCenter(dealer);
});
}
