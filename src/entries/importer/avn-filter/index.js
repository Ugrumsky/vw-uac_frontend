import 'globals/globals';
import './index.scss';

require('../../../globals/js/avn.js');
/*avn*/

var filterBlock = $('.js_entire-filter')
var filterStages = $('.js_filter-stage')
var filterToggler = $('#js_filter-toggler')

$(function(){
	var isMobile = window.matchMedia("(max-width: 1023px)").matches;
	if(isMobile){
		filterBlock.addClass('is_hidden');
		filterToggler.prev().addClass('is_mobile');
	}
	else{
		filterStages.last().addClass('is_hidden');
		filterStages.first().find('.page-section').last()
	}

filterToggler.on('click', function(){
	$(this).parent().toggleClass('is_open')
	if(isMobile){
		filterBlock.slideToggle();
	}
	else{
		filterStages.last().slideToggle('fast');
		filterStages.prev().find('.page-section').last()
	}
})
})


/*car config selection hacks*/
/* not needed 20.03.2017 Razvodov
var hideCardsTimeout;

var lastCardsAmount = getCardsHidden();
var currentCardsAmount = getCardsHidden();

$(window).on('resize', function() {

  var wWidth = $(window).width();
  
  clearTimeout(hideCardsTimeout);

  hideCardsTimeout = setTimeout(function() {
    if ($('.select-configuration').length > 0) {
      currentCardsAmount = getCardsHidden()
      if (currentCardsAmount != lastCardsAmount) {
        $('.page-section__content--car-list .js_show-more-card-btn span').each(function() {
          $(this).text(+$(this).attr('data-originalAmount') + currentCardsAmount);
        })
        lastCardsAmount = currentCardsAmount;
      } else {
        return
      }
    }

  }, 500)
})

$('.page-section__content--car-list .js_show-more-card-btn span').each(function() {
  $(this).attr('data-originalAmount', $(this).text())
  $(this).text(+$(this).attr('data-originalAmount') + currentCardsAmount);
})

function getCardsHidden() {
  var cardsHidden;
if (window.matchMedia("(min-width: 1024px) and (max-width: 1168px)").matches) {
    cardsHidden = 1;
  } else if (window.matchMedia("(min-width: 512px) and (max-width: 1023px)").matches)  {
    cardsHidden = 2;
  } 
    else if(window.matchMedia("(max-width: 511px)").matches) {
    cardsHidden = 3;
    }
  else {
    cardsHidden = 0;
  }
  return cardsHidden;
}
*/