import React from 'react';
let classNames = require('classnames');

const EventCardAboveTheFold = (props) => {

    let innerClass= 'eventCard__above-the-fold-inner';

    if (!props.notPadded) innerClass += ' padded-container';

    let cardTitle = '';

    if(props.cardTitle != null) {
        cardTitle = <div className="eventCard__title">{props.cardTitle}</div>;
    }


    return (
        <div className="eventCard__above-the-fold">
            <div className={innerClass}>
                {cardTitle}
                {props.children}
            </div>
        </div>
    );
};

export default EventCardAboveTheFold;
