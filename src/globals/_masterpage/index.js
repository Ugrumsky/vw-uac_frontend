import $ from 'jquery';

require('../globals');
require('./index.scss');

import 'components/NavigationModules/m502_Mainnavigation/index.js';
import 'components/NavigationModules/m520_Global_Footer/index.js';


if (module.hot) {
  module.hot.accept();
}

