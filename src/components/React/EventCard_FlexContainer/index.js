import React from 'react';

const EventCardFlexContainer = (props) => {
    return (
        <div className="eventCard__flex-container">
            {props.children}
        </div>
    );
};


export default EventCardFlexContainer;
