/**
 * Created by ufs on 3/17/2017.
 */
import {findComponent} from '../../../../globals/js/Libraries/component';
import TwoThreeNav from '../simpleNav23';
import titleItem from './titleItem.pug';
import linkItem from './linkItem.pug';
import gotoByScroll from '../../../../globals/js/utility/gotoByScroll';
import _debounce from 'lodash/debounce';
// import sticky from '../../../../globals/js/Libraries/vwsticky';
// Import styles
import './index.scss';

const uglyOffset = 40;

class FourNavItem {
  constructor(el, controller) {
    this.controller = controller;
    this.$block = $(el);
    this.title = $(this.$block).data('nav-four-block');
    this.$link = $(linkItem({
      text: this.title,
    }));
    this.isActive = false;
    this.init();
  }

  init() {
    const self = this;
    this.$link.on('click', function(e) {
      e.preventDefault();
      self.onClick.bind(self)();
    });
  }

  onClick() {
    gotoByScroll(this.$block, uglyOffset-1); // TODO: delete magic number. It's offset
    this.closeController.bind(this)();
    this.controller.deactivateAll();
    this.activate();
  }

  closeController() {
    this.controller.opened = false;
    this.controller.update();
  }
  activate() {
    this.$link.addClass('is-active');
    this.isActive = true;
  }
  deactivate() {
    this.$link.removeClass('is-active');
    this.isActive = false;
  }
}

class FourNav extends TwoThreeNav {
  constructor(el) {
    super(el);
    this.el = el;
    this.container = $(el).find('.navlvl4__container');
    this.list = $(el).find('.navlvl4__list');
    this.stickyInner = this.container[0];
    this.$inner = $(el).find('.navlvl4__inner');
    this.$anchorBlocks = $('[data-nav-four-block]');
    this.$titleItem = $(titleItem());
    this.$titleItemText = this.$titleItem.find('.navlvl4__title-text');
    this.links = this.$anchorBlocks.map((i, block) => new FourNavItem(block, this));
    this.offsetSide = 'bottom';
    this.activeTitle = '';
    this.opened = false;
    this.initComponent();
    this.init();
  }

  init() {
    if(this.$anchorBlocks.length == 0) {
      this.destroyNav.bind(this)();
      return false;
    }
    this.$titleItem.on('click', (e) => {
      e.preventDefault();
      this.toggle.bind(this)();
    });
    this.$inner.prepend(this.$titleItem);
    this.addLinks();
    this.update();
    this.updateCurrentBlock.bind(this)();
    $(window).on('scroll', _debounce(this.updateCurrentBlock.bind(this), 100));
  }

  update() {
    this.$titleItemText.text(this.activeTitle);
    if(this.opened) {
      this.container.addClass('is-opened');
    } else {
      this.container.removeClass('is-opened');
    }
  }

  toggle() {
    this.opened = !this.opened;
    this.update();
  }
  addLinks() {
    this.links.each((i, FourNavItem) => {
      this.list.append(FourNavItem.$link);
    });
  }

  updateCurrentBlock() {
    this.links.each((i, block) => {
      const $block = block.$block;
      const elTop = $block.offset().top;
      const windowTop = $(window).scrollTop();
      if(elTop <= windowTop + uglyOffset) {
        this.activeTitle = block.title;
        this.deactivateAll();
        block.activate();
        this.update();
      }
    });
  }

  deactivateAll() {
    this.links.each((i, block) => {
      block.deactivate();
    });
  }

  destroyNav() {
    this.el.remove();
  }
}

findComponent('.navlvl4', FourNav);
