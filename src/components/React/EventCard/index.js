import React from 'react';

class EventCard extends React.Component {
    /* Below the fold is toggled in two separate ways -
    through actions for OfferCards and through context
    for simple event-card (they don't need a way to be toggled
    from outside of themselves as of now, so I left it this way).*/
    //TODO: think about refactoring

    constructor(props) {
        super(props);
        this.state = {
            belowTheFoldShown: false,
        };
    }

    getChildContext = () => {
        return {onClick: this.toggleBelowTheFoldVisibility};
    };

    toggleBelowTheFoldVisibility = (param) => {
        let newState;
        /*Check that param is passed and it's not event object.*/
        if (typeof param !== "undefined" && typeof param.target == "undefined" ) {
            newState = param;
        } else {
            newState = !this.state.belowTheFoldShown;

        }
        this.setState({
            belowTheFoldShown: newState,
        });
    };

    render() {
        let cardClass = 'eventCard js_eventCard';

        cardClass += this.state.belowTheFoldShown ? ' below-the-fold-shown' : '';



        const belowToggleArrow = this.props.noBelow ? '' :
            <div className="eventCard__fold-toggler hasicon icon-base-arrow-down"
                 onClick={this.toggleBelowTheFoldVisibility}></div>;

        return (
            <div className={cardClass}>
                {belowToggleArrow}
                {this.props.children}
            </div>
        );
    }
}


EventCard.childContextTypes = {
    onClick: React.PropTypes.func
}

export default EventCard;


