const dotProp = require('dot-prop-immutable');

import {offerList} from '../globals/mocks/reactMocks.js';

const initialState = offerList;

export default (state = initialState, action) => {
    switch (action.type) {
        case 'RESOLVE_OFFER':
            return {
                ...dotProp.set(state, `${action.payload.offerId}.offerUserDecisionStatus`, action.payload.userDecision)
            }
        case 'TOGGLE_BELOW_THE_FOLD_VISIBILITY':
            return {
                ...dotProp.set(state, `${action.payload.offerId}.belowTheFoldShown`, !state[action.payload.offerId].belowTheFoldShown)
            }
        default:
            return state;
    }
}

