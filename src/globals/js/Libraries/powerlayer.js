/**
 * Created by ufs on 3/22/2017.
 */
import url from 'url';
import $ from 'jquery';
// import _filter from 'lodash/filter';
require('imports-loader!history.js/history.js');
require('imports-loader!history.js/history.adapter.ender.js');
// import
// import modernizr from 'modernizr';
import freezer from '../utility/freezeBody';
import '../../style/vwd5/_powerlayer.scss';


class PowerLayer {
    constructor(href, controller) {
        this.url = href;
        this.urlParsed = url.parse(this.url);
        console.log(this.urlParsed);
        this.controller = controller;
        this.wrapper = null;
        this.dom = null;
        this.scripts = null;
        this.opened = false;
    }

    open(actionSrc) {
        console.log('opened');

        const self = this;
        actionSrc.setAttribute('disabled', true);
        // if (this.dom === null) {
        this.wrapper = $('<iframe/>', {
            src: self.url,
            class: 'm400',
        }).appendTo($('body')).append(this.dom);


        freezer.lock();
        this.wrapper.on('load', function() {
            console.log(self.wrapper.contents().find('.m400-close'));
            self.wrapper.contents().find('.m400-close')
                .on('click', /* @this HTMLElement */ function(e) {
                    e.preventDefault();
                    window.history.back();
                });
            window.history.pushState(null, null, url.parse(actionSrc.href).path);
            self.opened = true;
            self.controller.opened = self.urlParsed.pathname;
            actionSrc.removeAttribute('disabled');
        });


        //     .appendTo($('body')).load(this.url, function() {
        //     self.wrapper.find('.m400-close')
        //         .on('click', /* @this HTMLElement */ function(e) {
        //             e.preventDefault();
        //             window.history.back();
        //         });

        //     require.ensure([], function() {
        //        require('../../../entries/dealer/powerlayer/index');
        //        console.log('ajax init');
        //     });
        //     actionSrc.removeAttribute('disabled');
        // });
        // $.ajax({
        //     url: self.url,
        //     dataType: 'text',
        //     success: function(data) {
        //         self.dom = $($.parseHTML(data, document, true));
        //         actionSrc.removeAttribute('disabled');
        //         self.generateDOM(actionSrc);
        //     },
        //     error() {
        //         actionSrc.removeAttribute('disabled');
        //     },
        // });
        // } else {
        //   actionSrc.removeAttribute('disabled');
        //   this.generateDOM(actionSrc);
        // }
    }

    close() {
        if (this.opened === true) {
            this.wrapper.remove();
            freezer.unlock();
            this.wrapper = null;
            this.opened = false;
        }
    }

    generateDOM(actionSrc) {
        // const self = this;
        freezer.lock();
        this.wrapper = $('<div/>', {
            class: 'm400',
        }).appendTo($('body'))
            .append(this.dom);
        this.wrapper.find('.m400-close')
            .on('click', /* @this HTMLElement */ function(e) {
                e.preventDefault();
                window.history.back();
            });
        window.history.pushState(null, null, url.parse(actionSrc.href).path);
        this.opened = true;
        this.controller.opened = this.urlParsed.pathname;
    }
}

class PowerLayerController {
    constructor() {
        this.powerLayers = {};
        this.opened = null;
        const self = this;
        const powerLayerLinks = document.getElementsByClassName('js_powerlayer_link');
        for (let i = 0; i < powerLayerLinks.length; i++) {
            const powerLayerLink = url.parse(powerLayerLinks[i].href || '');
            if (!this.powerLayers.hasOwnProperty(powerLayerLink.pathname)) {
                this.powerLayers[powerLayerLink.pathname] = new PowerLayer(powerLayerLink.href, this);
            }
            powerLayerLinks[i].addEventListener('click', /* @this HTMLElement */function(e) {
                e.preventDefault();
                self.powerLayers[url.parse(this.href).pathname].open(this);
            });
        }
/* commented by iv 19/04/2017
        window.addEventListener('popstate', function(event) {
            const state = event.state;
            console.log(event);
            if (state === null) {
                self.powerLayers[self.opened].close();
                self.opened = null;
            }
        });*/
    }
}

export default new PowerLayerController();
