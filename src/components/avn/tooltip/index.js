import $ from 'jquery';
$(document).on('mouseenter', '.has_tooltip__icon', e => {
  let target = e.currentTarget;

  let tooltipElem = $(target).siblings('.has_tooltip__popup')[0];
  let tooltipArrow = $(tooltipElem).find('.has_tooltip__arrow')[0];
  let coords = target.getBoundingClientRect();
  let left = coords.left + (target.offsetWidth - tooltipElem.offsetWidth) / 2;
  let right = null;
  let bottomFlag = false;
  let arrowLeft = null;

  // не вылезать за левую границу окна
  if (left < 0) {
    left = 0;
    arrowLeft = `${coords.left + target.offsetWidth / 2}px`;
  } else {
    arrowLeft = '';

    right = $(window).width() - (coords.right - (target.offsetWidth - tooltipElem.offsetWidth) / 2);

    // не вылезать за правую границу окна
    if (right < 0) {
      right = 0;
      arrowLeft = `${ tooltipElem.offsetWidth - ($(window).width() - coords.right + target.offsetWidth / 2)}px`;
    } else {
      arrowLeft = '';
    }
  }

  tooltipArrow.style.left = arrowLeft;

  // не вылезать за верхнюю границу окна
  let top = coords.top - tooltipElem.offsetHeight;
  if (top < 0) {
    bottomFlag = true;
    top = coords.top + target.offsetHeight;
  }

  if (right == 0)
    tooltipElem.style.right = 0;
  else
    tooltipElem.style.left = `${left}px`;

  tooltipElem.style.top = `${top}px`;

  if (bottomFlag)
    $(tooltipElem).addClass('has_tooltip__popup--bottom');
  else
    $(tooltipElem).removeClass('has_tooltip__popup--bottom');
});