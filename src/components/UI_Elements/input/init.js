import inputMask from 'inputmask';

export function initPhoneField(container) {
  container = container || document;
  let elements = $(container).find('.input__field[type="tel"]');
  elements.each(function(){
    if(!$(this).hasClass('inited')){
      let im = inputMask({
        mask: '+7(999)999-99-99',
        showMaskOnHover: false,
      });
      im.mask($(this)[0]);
      $(this).addClass('inited')
    }
  })
}
