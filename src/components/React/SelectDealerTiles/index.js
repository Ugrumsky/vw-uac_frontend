import React from 'react';
import {connect} from 'react-redux';
import * as actions from '../../../actions/index';
import _filter from 'lodash/filter';

const shortid = require('shortid');


const mapStateToProps = (state) => {
    return {
        dealerList: state.DealerList,
        requestList: state.RequestList,
        lastVisit: state.Main.lastVisit
    };
}

const mapActionsToProps = (dispatch) => {
    return {
        changeDealer: (id) => {
            dispatch(actions.changeDealer(id))
        },
    };
}
@connect(mapStateToProps, mapActionsToProps)
class SelectDealerTiles extends React.Component {


    constructor(props) {
        super(props);
        this.state = {};
    }

    changeDealer = (id) => {
        this.props.changeDealer(id);
    }

    getUnreadAmount = (dealerId) => {
        let tempObj = _filter(this.props.requestList, (o) => {
            return o.requestOwnDealer == dealerId;
        });

        tempObj = _filter(tempObj, (o) => {
            return o.requestInfo.requestDate > this.props.lastVisit;
        });

        return tempObj.length;
    }


    render() {
        const dealerList = this.props.dealerList;
        let dealerIds = [];

        for (let key in dealerList) {
            dealerIds.push(key);
        }

        const content = Object.values(dealerList).map((dealer, i) => {
            return (
                <SelectDealerTile
                    key={shortid.generate()}
                    dealerName={dealer.dealerName}
                    notificationCount={this.getUnreadAmount(dealerIds[i])}
                    onClick={() => {
                        this.changeDealer(dealerIds[i])
                    }}/>
            )
        })

        return (
            <ul className="main-page-dealerPicker__list">
                {content}
            </ul>
        );
    }
}

export default SelectDealerTiles;


const SelectDealerTile = (props) => {
    const notificationCounter = props.notificationCount > 0 ?
        <div className="notification-counter main-page-dealerPicker__item-counter">{props.notificationCount}</div> : null;

    return (
        <li className="main-page-dealerPicker__item" onClick={props.onClick}>
            <div className="main-page-dealerPicker__item-title">{props.dealerName}</div>
            {notificationCounter}
        </li>
    );
};
