import Component, {findComponent} from '../../../globals/js/Libraries/component';
import $ from 'jquery';
import map from '../../../globals/js/Libraries/ya_map';
import viewport from '../../../globals/js/common/viewport';
import {hasClass} from '../../../globals/js/utility/classesMunipulation';
import _debounce from 'lodash/debounce';
// Import styles
import './index.scss';


class SimpleMap extends Component {
  constructor(el) {
    super(el);
    this.offset = hasClass(this.componentElement, 'mk153_left-sided') ? 'left' : 'right';
    this.mapContainer = this.componentElement.querySelectorAll('.mk153__map')[0];
    this.mapOffset = null;
    // this.mapMarkers
    this.initComponent();
    map.loadApi(this.init.bind(this));
  }

  init() {
    const self = this;

    function applyMargin() {
      if (viewport().width >= 768) {
        let area = {
          top: 0,
          width: '33%',
          height: '100%', // The percentages are calculated relative to the size of the map container.
        };
        if (self.offset === 'left') {
          area.left = 0;
        }
        if (self.offset === 'right') {
          area.right = 0;
        }


        self.mapOffset.setArea(area);
      } else {
        self.mapOffset.setArea({
          top: 0,
          left: 0,
          width: '100%',
          height: '10%', // The percentages are calculated relative to the size of the map container.
        });
      }
      self.map.center();
    }

    if (!this.map) {
      this.map = map.newMap(this.mapContainer, {});
      this.map.init();
      this.map.addMarker([55.74954, 37.621587]); // todo: уюрать костыль
      this.mapOffset = this.map.map.margin.addArea({
        top: 0,
        left: 0,
        width: '0',
        height: '0',
      });

      $(document).ready(_debounce(applyMargin.bind(this), 200));
      $(window).resize(_debounce(applyMargin.bind(this), 200));
    }
  }
}

findComponent('.mk153', SimpleMap);
