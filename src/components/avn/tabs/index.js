import $ from 'jquery';
var getHeaderHeight = 70;
let Tabs = (cont = $('body')) => {
  $(cont).find('.tabs').not('.inited').each( (i,cont) => {
    let $tabs;
    let $btns;
    let $prevBtn;
    let $nextBtn;
    let index = 0;

    let changeTab = i => {
      let tabsTopOffset = $(cont).offset().top;

      // скроллим к верхней границе табов при переключении
      if ($(window).scrollTop() > tabsTopOffset + 30) {
        $('body').animate({
          scrollTop: tabsTopOffset - getHeaderHeight() - 10
        });
      }

      if (i >= $tabs.length || i < 0)
        return;

      let $tabContent = $tabs.eq(i);

      $tabContent
        .removeClass('hidden')
        .addClass('active')
        .siblings().addClass('hidden');

      $btns.eq(i)
        .addClass('active')
        .siblings().removeClass('active');

      $(cont).trigger('tab:toggle', {
        number: i,
        $target: $tabContent
      });
      
      index = i;

      if (index == 0)
        $prevBtn.css('visibility', 'hidden');
      else
        $prevBtn.css('visibility', 'visible');

      if (index == $tabs.length - 1)
        $nextBtn.css('visibility', 'hidden');
      else
        $nextBtn.css('visibility', 'visible');
    };


    $tabs = $(cont).find('.tabs__content').eq(0).children('.tabs__tab');
    $btns = $(cont).find('.tabs__header').eq(0).children('.tabs__btn');
    $prevBtn = $(cont).find('.tabs__prev-btn');
    $nextBtn = $(cont).find('.tabs__next-btn');

    $btns.first().addClass('active');
    $prevBtn.css('visibility', 'hidden');
    $tabs.not(':first-child').addClass('hidden');

    $btns.each( (i,btn) => {
      $(btn).on('click', () => changeTab(i) );
    });

    $nextBtn.on('click', (e) => {
      e.preventDefault();
      changeTab(index+1);
    });
    $prevBtn.on('click', (e) => {
      e.preventDefault();
      changeTab(index-1); 
    });

    $(cont).addClass('inited');
  })
}

export default Tabs;
