import React from 'react';
import {connect} from 'react-redux';
import {parsePhone}from '../../../globals/js/utility/miscUtils.js';
import * as actions from '../../../actions/index';
import Button from '../Button/index';
import DealerInfoCardTooltip from '../DealerInfoCard_tooltip/index';
import IconLink from '../IconLink/index';

const classNames = require('classnames');
const shortid = require('shortid');

const mapStateToProps = (state) => {
    return {
        dealerInfo: state.DealerList[state.Main.currentSelectedDealerId],
    }
}

const mapActionsToProps = (dispatch) => {
    return {
        changeDealer: (id) => {
            dispatch(actions.changeDealer(id))
        }
    }
}
@connect(mapStateToProps, mapActionsToProps)
class DealerInfoCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            additionalDealersShown: false,
        };
    }

    componentDidMount() {
        this.initMap();
    }

    componentDidUpdate() {
        this.marker.setMap(null)
        this.marker = '';
        this.marker = new google.maps.Marker(this.mapMarkerConstructor());
        this.map.panTo(this.props.dealerInfo.dealerCoords);
    }

    setSticky() { //TODO:rework this method!!
        let target = $('.js_dealer-info-card');
        let initialCardTop;
        let posCorrection = 20;

        if ($('html').hasClass('is-ie')) {
            initialCardTop = target.offset().top;
            target.addClass('sticky-ie');
        }

        $(window).on('scroll', function () {
            if ($('html').hasClass('is-ie')) {
                let scrollLength = $(document).scrollTop();
                if (scrollLength > initialCardTop) {
                    target.css({'top': scrollLength - initialCardTop + posCorrection});
                }
            }
        })
    };

    marker = '';

    mapMarkerConstructor() {
        const getMarkerIcon = (iconUrl) => {
            return {
                url: iconUrl,
                size: new google.maps.Size(33, 44),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(22, 44),
                scaledSize: new google.maps.Size(33, 44),
            }
        };
        const markerOptions = {
            position: this.props.dealerInfo.dealerCoords,
            map: this.map,
            icon: getMarkerIcon('data:image/svg+xml;base64,PHN2ZyBpZD0iTGF5ZXJfMSIgZGF0YS1uYW1lPSJMYXllciAxIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAzMyA0NCI+PGRlZnM+PHN0eWxlPi5jbHMtMXtmaWxsOiMwMDk5ZGE7c3Ryb2tlOiNmZmY7c3Ryb2tlLXdpZHRoOjEuNXB4O30uY2xzLTJ7ZmlsbDojZmZmO308L3N0eWxlPjwvZGVmcz48dGl0bGU+QXJ0Ym9hcmQgMTwvdGl0bGU+PHBhdGggY2xhc3M9ImNscy0xIiBkPSJNMzEuOCwxNi40QTE1LjQsMTUuNCwwLDAsMCwxLDE2LjRDMSwzMC40LDE2LjQsNDMsMTYuNCw0M1MzMS44LDMwLjQsMzEuOCwxNi40Ii8+PGNpcmNsZSBjbGFzcz0iY2xzLTIiIGN4PSIxNi40IiBjeT0iMTYuMzgiIHI9IjQuNSIvPjwvc3ZnPg=='),
            optimized: false,
        }

        return markerOptions;
    }

    initMap() {
        this.map = new google.maps.Map(this.refs.mapContainer, {
            center: this.props.dealerInfo.dealerCoords,
            zoom: 10,
            maxZoom: 18,
            scrollwheel: false,
            disableDefaultUI: true
        });


        this.marker = new google.maps.Marker(this.mapMarkerConstructor());
        let mapResizeTimer;
        google.maps.event.addDomListener(window, 'resize', () => {
            clearTimeout(mapResizeTimer)
            mapResizeTimer = setTimeout(() => {
                this.map.setCenter(this.props.dealerInfo.dealerCoords);
            }, 500)
        });

    }

    render() {
        let dealerInfo = this.props.dealerInfo;

        const btns = dealerInfo.dealerInfoBtns.map((btn, i) => {
            return (
                <Button key={shortid.generate()} tagName='a' text={btn.btnText} href={btn.btnLink}/>
            )
        })

        return (
            <div className="dealer-info-card">
                <div className="gridcontainer">
                    <div className="grid_12">
                        <div className="dealer-info-card__go-back-btn side-padding">
                            <IconLink
                                iconClass="icon-arrow-left"
                                className="iconBefore"
                                onClick={() => {
                                    this.props.changeDealer(null)
                                }}>
                                Вернуться назад
                            </IconLink>
                        </div>
                    </div>
                    <div className="grid_12">
                        <div className="dealer-info-card__map" ref='mapContainer'></div>
                    </div>
                    <div className="grid_12 grid_xs_8 grid_l_12">
                        <div className="dealer-info-card__title side-padding">{dealerInfo.dealerName}</div>
                    </div>
                    <div className="grid_12 grid_xs_4 grid_l_12">
                        <DealerInfoCardTooltip
                            dealerList={this.props.dealerList}
                            app={this.props.App}
                            changeDealer={this.props.changeDealer}/>
                    </div>
                    <div className="grid_12">
                        <div className="dealer-info-card__info-block side-padding">
                            <div className="dealer-info-card__info-block-title">Контакты</div>
                            <div className="dealer-info-card__info-block-content">{dealerInfo.dealerAddress}
                                <br className="dealer-info-card__info-block-content-break"/>
                                <a href={'tel:' + parsePhone(dealerInfo.dealerPhone)}>{dealerInfo.dealerPhone}</a>
                            </div>
                        </div>
                        <div className="dealer-info-card__info-block side-padding">
                            <div className="dealer-info-card__info-block-title">Персональный менеждер</div>
                            <div
                                className="dealer-info-card__info-block-content">{dealerInfo.dealerManagerName}
                                <br className="dealer-info-card__info-block-content-break"/>
                                <a href={'tel:' + parsePhone(dealerInfo.dealerManagerPhone)}>{dealerInfo.dealerManagerPhone}</a>
                            </div>
                        </div>
                        <div className="dealer-info-card__btns-block side-padding">
                            {btns}
                        </div>

                    </div>
                </div>
            </div>
        );
    }

}

export default DealerInfoCard;
