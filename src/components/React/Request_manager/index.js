import React from 'react';
import {parsePhone} from '../../../globals/js/utility/miscUtils.js';
import EventCard from '../EventCard/index';
import EventCardAboveTheFold from '../EventCard_AboveTheFold/index';


const RequestManager = (props) => {

    const requestInfo = props.requestData.requestInfo;
    const managerInfo = requestInfo.requestManagerInfo;

    let content;
    switch (requestInfo.requestStep) {
        case 'initial': {
            content = 'Спасибо! Ваш менеджер скоро свяжется с вами!';
            break;
        }
        case 'managerInfo': {
            content = <EventCard noBelow>
                <EventCardAboveTheFold cardTitle="Ваш персональный менеджер">
                    <div className="eventCard__manager-block">
                        <div className="eventCard__manager-avatar">
                            <img src={managerInfo.managerAvatar} alt="Ваш менеджер"/>
                        </div>
                        <div className="eventCard__manager-info">
                            <div className="eventCard__manager-title">{managerInfo.managerName}</div>
                            <a className="eventCard__manager-phone"
                               href={'tel:' + parsePhone(managerInfo.managerPhone)}>{managerInfo.managerPhone}</a>
                        </div>
                    </div>
                </EventCardAboveTheFold>
            </EventCard>
            break;
        }
    }

    return (
        <div>{content}</div>

    );
}

export default RequestManager;


