$(document).on('click','.js_offer-card__btns-accept', function(){
	toggleBtnsActive($(this));
	$(this).closest('.js_eventCard').removeClass('below-the-fold-shown');
	$(this).closest('.js_eventCard').find('.js_offer-card__overlay').addClass('is_visible');
})

$(document).on('click','.js_offer-card__btns-decline', function(){
	toggleBtnsActive($(this));
})


function toggleBtnsActive(elem){
	let parentCard = elem.closest('.js_offer-card__container');

	parentCard.find('.js_offer-card__btn').removeClass('is_active');
	elem.addClass('is_active');	
}



export function initOfferSliders(){
	let sliders = $('body').find('.js_timeline-block-with-offers .js_offer-cards-slider');
	updateSliders(sliders)
	$(window).on('resize', function(){
		updateSliders(sliders)
	})
	sliders.on('beforeChange', function(event, slick, currentSlide, nextSlide){
	  sliders.find('.js_eventCard').removeClass('below-the-fold-shown');
	});

	function init(sliders){
		sliders.each(function(i, el){
			let offerCards = $(el).find('.js_eventCard');
			if (offerCards.length > 1){
				 offerCards.wrap('<div class="offer-card__slide"></div>')
				$(el).not('.slick-initialized').slick({
				      dots: true,
				      infinite: false,
				      arrows: false,
				      speed: 300,
				      initialSlide: 0 ,
				      centerMode: true,
				      slidesToShow: 1,
				      centerPadding: '2rem',
				      //adaptiveHeight: true,
				      //slidesToShow: 2,
				      //swipeToSlide: true,
				}).addClass('carousel');
			}
		})
	}


	function killSliders(sliders){
		sliders.each(function(i, el){
			if ($(el).hasClass('slick-initialized')){
				$(el).slick('unslick')
			}
		})
	}

	function updateSliders(sliders){
		if (window.matchMedia("(max-width: 768px)").matches){
			init(sliders)
		}
		else{
			killSliders(sliders)
		}
	}
}
