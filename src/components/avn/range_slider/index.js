import $ from 'jquery';



window.sliderControls = {
  'initSlider': function initSlider(elem, options) {
    var optionsDefault = {
      connect: true,
      range: {
        min: 75000,
        max: 3600000
      },
      format: wNumb({
        decimals: 0,
        thousand: ' '
      }),
      pips: {
        mode: 'positions',
        values: [0, 100],
        density: 1,
        format: wNumb({
          decimals: 0,
          thousand: ' '
        })
      }
    };

    var options = jQuery.extend(optionsDefault, options);
    var slider = $(elem).get(0);
    noUiSlider.create(slider, options);

    var inputs = [];
    $.each(options.start, function(key, value) {
      var input = $.parseHTML('<input hidden type="text" name="' + options.inpNames[key] + '">');
      $(elem).prepend(input);
      inputs.push(input);
    });

    var values = $(elem).find('.noUi-value-large');
    values.eq(0).addClass('first-value');
    values.eq(values.length - 1).addClass('last-value');

    var minValInicator = $('.noUi-value-large.first-value');
    var maxValInicator = $('.noUi-value-large.last-value');

    slider.noUiSlider.on('update', function() {
      minValInicator.text(this.get()[0])
      maxValInicator.text(this.get()[1])
      var that = this;
      $.each(inputs, function(k, inp) {
        $(inp).val(that.get()[k].replace(/\s/g, ''));
      });
    });
    return slider;
  },

  'destroySlider': function destroySlider(elem) {
    var slider = $(elem).get(0);
    slider.noUiSlider.destroy();
  }
};