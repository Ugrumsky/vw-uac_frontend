import 'globals/globals';
import './index.scss';

// HMR setup
if (module.hot) {
  module.hot.accept();
}

$(function(){
  tabs.initSlidingTabs($('.js_tabs__head'));
})