import React from 'react';
import Select from 'react-select';
import * as actions from '../../../actions/index';
import {connect} from 'react-redux';
import _filter from 'lodash/filter';
import _union from 'lodash/union';
import _find from 'lodash/find';

const shortid = require('shortid');

const mapActionsToProps = (dispatch) => {
    return {
        updateOfferFilterTags: (value) => {
            dispatch(actions.updateOfferFilterTags(value));
        },
    };
}

@connect(null, mapActionsToProps)
class OfferTagFilter extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            placeholderIsShown: true,
        };
    }

    handleSelectChange = (value) => {
        let valsToUpdate;
        if (value.length > 0){
            valsToUpdate = value.map((val, i)=>{
                val['isSelected'] = true;
                return val
            })
        }
        else{
            valsToUpdate = this.props.tags.map((val, i)=>{
                val['isSelected'] = false;
                val['isChecked'] = false;
                return val
            })
        }

        const newVal = _union(valsToUpdate, this.props.tags);
        this.props.updateOfferFilterTags(newVal)
    };

    handleClickOnRemoveTag = (value) =>{
        const tags = this.props.tags;
        const targetObj = _find(tags,(o)=>{ return o.value == value});
        targetObj['isSelected'] = false;
        targetObj['isChecked'] = false;
        const newVal = _union(targetObj, this.props.tags);
        this.props.updateOfferFilterTags(newVal);
    };

    handleCheckboxChange = (e) =>{
        const tags = this.props.tags;
        const targetObj = _find(tags,(o)=>{ return o.value == e.target.value});
        targetObj['isChecked'] = !targetObj['isChecked'];
        const newVal = _union(targetObj, this.props.tags);
        this.props.updateOfferFilterTags(newVal);


    }
    handleOnFocus = () => {
        this.setState({
            placeholderIsShown: false,
        })
    };

    handleOnBlur = () => {
        this.setState({
            placeholderIsShown: true,
        })
    };


    render() {
        let placeholderClass = 'tagFilter__select-placeholder';
        placeholderClass += !this.state.placeholderIsShown ? ' is_hidden' : '';

        const selectedTags = _filter(this.props.tags, function(o) { return o.isSelected; });

        let tagsList = selectedTags.map((tag, i) => {
            const tagCount = this.props.offerTagsCount[tag.value];
            const isDisabled = tagCount > 0 ? false : true;

            return (
                <div
                    key = {shortid.generate()}
                    className="tagFilter__tag-item">
                    <label>
                        <input type="checkbox"
                               id={tag.value}
                               value={tag.value}
                               checked = {tag.isChecked}
                               onChange = {(e)=>{this.handleCheckboxChange(e)}}
                               disabled={isDisabled}/>
                        <div className="tagFilter__tag-item-presentation">
                            {tag.label} {tagCount}
                        </div>
                    </label>
                    <div className="tagFilter__tag-item-remove hasicon icon-base-close" onClick={() => {
                        this.handleClickOnRemoveTag(tag.value)
                    }}></div>
                </div>

            );
        });

        return (
            <div className="offerTagFilter__container">
                <div className="tagFilter">
                    <div className="tagFilter__select">
                        <Select
                            name="form-field-name"
                            value={selectedTags}
                            multi={true}
                            options={this.props.tags}
                            placeholder = ''
                            onChange={this.handleSelectChange}
                            onBlur = {this.handleOnBlur}
                            onFocus = {this.handleOnFocus}
                        />
                        <div className={placeholderClass}>+ Добавить интерес</div>
                    </div>
                    {tagsList}
                </div>
            </div>
        );
    }
}



export default OfferTagFilter;
