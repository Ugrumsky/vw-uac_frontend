import React from 'react';
import EventCardSimpleText from '../EventCard_SimpleText/index';

const EventCardTextArea = (props) => {

    return (
        <div className="eventCard__textarea">
            <EventCardSimpleText>
                <b>{props.title}</b>
            </EventCardSimpleText>
            <textarea className="input__field mb20" placeholder={props.placeholder}/>
        </div>
    );

}

export default EventCardTextArea;
