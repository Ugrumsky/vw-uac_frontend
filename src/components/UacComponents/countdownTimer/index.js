function startTimer(duration, display, callback) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;

        if (--timer < 0) {
            timer = duration;
            callback();
        }
    }, 1000);
}
/*
window.onload = function () {
    var fiveMinutes = 15,
        display = document.querySelector('.js_phone-confirm-counter');
    startTimer(fiveMinutes, display,function(){
        console.log('timer finished')
    });
};*/