import BasicContentTeaser from '../../TeaserModules/m200_Basic_Content_Teaser/index.js';
import './index.scss';
import Component, {findComponent} from '../../../globals/js/Libraries/component';

import tabNavItem from '../../NavigationModules/mk505_Tab_Nav/index.js';
import $ from 'jquery';

import Slider from '../../../globals/js/Libraries/slider';
import {arrowTheme} from '../../UI_Elements/pagination/arrows/index';

const ARROW_THEME_NOBG = arrowTheme('nobg');

class HighlightModuleSlide extends BasicContentTeaser {
  constructor(element, index) {
    super($(element));
    this.slickSlideId = index;
  }

  addTabLink(tabNav, slider) {
    const self = this;


    this.tabNavItem = $(tabNavItem({
      url: '#',
      text: this.subline.text(),
    })).on('click', function(e) {
      e.preventDefault();
      slider.goTo(self.slickSlideId);
    });

    if (this.slickSlideId === 0) {
      this.tabNavItem.addClass('is-active');
    }

    slider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
      if (self.slickSlideId === nextSlide) {
        self.tabNavItem.addClass('is-active');
      } else {
        self.tabNavItem.removeClass('is-active');
      }
    });

    tabNav.append(this.tabNavItem);
  }
}


class HighlightModule extends Component {
  constructor(element) {
    super(element);
    this.wrapper = $(element);
    this.tabNav = this.wrapper.find('.m155__tab_nav');
    this.inner = this.wrapper.find('.m155__inner');

    this.slides = this.inner.children('.m155_item').map((index, el) => new HighlightModuleSlide(el, index));

    this.dots = this.wrapper.find('.m155__dots-list');
    this.controls = this.wrapper.find('.m155__arrow-inner');
    this.slider = new Slider(this.inner, {
      appendArrows: this.controls,
      appendDots: this.dots,
      centerMode: true,
      prevArrow: ARROW_THEME_NOBG.left,
      nextArrow: ARROW_THEME_NOBG.right,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            dots: false,
          },
        },
      ],
    });
    this.initComponent();
    this.init();
  }

  init() {
    const self = this;
    for (let i = 0; i < this.slides.length; i++) {
      this.slides[i].addTabLink(this.tabNav, this.slider);
    }

    if (this.slides.length === 1) {
      this.inner.addClass('is-one-element');
    }

    $(() => {
      self.slider.mount();
    });
  }
}


findComponent('.m155', HighlightModule);


