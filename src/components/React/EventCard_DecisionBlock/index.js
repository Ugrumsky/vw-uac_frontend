import React from 'react';
import Button from '../Button/index';
import RadioBtn from '../RadioBtn/index';

import EventCardForm from '../EventCard_Form/index';
import EventCardSimpleText from '../EventCard_SimpleText/index';
import EventCardFlexContainer from '../EventCard_FlexContainer/index';

const shortid = require('shortid');

class EventCardDecisionBlock extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showRadios: false,
            userDecision: 'success',
        };
    }

    toggleDeclineBlockVisibility = () => {
        this.setState({
            showRadios: !this.state.showRadios,
            declineCause: false
        });
    };

    handleRadios = (e) => {
        this.setState({
            declineCause: e.target.value,
            userDecision: 'fail',
        });
    };

    resolveStep = (data) => {
        this.props.resolveStep(data);
    };

    render() {

        const radiosObj = this.props.radios;

        const radios = radiosObj.btns.map((radio, i) => {
            const checked = radio.value == this.state.declineCause ? true : false;
            return (
                <RadioBtn key={shortid.generate()}
                          name={radiosObj.name}
                          checked={checked}
                          value={radio.value}
                          text={radio.title}
                          onChange={this.handleRadios}/>
            );
        })

        let decisionBlock;

        let resolveDeclineBtnClass = 'btn_cta';

        if (!this.state.declineCause) resolveDeclineBtnClass += ' disabled';

        if (this.state.showRadios) {
            decisionBlock = <EventCardForm>
                <EventCardSimpleText>
                    {this.props.title}
                </EventCardSimpleText>
                <EventCardFlexContainer>
                    {radios}
                </EventCardFlexContainer>
                <EventCardFlexContainer>
                    <Button className={resolveDeclineBtnClass}
                            tagName='div' text='Завершить'
                            onClick={() => {
                                this.resolveStep({
                                    dealerId: this.props.dealerId,
                                    requestId: this.props.requestId,
                                    userDecision: this.state.userDecision,
                                    declineCause: this.state.declineCause,
                                });
                            }}/>
                    <Button tagName='div'
                            text='Отмена'
                            onClick={this.toggleDeclineBlockVisibility}/>
                </EventCardFlexContainer>
            </EventCardForm>;
        }
        else {
            decisionBlock = <EventCardForm>
                <EventCardFlexContainer>
                    <Button className='btn_cta'
                            tagName='div'
                            text='Принять'
                            onClick={() => {
                                this.resolveStep({
                                    dealerId: this.props.dealerId,
                                    requestId: this.props.requestId,
                                    userDecision: this.state.userDecision,
                                    declineCause: this.state.declineCause,
                                });
                            }}/>
                    <Button tagName='div'
                            text='Отказаться'
                            onClick={this.toggleDeclineBlockVisibility}/>
                </EventCardFlexContainer>
            </EventCardForm>;
        }

        return (
            <div>{decisionBlock}</div>
        );
    }
}


export default EventCardDecisionBlock;


