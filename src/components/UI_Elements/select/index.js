// import './index.scss';
//import {defaultSelectElementInit} from './init';

import $ from 'jquery';

require("expose-loader?selectInit!./init.js")

document.addEventListener('DOMContentLoaded', (event) => {
	initSelects()
});





$(document).on('change', '.native-select select',  function(){
	let val = $(this).find("option:selected").text();
	let faux = $(this).prev();

	$(this).prev().html(val)
	faux.removeClass('placeholder')
})

$(document).on('change', 'select',  function(){
	let cont = $(this).closest('.select_wrapper');

	cont.removeClass('placeholder')
})


export let initSelects = function (){
	let touchsupport = ('ontouchstart' in window) || (navigator.maxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0);
	let elements = $('.select__field');
	elements.each(function(){
		if(!$(this).hasClass('inited')){
			$(this).addClass('inited');
			if (!touchsupport) {
			    selectInit.defaultSelectElementInit($(this)[0]);
			  }
			else{
				$(this).closest('.select_wrapper').addClass('native-select')
			}
		}
	})
}

