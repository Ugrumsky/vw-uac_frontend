/**
 * Created by ufs on 3/17/2017.
 */
import Accordion from '../../ContentModules/m108_Accordion/index';
import Component, {findComponent} from '../../../globals/js/Libraries/component';
import _toArray from 'lodash/toArray';
import _map from 'lodash/map';
// Import styles
import './index.scss';


class TrimsList extends Component {
  constructor(el) {
    super(el);
    this.color = false;
    this.accordions = _map(_toArray(this.componentElement
      .querySelectorAll('.mk268_trim__features-list, .mk268_more > .m108_Accordion')), (node) => new Accordion(node));

    console.log(this);
    this.initComponent();
  }
}

findComponent('.mk268', TrimsList);
