import React from 'react';
import {connect} from 'react-redux';
import * as actions from '../../../actions/index';
const classNames = require('classnames');
const shortid = require('shortid');


const mapStateToProps = (state) => {
    return {
        dealerList: state.DealerList,
        main: state.Main
    }
}

const mapActionsToProps = (dispatch) => {
    return {
        changeDealer: (id) => {
            dispatch(actions.changeDealer(id))
        }
    }
}

@connect(mapStateToProps, mapActionsToProps)
class DealerInfoCardTooltip extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            additionalDealersShown: false,
        };
    }

    toggleAdditionalDealersVisibility = () => {
        this.setState({
            additionalDealersShown: !this.state.additionalDealersShown
        });
    };

    changeDealer = (id) => {
        this.props.changeDealer(id);
    }


    render() {
        let tooltipClass = 'dealer-info-card__select-dialogue-block side-padding';
        if (this.state.additionalDealersShown) tooltipClass += ' is_visible';

        const otherDealers = [];
        for (let key in this.props.dealerList) {
            if (key != this.props.main.currentSelectedDealerId) {
                otherDealers.push({
                    title: this.props.dealerList[key].dealerName,
                    id: key
                })
            }
        }
        const otherDealersRender = otherDealers.map((dealer, i) => {

            return (
                <li key={shortid.generate()} onClick={() => {
                    this.changeDealer(dealer.id)
                }}>{dealer.title}</li>
            )
        })
        return (
            <div className={tooltipClass}>
                <div className="dealer-info-card__select-dialogue-toggler"
                     onClick={this.toggleAdditionalDealersVisibility}>
                    <span>Выбрать другой дилерский центр</span></div>
                <div className="dealer-info-card__select-dialogue">
                    <div className="dealer-info-card__select-dialogue-tail"></div>
                    <div className="dealer-info-card__select-dialogue-content">
                        <div
                            className="dealer-info-card__select-dialogue-close-btn hasicon icon-navigation-close"
                            onClick={this.toggleAdditionalDealersVisibility}></div>
                        <div className="dealer-info-card__select-dialogue-content-title">Выбрать другого
                            дилера
                        </div>
                        <ul className="dealer-info-card__select-dialogue-content-links">
                            {otherDealersRender}
                        </ul>
                    </div>
                </div>
            </div>
        );
    }

}

export default DealerInfoCardTooltip;
