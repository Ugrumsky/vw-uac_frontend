/*service select*/
export default function makeCitySelect(array){
  
    let options = `<option class="select__item" value="all" selected="" hidden="">Вcе города</option>`;
    array.forEach(function(item){
      options = options + `<option class="select__item" value="${item.id}">${item.name}</option>`;
    })
    let select = `
    <div class="select_wrapper placeholder vwd5_select js_error_container" >
      <div class="select_wrapper__group">
        <div class="faux-select-native" data-placeholder="Выберите ваш город">Выберите ваш город</div>
        <select class="select__field" id ="dealerCitiesSelect" data-searchable="true" >
        ${options}
        </select>
      </div><span class="select_wrapper__help-block error-container">Город не выбран</span>
    </div>
    `
    if($('#js_dealer-locator__filter-block').find('#dealerCitiesSelect').length == 0){
      $('#js_dealer-locator__filter-block').append(select);
    }
}