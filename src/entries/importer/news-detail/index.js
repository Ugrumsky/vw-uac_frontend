import 'globals/globals';
import './index.scss';

// HMR setup
if (module.hot) {
  module.hot.accept();
}


$(function(){
	announcementSliders.initAnnouncementSliders();
    $('.news-item').find('img, table').addClass('pull_sm_2')
})