import 'globals/globals';
import './index.scss';

// Page related components
import '../../../components/TeaserModules/m211_Next_Steps_Teaser/index';
import '../../../components/ContentModules/m103_Copy_Media/index';
import '../../../components/ContentModules/m151_Introcopy/index';
import '../../../components/StageModules/m310_Editorial_Stage/index';
import '../../../components/DwaComponents/mk171_DwaHeader/index';
import '../../../components/DwaComponents/mk271_StepIcons/index';
import '../../../components/NavigationModules/mk571_StepList/index';
import '../../../components/NavigationModules/mk572_Carla/index';
import '../../../components/FunctionalModules/mk615_Step_Form/index';

/*
import {dealerData} from '../../../globals/mocks/dealerMocks.js';
import {carModels} from '../../../globals/mocks/tradein-car-models.js';
import {vwCarModels} from '../../../globals/mocks/vw_cars.js';

window.dealerData = dealerData;
window.carModels = carModels;
window.vwModels = vwCarModels;
*/
if (module.hot) {
  module.hot.accept();
}

window.citiesList = [];
let currentSelectedCityId;
let currentSelectedCity;
let currentSelectedCityDealersList;
let currentSelectedCityDealersListFiltered;
let currentSelectedDealersAmount;
let currentSelectedDealersIds = [];


/*************first step***************/


    /*VW cars*/
    function makeWishVwSelect(obj) {

      let options = `<option class="select__item" value="" disabled="" selected="" hidden="">Модель *</option>`;
      for (key in obj) {
        options = options + `<option class="select__item" value="${key}">${obj[key]}</option>`;
      }
      let select = `
        <div class="select_wrapper placeholder vwd5_select js_error_container" >
          <div class="select_wrapper__group">
            <div class="faux-select-native" data-placeholder="Модель *">Модель *</div>
            <select class="select__field" id ="dealerCitiesSelect" data-searchable="true" name="wish_car_new">
            ${options}
            </select>
          </div><span class="select_wrapper__help-block error-container">Модель</span>
        </div>
        `
      if ($('body').find('#wish-car-vw-select').children().length == 0) {
        $('#wish-car-vw-select').append(select);
      }
    }
    /*Other cars*/
    function makeWishCarManufacturerSelect(obj) {

      let options = `<option class="select__item" value="" disabled="" selected="" hidden="">Марка *</option>`;
      for (key in obj) {
        options = options + `<option class="select__item" value="${obj[key].brand_id}">${key}</option>`;
      }
      let select = `
        <div class="select_wrapper placeholder vwd5_select js_error_container" >
          <div class="select_wrapper__group">
            <div class="faux-select-native" data-placeholder="Марка *">Марка *</div>
            <select class="select__field" data-searchable="true" name="wish_used_brand">
            ${options}
            </select>
          </div><span class="select_wrapper__help-block error-container">Марка не выбрана</span>
        </div>
        `
      if ($('body').find('#wish-car-other-manufacturer-select').children().length == 0) {
        $('#wish-car-other-manufacturer-select').append(select);
      }
    }

    function makeWishCarModelSelect(obj) {

      let options = `<option class="select__item" value="" disabled="" selected="" hidden="">Модель *</option>`;
      for (key in obj) {
        options = options + `<option class="select__item" value="${key}">${obj[key]}</option>`;
      }
      let select = `
        <div class="select_wrapper placeholder vwd5_select js_error_container" >
          <div class="select_wrapper__group">
            <div class="faux-select-native" data-placeholder="Модель *">Модель *</div>
            <select class="select__field required" data-searchable="true" required name="wish_used_model">
            ${options}
            </select>
          </div><span class="select_wrapper__help-block error-container">Модель не выбрана</span>
        </div>
        `
      if ($('body').find('#wish-car-other-model-select').children().length == 0) {
        $('#wish-car-other-model-select').append(select);
      }
    }

    $(document).on('change', '#wish-car-other-manufacturer-select select', function() {
      $('#wish-car-other-model-select').empty()
      var val = $(this).val();
      var wishCarManufacturerModelsObj;
      for (key in carModels) {
        if (window.carModels[key]['brand_id'] == val) {
          wishCarManufacturerModelsObj = carModels[key]['models']
        }
      }
      makeWishCarModelSelect(wishCarManufacturerModelsObj)
      selects.initSelects();
      $(this).closest('.mk615').data('_component').addValidation();
    })

    $(function() {
      makeWishVwSelect(vwModels);
      makeWishCarManufacturerSelect(carModels);
      selects.initSelects();
    })






/*************second step***************/



    function makeCurrentCarManufacturerSelect(obj) {

      let options = `<option class="select__item" value="" disabled="" selected="" hidden="">Марка</option>`;
      for (key in obj) {
        options = options + `<option class="select__item" value="${obj[key].brand_id}">${key}</option>`;
      }
      let select = `
        <div class="select_wrapper placeholder vwd5_select js_error_container" >
        <label class="select_wrapper__title" for="currentCarManufacturerSelect">Марка </label>
          <div class="select_wrapper__group">
            <div class="faux-select-native" data-placeholder="Марка">Марка</div>
            <select class="select__field required" id ="currentCarManufacturerSelect" data-searchable="true" name="current_car_brand">
            ${options}
            </select>
          </div><span class="select_wrapper__help-block error-container">Марка не выбрана</span>
        </div>
        `
      if ($('body').find('#dwa-form__current-car-manufacturer').children().length == 0) {
        $('#dwa-form__current-car-manufacturer').append(select);
      }
    }

    function makeCurrentCarModelSelect(obj) {

      let options = `<option class="select__item" value="" disabled="" selected="" hidden="">Модель</option>`;
      for (key in obj) {
        options = options + `<option class="select__item" value="${key}">${obj[key]}</option>`;
      }
      let select = `
        <div class="select_wrapper placeholder vwd5_select js_error_container" >
        <label class="select_wrapper__title" for="currentCarModelSelect">Модель </label>
          <div class="select_wrapper__group">
            <div class="faux-select-native" data-placeholder="Модель">Модель</div>
            <select class="select__field required" id ="currentCarModelSelect" data-searchable="true" name="current_car_model">
            ${options}
            </select>
          </div><span class="select_wrapper__help-block error-container">Модель не выбрана</span>
        </div>
        `
      if ($('body').find('#dwa-form__current-car-model').children().length == 0) {
        $('#dwa-form__current-car-model').append(select);
      }
    }

    $(document).on('change', '#dwa-form__current-car-manufacturer select', function() {
      $('#dwa-form__current-car-model').empty()
      var val = $(this).val();
      var currentSelectedManufacturerModelsObj;
      for (key in carModels) {
        if (window.carModels[key]['brand_id'] == val) {
          currentSelectedManufacturerModelsObj = carModels[key]['models']
        }
      }
      makeCurrentCarModelSelect(currentSelectedManufacturerModelsObj)
      selects.initSelects();
      $(this).closest('.mk615').data('_component').addValidation();
    })


    $(function() {
      makeCurrentCarManufacturerSelect(carModels);
      selects.initSelects();
    })





/*************third step***************/



    /*Map*/
    map = false;
    let markers = [];


    $(function() {
      $.each(dealerData, function(cityId, data) {
        if  (cityId != ''){
        citiesList.push({
          name: data.CITY.NAME,
          id: data.CITY.ID
        })
      }
        /*$.each(data.DEALERS, function(i, dealer) {
          dealer.cityId = cityId;
          dealersArr.push(dealer)
        });*/
      });
    })

    function markerClickHandler(dealer) {

      $('[name=dealer]').val(dealer.ID);
      makeMapConfirm(dealer);

    }

    function setMarker(lat, lng, title, map) {
      if (typeof(lat) == 'undefined') {
        lat = 55.714916;
      }
      if (typeof(lng) == 'undefined') {
        lng = 37.718094;
      }
      if (typeof(title) == 'undefined') {
        title = '';
      }
      if (typeof(map) == 'undefined') {
        return false;
      }
      let iconImg =  {
        
        url: 'data:image/svg+xml;base64,PHN2ZyBpZD0iTGF5ZXJfMSIgZGF0YS1uYW1lPSJMYXllciAxIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAzMyA0NCI+PGRlZnM+PHN0eWxlPi5jbHMtMXtmaWxsOiMwMDk5ZGE7c3Ryb2tlOiNmZmY7c3Ryb2tlLXdpZHRoOjEuNXB4O30uY2xzLTJ7ZmlsbDojZmZmO308L3N0eWxlPjwvZGVmcz48dGl0bGU+QXJ0Ym9hcmQgMTwvdGl0bGU+PHBhdGggY2xhc3M9ImNscy0xIiBkPSJNMzEuOCwxNi40QTE1LjQsMTUuNCwwLDAsMCwxLDE2LjRDMSwzMC40LDE2LjQsNDMsMTYuNCw0M1MzMS44LDMwLjQsMzEuOCwxNi40Ii8+PGNpcmNsZSBjbGFzcz0iY2xzLTIiIGN4PSIxNi40IiBjeT0iMTYuMzgiIHI9IjQuNSIvPjwvc3ZnPg==',
       size: new google.maps.Size(33, 44),
        origin: new google.maps.Point(0, 0),
        //anchor: new google.maps.Point(0, 0),
        scaledSize: new google.maps.Size(33, 44),
    }
      var marker = new google.maps.Marker({
        position: {
          lat: parseFloat(lat),
          lng: parseFloat(lng)
        },
        map: map,
        title: title,
        icon: iconImg,
        optimized: false,
      });
      return marker;
    }

    function drawDealerMarkers(map) {
      if (typeof(map) == 'undefined') {
        return false;
      }
      clearMap();
      $.each(currentSelectedCityDealersListFiltered, function(i, dealer) {
        marker = setMarker(dealer.LATITUDE, dealer.LONGITUDE, dealer.NAME, map);
        marker.addListener('click', function() {
          markerClickHandler(dealer)
        });
        markers.push(marker);
      });
    }

    function clearMap(){
      $.each(markers, function(i,d){
        d.setMap(null);
      })
      markers = [];
    }

    function getBoundsFromMarkers(mrks) {
      if (typeof(mrks) == 'undefined') {
        mrks = [];
      }
      var bound = new google.maps.LatLngBounds();
      $.each(mrks, function(i, marker) {
        bound.extend(marker.getPosition());
      });
      return bound;
    }

    function setMapForCity(cityId) {

      var dealers = markers;
      if (typeof(dealers) == 'undefined') {
        return false;
      }
      var bounds = getBoundsFromMarkers(dealers);
      map.fitBounds(bounds);
    }

    function initMap() {
      map = new google.maps.Map(document.getElementById('dwa-form__dealer-map'), {
        center: {
          lat: 55.714916,
          lng: 37.718094
        },
        zoom: 10,
        maxZoom: 16,
        scrollwheel: false,
      });
      drawDealerMarkers(map, 174);
    }


    $(function() {
      initMap();
    })

    $(function() {
      citiesList.sort(function(a, b) {
        if (a.name.toLowerCase() < b.name.toLowerCase())
          return -1;
        if (a.name.toLowerCase() > b.name.toLowerCase())
          return 1;
        return 0;
      });
      makeCitySelect(citiesList);
    })


    function makeCitySelect(array) {

      let options = `<option class="select__item" value="" disabled="" selected="" hidden="">Выберите ваш город</option>`;
      array.forEach(function(item) {
        options = options + `<option class="select__item" value="${item.id}">${item.name}</option>`;
      })
      let select = `
        <div class="select_wrapper placeholder vwd5_select js_error_container" >
        <label class="select_wrapper__title" for="dealerCitiesSelect">Город </label>
          <div class="select_wrapper__group">
            <div class="faux-select-native" data-placeholder="Выберите ваш город">Выберите ваш город</div>
            <select class="select__field required" id ="dealerCitiesSelect" data-searchable="true" required name="city">
            ${options}
            </select>
          </div><span class="select_wrapper__help-block error-container">Город не выбран</span>
        </div>
        `
      if ($('body').find('#dwa-form__city-select').children().length == 0) {
        $('#dwa-form__city-select').append(select);
      }
    }

    function makeDealerSelect(array, required) {

      let options = `<option class="select__item" value="" disabled="" selected="" hidden="">Выберите дилера</option>`;
      array.forEach(function(item) {
        options = options + `<option class="select__item" value="${item.XML_ID}">${item.NAME + ' ' + item.ADDRESS}</option>`;
      })
      let requiredSelect = required ? `<select class="select__field required" data-searchable="true" required name='dealers'>` : `<select class="select__field" data-searchable="true" name='dealers'>`
      let select = `
        <div class="select_wrapper placeholder vwd5_select js_error_container" >
        <label class="select_wrapper__title" >Дилер </label>
          <div class="select_wrapper__group">
            <div class="faux-select-native" data-placeholder="Выберите дилера">Выберите дилера</div>
            ${requiredSelect}
            ${options}
            </select>
          </div><span class="select_wrapper__help-block error-container">Дилер не выбран</span>
        </div>
        `
      $('#dwa-form__dealer-selects').append(select);
    }

    $(document).on('change', '#dealerCitiesSelect', function() {
      $('#dwa-form__dealer-selects').empty();
      currentSelectedCityId = $(this).val();
      currentSelectedCity = dealerData[currentSelectedCityId];
      if (typeof(currentSelectedCity) == undefined || !currentSelectedCity) {
      } else {
        currentSelectedCityDealersList = $.map(currentSelectedCity.DEALERS, function(value, index) {
          return [value];
        });
        currentSelectedCityDealersList.sort(function(a, b) {
          if (a.NAME.toLowerCase() < b.NAME.toLowerCase())
            return -1;
          if (a.NAME.toLowerCase() > b.NAME.toLowerCase())
            return 1;
          return 0;
        });
        currentSelectedCityDealersListFiltered = currentSelectedCityDealersList.slice(0);
        currentSelectedDealersAmount = currentSelectedCityDealersList.length;
        makeDealerSelect(currentSelectedCityDealersList, true)
        selects.initSelects();
        $('#js_dwa-form__add-dealers-btn').hide();
        $('#js_dwa-form__dealer-section').slideDown(300);
        drawDealerMarkers(map, currentSelectedCityId);
        setTimeout(function() { /*doesnt draw when hidden*/
          google.maps.event.trigger(map, 'resize');
          setMapForCity(currentSelectedCityId)
        }, 500)
        $('#js_dwa-form__dealer-map__container').find('.js_map-confirm-dealer-selection').remove();
        checkForLastOrOnlyDealer();
        $(this).closest('.mk615').data('_component').addValidation()
      }
    });

    $(document).on('change', '#dwa-form__dealer-selects select', function() {
      let position;
      let val = $('#dwa-form__dealer-selects .select_wrapper').last().find('select').val();

      currentSelectedCityDealersList.forEach(function(item, i) {
        if (item['XML_ID'] == ''){
          setMapForCity(currentSelectedCityId);
        }
        else if (item['XML_ID'] == val) {
          position = {
            lat: parseFloat(item['LATITUDE']),
            lng: parseFloat(item['LONGITUDE'])
          }
        }
      })
      $('#js_dwa-form__dealer-map__container').find('.js_map-confirm-dealer-selection').remove();
      map.panTo(position);
      map.setZoom(16);
      checkIfShowMoreDealersBtn();
      $(this).closest('.mk615').data('_component').validateTab()
    })

    $(document).on('click', '#js_dwa-form__add-dealers-btn--confirm', function() {
      $('#dwa-form__dealer-selects .select_wrapper').addClass('is_disabled');
      $('#dwa-form__dealer-selects').append(makeRemoveDealerBtn())
      $('#js_dwa-form__add-dealers-btn').hide();
      updateSelectedDealersIdsArr();
      filterSelectedDealers()
      makeDealerSelect(currentSelectedCityDealersListFiltered, false)
      selects.initSelects();
      clearMap();
      drawDealerMarkers(map);
      setMapForCity(currentSelectedCityId)
      checkForLastOrOnlyDealer()
      $('body').find('.mk615').eq(0).data('_component').addValidation()
    })

    function checkIfShowMoreDealersBtn() {
      if (currentSelectedDealersAmount > 1 && currentSelectedDealersIds.length < 2) {
        $('#js_dwa-form__add-dealers-btn').show();
      }
    }

    function makeRemoveDealerBtn() {
      let tmp = `<div class="dwa-form__remove-dealers-btn mb20 js_dwa-form__remove-dealers-btn">
                      <div class="btn" role="button"><span class="btn__text">Удалить дилера</span></div>
                    </div>`
      return tmp
    }

    $(document).on('click', '.js_dwa-form__remove-dealers-btn', function() {
      $(this).prev().remove();
      $(this).remove();
      let tempVal = $('#dwa-form__dealer-selects .select_wrapper').last().find('select').val();
      $('#dwa-form__dealer-selects .select_wrapper').last().remove()
      updateSelectedDealersIdsArr();
      filterSelectedDealers()
      makeDealerSelect(currentSelectedCityDealersListFiltered, false)
      selects.initSelects();
      let lastAddedSelect = $('#dwa-form__dealer-selects').find('.select_wrapper').last().find('select');
      if (typeof(lastAddedSelect.prop('_instance')) != 'undefined') {
        lastAddedSelect.prop('_instance').setValueByChoice('' + tempVal); 
      } else {
        lastAddedSelect.val(tempVal);
      }
      $('#dwa-form__dealer-selects select').first().addClass('required');
      clearMap();
      drawDealerMarkers(map);
      setMapForCity(currentSelectedCityId)
      $('body').find('.mk615').eq(0).data('_component').addValidation();
      $('#dwa-form__dealer-selects select').first().trigger('change');
    })


    function updateSelectedDealersIdsArr(){
      currentSelectedDealersIds = [];
      $('#dwa-form__dealer-selects select').each(function(i, el){
        currentSelectedDealersIds[i] = $(this).val()
      })

    }
    function checkForLastOrOnlyDealer(){
    let lastAddedSelect = $('#dwa-form__dealer-selects').find('.select_wrapper').last().find('select');
      if (typeof(lastAddedSelect.prop('_instance')) != 'undefined') {
        if(lastAddedSelect.closest('.select').find('.select__item--choice.select__item--selectable').length == 1){
          let tempVal = lastAddedSelect.closest('.select').find('.select__item--choice.select__item--selectable').data('value');
          lastAddedSelect.prop('_instance').setValueByChoice('' + tempVal); //!!!!
        }
      } else {
        //lastAddedSelect.val(tempVal);
      }
    }
    function filterSelectedDealers(){
      currentSelectedCityDealersListFiltered = currentSelectedCityDealersList.slice(0);
      currentSelectedCityDealersListFiltered = currentSelectedCityDealersListFiltered.filter(function(x) { return currentSelectedDealersIds.indexOf(x.XML_ID) < 0 })
    }
    /*map modal*/

    function makeMapConfirm(item) {

      var mapContainer = $('#js_dwa-form__dealer-map__container');

      var modalConfirm = `
    <div class="js_map-confirm-dealer-selection map-confirm-dealer-selection">
        <div class="map-confirm-dealer-selection__content">
            <div class="map-confirm-dealer-selection__close-btn hasicon icon-navigation-close js_map-confirm-dealer-selection__close-btn"></div>
            <div class="map-confirm-dealer-selection__name">${item['NAME']}</div>
            <div class="map-confirm-dealer-selection__address">${item['ADDRESS']}</div>
            <div class="mt20 js_dwa-form__add-dealers-btn-map" data-dealerid="${item['XML_ID']}">
              <div class="btn block-centered" role="button"><span class="btn__text">Выбрать</span></div>
            </div>
        </div>
    </div>`

      mapContainer.append(modalConfirm)

    }

    $(document).on('click', '.js_dwa-form__add-dealers-btn-map', function() {
      let dealerId = $(this).data('dealerid')
      $(this).closest('.js_map-confirm-dealer-selection').remove();
      let lastAddedSelect = $('#dwa-form__dealer-selects').find('.select_wrapper').last().find('select');
      if (typeof(lastAddedSelect.prop('_instance')) != 'undefined') {
        lastAddedSelect.prop('_instance').setValueByChoice('' + dealerId);
      } else {
        lastAddedSelect.val(dealerId);
      }
      $('#dwa-form__dealer-selects select').trigger('change');
    })

    $(document).on('click', '.js_map-confirm-dealer-selection__close-btn', function() {
      $(this).closest('.js_map-confirm-dealer-selection').remove();
    })

    $(document).on('click', '#js_dwa-form__add-comment-trigger', function() {
        $('#js_dwa-form__add-comment__content').slideDown(300);
        $(this).hide();
    })

    $('[name=rules_agree]').on('click', function() {
      if ($(this).is(':checked')) {
        //$(this).trigger('kdxFieldValidator', [true]);
        $('[name=adv_agree]').prop('checked', true);
      } else {
        //$(this).trigger('kdxFieldValidator', [false]);
        $('[name=adv_agree]').prop('checked', false);
      }
      //$(this).closest('.mk615').data('_component').validateTab()
    });
    $('[name=adv_agree]').on('click', function() {
      if ($(this).is(':checked')) {
        $('[name=rules_agree]').prop('checked', true).trigger('change');
        //$('[name=rules_agree]').trigger('kdxFieldValidator', [true]);
      }
      //$(this).closest('.mk615').data('_component').validateTab()
    });

$(document).on('formSubmit',function(e, target){
  e.stopPropagation();

  let submit_data = new FormData();
  const form = $(target).find('form');
  const formData = form.data('formData')
  let topLoader =  $('#js_top-loader__container').data('_component');

  if( form.find('.js_dragNdropUpload').length > 0){
    let dZ =  form.find('.js_dragNdropUpload').get(0).dropzone;
    let photos = dZ.getAcceptedFiles();

    $.each(photos, function (strId, objFile) {
      submit_data.append('images[]',objFile);
    });
    
  }

  for ( var key in formData ) {
    if (typeof formData[key] != 'undefined' && formData[key] instanceof Array){
      $.each(formData[key], function (strId, val) {
        submit_data.append(key + '[]', val);
      });
    }
    else{
      submit_data.append(key, formData[key]);
    }
    //submit_data.append(key, formData[key]);
  }


  //submit_data.append('test', true); //for testing

  /*tracking*/
  Bootstrapper.ngw.ngw_environment = {
    ApplicationID: 'LocalRUDWA',
    Language: 'ru',
    Country: 'RU'
};
  Bootstrapper.ngw.ngw_user = {
      BrowserResolutionHeight: window.innerHeight || document.body.clientHeight,
      BrowserResolutionWidth: window.innerWidth || document.body.clientWidth,
      BrowserResolutionBreakpoint: 900,
      BrowserResolutionOrientation: window.innerHeight > window.innerWidth ? 'v' : 'h'
  };

  Bootstrapper.ngw.ngw_partner = {
      InformationBnr: '',
      InformationName: '',
      InformationDepartment: 'sales',
      InformationZIP: ''
  };
  Bootstrapper.ngw.ngw_eventInfo = {
      eventAction: 'VWBasic_FormCTA_Click',
      pageId: window.location.href,
      pageName: 'DWA',
      contentID: ''
  };
  Bootstrapper.ensEvent.trigger('VWBasic_FormCTA_Click');
  /*/tracking*/

$.ajax({
    xhr: function () {
        let xhr = new window.XMLHttpRequest();
         xhr.upload.addEventListener("loadstart", function (evt) {
          topLoader.show();
          topLoader.setBarWidth(0);
        }, false);

        xhr.upload.addEventListener("progress", function (evt) {
            if (evt.lengthComputable) {
                let percentComplete = evt.loaded / evt.total;
                topLoader.setBarWidth(percentComplete);
                if (percentComplete === 1) {
                    topLoader.hide();
                }
            }
        }, false);
        xhr.addEventListener("progress", function (evt) {
            if (evt.lengthComputable) {
                let percentComplete = evt.loaded / evt.total;
                topLoader.setBarWidth(percentComplete);
            }
        }, false);
        return xhr;
    },
    type: 'POST',
    url: "http://cars.volkswagen.ru/ajax/forms/online_assessment.php",
    data: submit_data,
    processData: false,
    contentType: false,
    dataType: 'json',
    success: function (data) {
      if (data.hasOwnProperty('error')){
        $('#js_system-notification__container').data('_component').updateContent(`<p>${data.error}</p>`);
        $('#js_system-notification__container').data('_component').show();
        $('#js_system-notification__container').data('_component').hideOnTimeout(3000);
      }
      else{
        $(target).hide();
        $('.js_submit-message-success').addClass('is_visible');
        /*tracking*/
        ga('send', 'event', 'CarValuationFormRequest', 'success', window.location.href);

         var _rutarget = window._rutarget || [];
          _rutarget.push({'event': 'thankYou', 'conv_id': 'dwa_form', 'sku': 'dwa_form' });

        Bootstrapper.ngw.ngw_environment = {
            ApplicationID: 'LocalRUDWA',
            Language: 'ru',
            Country: 'RU'
        };
        Bootstrapper.ngw.ngw_user = {
            BrowserResolutionHeight: window.innerHeight || document.body.clientHeight,
            BrowserResolutionWidth: window.innerWidth || document.body.clientWidth,
            BrowserResolutionBreakpoint: 900,
            BrowserResolutionOrientation: window.innerHeight > window.innerWidth ? 'v' : 'h'
        };
        Bootstrapper.ngw.ngw_eventInfo = {
            eventAction: 'VWBasic_FormCTA_Click',
            pageId: window.location.href,
            pageName: 'DWA',
            contentID: ''
        };
        Bootstrapper.ensEvent.trigger('VWBasic_FormSubmissionSuccessMessage_Load');
        /*/tracking*/
      }
    },
    error: function (data) {
      $('#js_system-notification__container').data('_component').updateContent('<p>Форма не отправлена, попробуйте позже</p>');
      $('#js_system-notification__container').data('_component').show();
      $('#js_system-notification__container').data('_component').hideOnTimeout(3000);
    }
});
})


/*tracking*/
$(function(){
  Bootstrapper.ngw.ngw_environment = {
    ApplicationID: 'LocalRUDWA',
    Language: 'ru',
    Country: 'RU'
  };
  Bootstrapper.ngw.ngw_user = {
      BrowserResolutionHeight: window.innerHeight || document.body.clientHeight,
      BrowserResolutionWidth: window.innerWidth || document.body.clientWidth,
      BrowserResolutionBreakpoint: 900,
      BrowserResolutionOrientation: window.innerHeight > window.innerWidth ? 'v' : 'h'
  };
  Bootstrapper.ngw.ngw_eventInfo = {
      eventAction: 'VWBasic_Form_Pageload',
      pageId: window.location.href,
      pageName: 'DWA',
      contentTags: ''
  };
  Bootstrapper.ensEvent.trigger('VWBasic_Form_Pageload');
})



$(document).on('click', '.js_page-reload', function(){
  location.reload();
})


$(function(){

var myScroll = new IScroll('.js_custom-scroll',{
  scrollbars: true,
  //interactiveScrollbars: true,
  //resizeScrollbars: true,
  fadeScrollbars: true,
  mouseWheel: true
});
})