/**
 * Created by Kodix on 01.02.2017.
 */
import * as util from 'globals/js/utility/core';
import Media from 'globals/js/media.js';
import toRem from 'globals/js/utility/to-rem';
import _map from 'lodash/map';
import _maxBy from 'lodash/maxBy';
import _each from 'lodash/each';
import _debounce from 'lodash/debounce';
import _isArray from 'lodash/isArray';

// buttons templates
import moreButtonTemplate from './Toggle/more.pug';
import menuButtonTemplate from './Toggle/menu.pug';


import $ from 'jquery';


class VWNavItem {
    constructor(item, $sublayer) {
        this.$item = $(item);

        // Parse item
        this.icon = this.$item.find('.hasicon').attr('class').replace('hasicon ', '');
        this.link = this.$item.children().attr('href');
        this.label = this.$item.find('.vw_m502_link_label').text();

        this.$sublayerItem = $(require('./SublayerItem/index.pug')({
            link: this.link,
            icon: this.icon,
            label: this.label,
        }));

        $sublayer.append(this.$sublayerItem);

        this.mainVisible = true;
    }


    flow() {
        if (!this.mainVisible) {
            return;
        }
        this.$item.addClass('js_vw_m502_nav_item_flow');
        this.$sublayerItem.removeClass('js_vw_m502_nav_item_flow');
        this.mainVisible = false;
    }

    reset() {
        if (this.mainVisible) {
            return;
        }
        this.$item.removeClass('js_vw_m502_nav_item_flow');
        this.$sublayerItem.addClass('js_vw_m502_nav_item_flow');
        this.mainVisible = true;
    }
}

/**
 * @this VWNav
 */
function collectItems() {
    return _map(this.$main.children('ul').children().slice(1), (item) =>
        new VWNavItem($(item).children()[0], this.$sublayer.children('.js_m520_sublayer_sections')));
}

export default class VWNav {

    constructor($mainSection, $sublayerSection) {
        const self = this;

        this.$main = $mainSection;
        this.$sublayer = $sublayerSection;

        this.button_templates = {
            more: moreButtonTemplate(),
            menu: menuButtonTemplate(),
        };

        this.$moreButton = $('<li/>').html(this.button_templates.more);
        this.NavItems = collectItems.call(this);
        if (process.env.NODE_ENV === 'development') {
            util.assert(_isArray(this.NavItems));
            // console.log(this.NavItems);
        }


        // Assign events
        if (this.NavItems.length > 5) {
            this.$main.children('ul').append(this.$moreButton);
            this.$moreButton.on('click', function(e) {
                self.openSublayer(e);
            });
            for (let i = 5; i < this.NavItems.length; i++) {
                this.NavItems[i].flow();
            }
        }

        this.$mobileMoreButton = this.$main.find('.js_m502_mob_menu_switcher');
        this.$mobileMoreButton.on('click', function(e) {
            self.openSublayer(e);
        });

        // Set nav same size
        $(document).ready(() => {
            function navCalc() {
                self.setNavItemHeight();
                self.logoHeight = self.$main.children('ul').children().slice(0, 1).outerHeight();

                self.flowItems();
                if (process.env.NODE_ENV === 'development') {
                    // console.log(self.logoHeight);
                }

                window.addEventListener('resize', _debounce(() => self.flowItems(), 200));
            }

            if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
                setTimeout(() => {
                    navCalc();
                }, 1);
            } else {
                navCalc();
            }
        });

        $(document).ready(() => self.flowItems());
    }

    openSublayer(event) {
        event.preventDefault();
        // console.log('open sublayer');
        this.$mobileMoreButton.toggle();
        this.$sublayer.toggleClass('open');
        if (Media.isMobile) {
            $('body').toggleClass('scroll-lock');
        }
    }

    setNavItemHeight() {
        const self = this;

        if (this.NavItems.length === 0) {
            return;
        }
        this.itemHeight = $(_maxBy(this.NavItems, (item) => item.$item.outerHeight()).$item).outerHeight();


        _each(this.NavItems, (item) => item.$item.css('height', `${toRem(self.itemHeight)}rem`));
    }

    flowItems() {
        if ($(window).width() < Media.breakpoints.tablet) {
            return;
        }

        let mainSectionHeight = this.$main.outerHeight() - this.logoHeight;

        console.log(mainSectionHeight, 'mainSectionHeight');
        this.reset();
        for (let arrLength = this.NavItems.length, i = arrLength > 5 ? 5 : arrLength - 1;
             i > -1 && (i + (arrLength > 5 ? 2 : 1)) * this.itemHeight > mainSectionHeight;
             i--) {
            this.NavItems[i].flow();
            if (i === 0) {
                // console.log(i, this.$moreButton.find('.hasicon'));

                this.$moreButton.html(this.button_templates.menu);
            }
        }
    }

    reset() {
        for (let i = 0, arrLength = this.NavItems.length; i < (arrLength > 5 ? 5 : arrLength); i++) {
            this.NavItems[i].reset();
        }
        this.$moreButton.html(this.button_templates.more);
    }
}
