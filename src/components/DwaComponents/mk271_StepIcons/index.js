/**
 * Created by ufs on 3/17/2017.
 */
import Component, {findComponent} from '../../../globals/js/Libraries/component';
import Page from '../../../globals/js/Libraries/page';
import _throttle from 'lodash/throttle';
// Import styles
import './index.scss';

export default class ToggleFilter extends Component {
    constructor(el) {
        super(el);
        this.$el = $(el);
        this.$tabs_heads = this.$el.find('.mk271__item');
        this.$tabs_bodies = this.$el.find('.mk271__tab');
        this.color = false;
        this.initComponent();
        this.init();
    }

    init() {
        this.$tabs_heads.each((i, tab) => {
            $(tab).click(
                (e) => {
                    const toggle = _throttle(this.toggleTab.bind(this), 200);
                    toggle(i, tab);
                });
        });

    }

    toggleTab(i, tab) {
        $('.mk271__item').removeClass('mk271__item_active');
        $('.mk271__tab').removeClass('mk271__tab_active');
        $tab = $(tab).closest('.mk271__item');
        $tab.toggleClass('mk271__item_active');
        this.$tabs_bodies.eq(i).toggleClass('mk271__tab_active');
/*
        $('.mk271__tab').find('.select__field').removeClass('required');
        this.$tabs_bodies.eq(i).find('.select__field').eq(0).addClass('required');
*/
        this.onlyEnableActiveTabInputs()
        this.$el.closest('.mk615').data('_component').addValidation();

    }

    onlyEnableActiveTabInputs(){
        this.$tabs_heads.find(':input').prop('disabled', true);
        this.$tabs_heads.filter('.mk271__item_active').find(':input').prop('disabled',false);

        this.$tabs_bodies.find(':input').prop('disabled', true);
        this.$tabs_bodies.filter('.mk271__tab_active').find(':input').prop('disabled', false);

        this.$tabs_bodies.find('.select__field').removeClass('required');
        this.$tabs_bodies.filter('.mk271__tab_active').find('.select__field').eq(0).addClass('required');
    }
}

findComponent('.mk271', ToggleFilter);
