import Component, {findComponent} from '../../../globals/js/Libraries/component';
import Page from '../../../globals/js/Libraries/page';
import '../../../components/NavigationModules/mk571_StepList/index';
import {FormValidator, addValidationOnChange} from '../../../globals/js/utility/formValidator';
import $ from 'jquery';
// import _isNull from 'lodash/isNull';
import _map from 'lodash/map';
import _each from 'lodash/each';
import './index.scss';

export default class StepForm extends Component {
  constructor(el) {
    super(el);
    this.color = false;
    this.el = el;
    this.$el = $(el);
    this.$steps = this.$el.find('.mk571');
    this.$tabs = this.$el.find('.mk571_stepList_content-item');
    this.$carla = this.$el.find('.mk572'); // Bottom sticky menu
    this.$checkTab = this.$el.find('.mk615__result-tab');
    this.$editIcons = this.$checkTab.find('.mk615__result-item__icon');
    this.steps_mod = null;
    this.tabForms = _map(this.$tabs, (i)=> new FormValidator(i));
    this.lastStepBtnTxt = 'Отправить';
    this.lastStepBtnClass = 'js_form-submit';
    this.BtnTxt = 'Продолжить';
    this.carla_mod = null;
    this.canProceed = false;
    this.initComponent();
    this.assignSubModules();
  }

  assignSubModules() {
    Promise.all([
      Page.getModuleByEl(this.$steps),
      Page.getModuleByEl(this.$carla),
    ]).then((...m)=> {
      this.steps_mod = m[0][0];
      // this.steps_mod = this.$steps.data('_component');
      this.carla_mod = m[0][1];
      this.init();
    });
  }

  init() {
    this.updateNav();
    this.addValidation();
    this.carla_mod.nextFn = () => {
      this.carlaNextController.bind(this)();
    };
    this.carla_mod.prevFn = () => {
      this.goToTab.bind(this)(this.steps_mod.activeTab - 1); // go to prev tab
    };
    this.steps_mod.$tab_heads.each((i, el)=> {
      $(el).click(this.validateTab.bind(this));
    });
    this.$editIcons.each((i, el)=> {
      $(el).click(()=>{
        this.editField.bind(this)(el);
      });
    });
    this.validateTab();
  }

  carlaNextController() {
    if(this.steps_mod.isLastTab()) {
      this.sendForm.bind(this)();
    } else {
      this.goToTab.bind(this)(this.steps_mod.activeTab + 1); // go to next tab
    }
  }


  addValidation() {
    $(':input', this.el).each((i, inp) => {
      addValidationOnChange(inp);
      // this.validateControl.bind(this)(el);
      $(inp).on('change blur keyup', ()=>{
        this.validateTab.bind(this)();
      });
    });
  }

  validateTab() {
    const curTab = this.steps_mod.activeTab;
    this.canProceed = this.tabForms[curTab].isValid();
    this.updateNav();
    this.fillLastTab();
  }

  updateNav() {
    const curStep = this.steps_mod.activeTab;
    // Button 'back'
    if(curStep) {
      this.carla_mod.$prev.removeClass('invisible');
    } else {
      this.carla_mod.$prev.addClass('invisible');
    }
    // Button 'next'
    if(this.canProceed) {
      this.carla_mod.$next.removeClass('disabled');
    } else {
      this.carla_mod.$next.addClass('disabled');
    }

    if(this.steps_mod.isLastTab()) {
      this.carla_mod.setNextText(this.lastStepBtnTxt);
    } else {
      this.carla_mod.setNextText(this.BtnTxt);
    }
  }

  goToTab(i) {
    if(i>this.steps_mod.$tab_heads.length-1) return false;
    this.steps_mod.goToTab(i);
    this.validateTab();
    this.updateNav();
  }

  sendForm() {
    this.validateTab();
    //this.gatherValues(this.$el);
    this.$el.find('form').data('formData', this.gatherValues(this.$el))
    $(document).trigger('formSubmit', this.$el);
    console.info('You can override me. mk615._component.sendForm = yourFunction');
  }

  fillLastTab() {
    if(!this.$checkTab.length) return false;
    const fields = this.$checkTab.find('.mk615__result-item');
    if(!fields.length) return false;
    _each(fields, (el, i)=> {
      const $resField = $(el);
      const $content = $resField.find('.mk615__result-item__text');
      const name = $resField.data('form-control');
      const $el = $(`[name=${name}]`);
      let val = '';
      if(!$el.length) {
        return false;
      }
      if($el.hasClass('select__field-dwa')) {
        // Selects
        val = $el.find('option:selected').text();
      } else if($el.hasClass('mk242__input')) {
        // Special auto cards with radio
        if($el.closest(':checked').length) {
          val = $el.closest(':checked').closest('.mk242').find('.m242_item__title').text();
        }
      } else if(($el.is(':checkbox') || $el.is(':radio'))) {
        // Radio and checkbox
        if($el.closest(':checked').length) {
          val = $el.closest(':checked').closest('label').text();
        }
      } else {
        val = $el.val();
      }
      $content.text(val);
    });
  }

  editField(el) {
    const name = $(el).closest('.mk615__result-item').data('form-control');
    for(let i = 0; i < this.$tabs.length; i++) {
      const input = $(this.$tabs[i]).find(`[name="${name}"]`);
      if(input.length) {
        this.goToTab(i);
        $(`[name="${name}"]`).focus();
        return true;
      }
    }
    return false;
  }

  gatherValues(cont){
    let inputs = cont.find(':input:not(.select__input--cloned):not([disabled])');
    let valObj = {}
    inputs.each(function(i, item){
      let $item = $(item);

      let name = $item.prop('name');
      let val = $item.val();
      if ($item.is('input[type="radio"]') && $item.prop('checked') == false ||
          $item.is('input[type="checkbox"]') && $item.prop('checked') == false
       ){
        return true;
      }
      if(inputs.filter(`[name="${name}"]`).length > 1 && !$item.is('input[type="radio"]')){
        valObj[name] = ( typeof valObj[name] != 'undefined' && valObj[name] instanceof Array ) ? valObj[name]: [];
        if( val != 0){
          valObj[name].push(val)     
        }
      }
      else{
        valObj[name] = val;
      }
    })
    //console.log(valObj)
    return valObj;
  }
}
import './index.scss';

findComponent('.mk615', StepForm);
