import React from 'react';
import {connect} from 'react-redux';
import * as actions from '../../../actions';
import DealerInfoCard from '../DealerInfoCard';
import RequestList from '../RequestList';
import OfferBlockStandalone from '../OfferBlock__standalone';
import SelectDealerTiles from '../SelectDealerTiles';
import Button from '../Button';

const mapActionsToProps = (dispatch) => {
    return {
        requestsFetchData: (data) => {
            dispatch(actions.requestsFetchData(data));
        },
        changeDealer: (id) => {
            dispatch(actions.changeDealer(id))
        }
    };
}

const mapStateToProps = (state) => {
    return {
        currentSelectedDealerId: state.Main.currentSelectedDealerId,
        requestList: state.RequestList,
        dealerList: state.DealerList,
    };
}

@connect(mapStateToProps, mapActionsToProps)
class App extends React.Component {

    constructor(props) {
        super(props);

    }

    componentDidMount() {
        this.props.requestsFetchData('http://ugrumiynas.synology.me/land_srv/news.php');

    }

    render() {
        const props = this.props;

        let content;

        if (props.requestList == null) {
            content = <div className='page-section pd-default bg-grey'>
                <div className='kdx_container-fluid'>
                    <div className="gridcontainer midpage-message fade-in">
                        <div className="grid_12 txt_center">
                            <div className="midpage-message__title">Заявки отсутствуют</div>
                            <div className="midpage-message__text">Возможно ваша заявка находится на стадии обработки,
                                если вы продолжаете видеть это сообщение в течении суток с момента подачи заявки,
                                свяжитесь с технической поддержкой.
                            </div>
                            <Button
                                className="midpage-message__btn btn_cta"
                                tagName="a"
                                href="#"
                                text="Написать в тех. поддержку"/>
                        </div>
                    </div>
                </div>
            </div>
        }
        else {
            if (props.currentSelectedDealerId != null) {
                content = <div className='page-section pd-default bg-grey'>
                    <div className='kdx_container-fluid'>
                        <div className='gridcontainer two-col-flex-stretch-desktop'>
                            <div className='grid_12 grid_l_3 pd-default only-bottom-pd'>
                                <DealerInfoCard />
                            </div>
                            <div className='grid_12 grid_l_9'>
                                <OfferBlockStandalone
                                    withHeadline
                                    dealerId={this.props.currentSelectedDealerId}/>
                                <RequestList dealerId={this.props.currentSelectedDealerId}/>
                            </div>
                        </div>
                    </div>
                </div>
                ;
            }
            else {
                content = <div className="no-left-col-mobile">
                    <div className='page-section pd-default bg-grey'>
                        <div className='kdx_container-fluid'>
                            <div className='gridcontainer two-col-flex-stretch-desktop'>
                                <div className='grid_12 grid_l_3 pd-default only-bottom-pd'>
                                    <div className="h3">Дилерские центры</div>
                                    <SelectDealerTiles/>
                                </div>
                                <div className='grid_12 grid_l_9'>
                                    <RequestList lastNum="3"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="page-section bg-grey pd-default">
                        <div className="kdx_container-fluid">
                            <div className="gridcontainer">
                                <div className="grid_12">
                                    <OfferBlockStandalone lastNum='3'/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>;
            }
        }

        return (
            <div>
                {content}
            </div>
        );
    }
}

export default App;

