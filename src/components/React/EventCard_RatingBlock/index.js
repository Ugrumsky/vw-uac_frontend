import React from 'react';
import Button from '../Button/index';
import RadioBtn from '../RadioBtn/index';

import EventCardForm from '../EventCard_Form/index';
import EventCardSimpleText from '../EventCard_SimpleText/index';
import EventCardFlexContainer from '../EventCard_FlexContainer/index';
import EventCardGiveRating from '../EventCard_giveRating/index';
import EventCardTextArea from '../EventCard_TextArea/index';

class EventCardRatingBlock extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    btnClickHandler = (data) => {
        this.props.btnClickHandler(data);
    };

    render() {

        const requestData = this.props.requestData;
        const stepStatus = requestData.requestInfo.requestStepStatus;
        const rating = this.props.currentRating;
        const isActive = this.props.isActive;


        let resolveBtnClass = 'btn_cta';
        if (!rating) resolveBtnClass += ' disabled';
        let resolveBtnText = rating < 4 ? 'Отправить' : 'Завершить';

        let btn =  stepStatus =='unresolved' ? <Button className={resolveBtnClass}
                                     tagName='div'
                                     text={resolveBtnText}
                                     onClick={() => {
                                         this.btnClickHandler({
                                             dealerId: requestData.requestOwnDealer,
                                             requestId: requestData.requestId,
                                             rating: this.props.currentRating
                                         });
                                     }}/> : null;

        let textArea = rating != false && rating < 4 && stepStatus != 'success'?
            <EventCardTextArea title="Что-то не устроило? Пожалуйста, оставьте комментарий."
                               placeholder="Введите текст сообщения"/>:
        null;


        const ratingBlock =
            <div>
                <EventCardGiveRating
                    currentRating={rating}
                    stepStatus ={stepStatus}
                    maxRating='5'
                    isActive={isActive}
                    onClick={(index) => {
                        this.props.onClick(index)
                    }}
                />
                <EventCardFlexContainer>
                    {textArea}
                    {btn}
                </EventCardFlexContainer>
            </div>;


        return (
            <div>{ratingBlock}</div>
        );
    }
}


export default EventCardRatingBlock;


