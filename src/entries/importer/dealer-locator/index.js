import 'globals/globals';
import './index.scss';
import _ from 'lodash';

require('js-marker-clusterer');

import makeCitySelect from './citySelect.js';
//import * as dealerCard from '../../../components/UacComponents/dealerLocator-dealerCard/index.js';
/*
import {dealerData} from '../../../globals/mocks/dealerMocks.js';
window.dealerData = dealerData;
*/
// HMR setup
if (module.hot) {
  module.hot.accept();
}

window.citiesList = [];
window.competencesList = {}
window.dealersArr = [];
window.dealersVisible = [];
window.markersAll = []
window.filterValues = {
  "cityId": "all",
  "competences": []
}
window.markerCluster;



$(function(){
     $.each(dealerData, function(cityId, data){
        if (cityId != ''){
            citiesList.push({name:data.CITY.NAME,id:data.CITY.ID})
            $.each(data.DEALERS, function(i, dealer){   
                dealer.cityId = cityId;    
                dealersArr.push(dealer)
                $.each(dealer['COMPETENCE']['VALUE_ENUM_ID'], function(i,val){
                    competencesList[val] = dealer['COMPETENCE']['VALUE'][i]
                })
             });
        }
    });
    makeCitySelect(citiesList)
    makeCompetenceSelect(competencesList)
    selectInit.defaultSelectElementInit($('#dealerCitiesSelect')[0])
    filterDealersByCity(dealersArr, filterValues['cityId'])
    filterDealersByCompetnece(dealersArr, filterValues['competences'])
    makeDealerCards(dealersArr)
    initMap();
    setMapForCity(filterValues['cityId']);

})

/*************GOOGLE***************/

map = false;
markers = {};

export function markerClickHandler(dealer){
    let card = $('#dealerid_' + dealer['ID'])
    scrollToCard(card);

}

export function setMarker(lat, lng, title, compOK, map){

    if(typeof(lat) == 'undefined'){
        lat = 55.714916;
    }
    if(typeof(lng) == 'undefined'){
        lng = 37.718094;
    }
    if(typeof(title) == 'undefined'){
        title = '';
    }
    if(typeof(map) == 'undefined'){
        return false;
    }
    let iconUrl = compOK ? 'data:image/svg+xml;base64,PHN2ZyBpZD0iTGF5ZXJfMSIgZGF0YS1uYW1lPSJMYXllciAxIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAzMyA0NCI+PGRlZnM+PHN0eWxlPi5jbHMtMXtmaWxsOiMwMDk5ZGE7c3Ryb2tlOiNmZmY7c3Ryb2tlLXdpZHRoOjEuNXB4O30uY2xzLTJ7ZmlsbDojZmZmO308L3N0eWxlPjwvZGVmcz48dGl0bGU+QXJ0Ym9hcmQgMTwvdGl0bGU+PHBhdGggY2xhc3M9ImNscy0xIiBkPSJNMzEuOCwxNi40QTE1LjQsMTUuNCwwLDAsMCwxLDE2LjRDMSwzMC40LDE2LjQsNDMsMTYuNCw0M1MzMS44LDMwLjQsMzEuOCwxNi40Ii8+PGNpcmNsZSBjbGFzcz0iY2xzLTIiIGN4PSIxNi40IiBjeT0iMTYuMzgiIHI9IjQuNSIvPjwvc3ZnPg==':'data:image/svg+xml;base64,PHN2ZyBpZD0iTGF5ZXJfMSIgZGF0YS1uYW1lPSJMYXllciAxIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAzMyA0NCI+PGRlZnM+PHN0eWxlPi5jbHMtMXtmaWxsOiNiZGMzYzY7c3Ryb2tlOiNmZmY7c3Ryb2tlLXdpZHRoOjEuNXB4O30uY2xzLTJ7ZmlsbDojZmZmO308L3N0eWxlPjwvZGVmcz48dGl0bGU+QXJ0Ym9hcmQgMTwvdGl0bGU+PHBhdGggY2xhc3M9ImNscy0xIiBkPSJNMzEuOCwxNi40QTE1LjQsMTUuNCwwLDAsMCwxLDE2LjRDMSwzMC40LDE2LjQsNDMsMTYuNCw0M1MzMS44LDMwLjQsMzEuOCwxNi40Ii8+PGNpcmNsZSBjbGFzcz0iY2xzLTIiIGN4PSIxNi40IiBjeT0iMTYuMzgiIHI9IjQuNSIvPjwvc3ZnPg==';
    var marker = new google.maps.Marker({
        position: {lat: parseFloat(lat), lng: parseFloat(lng)},
        //map: map,
        title: title,
        icon: getMarkerIcon(iconUrl),
        optimized: false,
    });
    return marker;
}

function getMarkerIcon(iconUrl){
    return {
        url: iconUrl,
        size: new google.maps.Size(33, 44),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(0, 44),
        scaledSize: new google.maps.Size(33, 44),
    }
}

export function makeDealerMarkers(map){
    if(typeof(map) == 'undefined'){
        return false;
    }
    $.each(dealersArr, function(i, data){
        if(data.cityOK){
            marker = setMarker(data.LATITUDE, data.LONGITUDE, data.NAME, data.compOK, map);
            marker.addListener('click', function() {
                markerClickHandler(data)
            });

            marker.addListener('onCardEnter', function(e) {
                this.setIcon(getMarkerIcon('data:image/svg+xml;base64,PHN2ZyBpZD0iTGF5ZXJfMSIgZGF0YS1uYW1lPSJMYXllciAxIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAzMyA0NCI+PGRlZnM+PHN0eWxlPi5jbHMtMXtmaWxsOiMwMDY4YWE7c3Ryb2tlOiNmZmY7c3Ryb2tlLXdpZHRoOjEuNXB4O30uY2xzLTJ7ZmlsbDojZmZmO308L3N0eWxlPjwvZGVmcz48dGl0bGU+QXJ0Ym9hcmQgMTwvdGl0bGU+PHBhdGggY2xhc3M9ImNscy0xIiBkPSJNMzEuOCwxNi40QTE1LjQsMTUuNCwwLDAsMCwxLDE2LjRDMSwzMC40LDE2LjQsNDMsMTYuNCw0M1MzMS44LDMwLjQsMzEuOCwxNi40Ii8+PGNpcmNsZSBjbGFzcz0iY2xzLTIiIGN4PSIxNi40IiBjeT0iMTYuMzgiIHI9IjQuNSIvPjwvc3ZnPg=='));
            });
            marker.addListener('onCardLeave', function() {
                icon = this.compOK ? 'data:image/svg+xml;base64,PHN2ZyBpZD0iTGF5ZXJfMSIgZGF0YS1uYW1lPSJMYXllciAxIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAzMyA0NCI+PGRlZnM+PHN0eWxlPi5jbHMtMXtmaWxsOiMwMDk5ZGE7c3Ryb2tlOiNmZmY7c3Ryb2tlLXdpZHRoOjEuNXB4O30uY2xzLTJ7ZmlsbDojZmZmO308L3N0eWxlPjwvZGVmcz48dGl0bGU+QXJ0Ym9hcmQgMTwvdGl0bGU+PHBhdGggY2xhc3M9ImNscy0xIiBkPSJNMzEuOCwxNi40QTE1LjQsMTUuNCwwLDAsMCwxLDE2LjRDMSwzMC40LDE2LjQsNDMsMTYuNCw0M1MzMS44LDMwLjQsMzEuOCwxNi40Ii8+PGNpcmNsZSBjbGFzcz0iY2xzLTIiIGN4PSIxNi40IiBjeT0iMTYuMzgiIHI9IjQuNSIvPjwvc3ZnPg==':'data:image/svg+xml;base64,PHN2ZyBpZD0iTGF5ZXJfMSIgZGF0YS1uYW1lPSJMYXllciAxIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAzMyA0NCI+PGRlZnM+PHN0eWxlPi5jbHMtMXtmaWxsOiNiZGMzYzY7c3Ryb2tlOiNmZmY7c3Ryb2tlLXdpZHRoOjEuNXB4O30uY2xzLTJ7ZmlsbDojZmZmO308L3N0eWxlPjwvZGVmcz48dGl0bGU+QXJ0Ym9hcmQgMTwvdGl0bGU+PHBhdGggY2xhc3M9ImNscy0xIiBkPSJNMzEuOCwxNi40QTE1LjQsMTUuNCwwLDAsMCwxLDE2LjRDMSwzMC40LDE2LjQsNDMsMTYuNCw0M1MzMS44LDMwLjQsMzEuOCwxNi40Ii8+PGNpcmNsZSBjbGFzcz0iY2xzLTIiIGN4PSIxNi40IiBjeT0iMTYuMzgiIHI9IjQuNSIvPjwvc3ZnPg=='
                this.setIcon(getMarkerIcon(icon));
            });
            marker.dealerId = data.XML_ID;
            marker.cityId = data.cityId;
            marker.compOK =  data.compOK
            markersAll.push(marker);
        }
    
    });
}

export function getBoundsFromMarkers(mrks) {
    if(typeof(mrks) == 'undefined'){
        mrks = [];
    }
    var bound = new google.maps.LatLngBounds();
    $.each(mrks, function(i, marker){
        bound.extend(marker.getPosition());
    });
    return bound;
}

export function setMapForCity(cityId){
    let dealers = [];
    if (cityId == 'all'){
        dealers = markersAll;
    }
    else{
        $.each(markersAll,function(i, item){
            if (item.cityId == cityId){
                dealers.push(item);
            }
        })
    }
    if(typeof(dealers) == 'undefined'){
        return false;
    }
    var bounds = getBoundsFromMarkers(dealers);
    map.fitBounds(bounds);
}

function initMap() {
    map = new google.maps.Map(document.getElementById('js_dealer-locator__map'), {
        center: {lat: 55.714916, lng: 37.718094},
        zoom: 10,
        maxZoom: 16,
        scrollwheel: false,
    });
    makeDealerMarkers(map);
    markerCluster = new MarkerClusterer(map, markersAll, {
        styles:[
                {   
                    url: 'data:image/svg+xml;base64,PHN2ZyBpZD0iTGF5ZXJfMSIgZGF0YS1uYW1lPSJMYXllciAxIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAzMyA0NCI+PGRlZnM+PHN0eWxlPi5jbHMtMXtmaWxsOiMwMDk5ZGE7c3Ryb2tlOiNmZmY7c3Ryb2tlLXdpZHRoOjEuNXB4O30uY2xzLTJ7ZmlsbDojZmZmO308L3N0eWxlPjwvZGVmcz48dGl0bGU+QXJ0Ym9hcmQgMTwvdGl0bGU+PHBhdGggY2xhc3M9ImNscy0xIiBkPSJNMzEuOCwxNi40QTE1LjQsMTUuNCwwLDAsMCwxLDE2LjRDMSwzMC40LDE2LjQsNDMsMTYuNCw0M1MzMS44LDMwLjQsMzEuOCwxNi40Ii8+PGNpcmNsZSBjbGFzcz0iY2xzLTIiIGN4PSIxNi40MiIgY3k9IjE2LjM3IiByPSI5LjgiLz48L3N2Zz4=',
                    height: 44,
                    width: 32,
                    anchor: [-10, 0],
                    textColor: '#000',
                    textSize: 11,
                    backgroundPosition: '0 0',
                    iconAnchor: [16,44]
                  }
                ]
    });
    map.addListener('idle', function() {
        markerCluster.redraw();
        updateClusters()
    });

}



/*citySelect*/






$(document).on('click', '#js_dealer-locator__map-toggler', function(){
  $(this).toggleClass('is_open');
  $('#js_dealer-locator__map').toggleClass('is_open');
  setTimeout(function(){ /*doesnt draw when hidden*/
      google.maps.event.trigger(map, 'resize');
      setMapForCity(filterValues['cityId']);
    },500)
})






export function scrollToCard(card){
    $('.js_dealer-card').removeClass('is_open');
    let cardTop = card.position().top;
    $('#js_dealer-cards-block').stop().animate({scrollTop: cardTop}, 300, function(){
        if(!card.hasClass('is_open')){
            card.addClass('is_open');
        }
    });
}



$(document).on('click', '.js_dealer-card__fold-toggler', function(e){
    togleCardOpenState(e.target)
})


$(document).on('click', '.js_dealer-card__name', function(e){
    togleCardOpenState(e.target)
})

function togleCardOpenState(target){
    let parentCard = $(target).closest('.js_dealer-card');
    let position =  {lat: parseFloat(parentCard.data('lat')), lng: parseFloat(parentCard.data('lng'))}
    if(parentCard.hasClass('is_open')){
        parentCard.removeClass('is_open')
    }
    else{
        $('.js_dealer-card').removeClass('is_open');
        parentCard.addClass('is_open')
        map.panTo(position);
        map.setZoom(14);
    }
}

/*
$(document).on('click', '.js_dealer-card__fold-toggler', function(){
    let parentCard = $(this).closest('.js_dealer-card');
    if(parentCard.hasClass('is_open')){
        parentCard.removeClass('is_open')
    }
    else{
        parentCard.addClass('is_open')
    }
})*/


$(document).on('mouseenter', '.js_dealer-card', function(){
    let ind = $(this).data('index')
    google.maps.event.trigger(markersAll[ind], 'onCardEnter');
})

$(document).on('mouseleave', '.js_dealer-card', function(){

    let ind = $(this).data('index')
    google.maps.event.trigger(markersAll[ind], 'onCardLeave');
})


/*NEW*/


function makeCompetenceSelect(obj){
  let options = ``;
  for (key in obj){
    options = options + `             
     <li>
      <label class="checkbox">
        <input class="checkbox__control" type="checkbox" name="${key}">
        <div class="checkbox__mask hasicon icon-competence-${key}"></div><span class="checkbox__title">${obj[key]}</span><span class="checkbox__help-block error-container"></span>
      </label>
    </li>`;
  }

  let competenceSelectTmp = `
    <div class="custom-dropdown-container multiple dealersCompetenceSelect js_error_container" id="js_dealersCompetenceSelect">
      <div class="custom-dropdown"><a class="custom-dropdown__input js_select_drop js_check_select_result">
      <input type="hidden" class="js_select_drop_vals">
          <div class="custom-dropdown__result-wrap js_result_wrap placeholder" data-placeholder="Выберите услуги">Выберите услуги</div></a>
        <div class="custom-dropdown__drop js_select_drop_item js_check_select">
        <div class="competence-blocker" id= "competence-blocker"></div>
          <div class="custom-dropdown__drop-wrap">
            <ul class="ul_simple">
             ${options}
            </ul>
          </div>
        </div>
      </div><span class="select_wrapper__help-block error-container">Поле не заполнено</span>
    </div>
  `
  if($('#js_dealersCompetenceSelect').length == 0){
    $('#js_dealer-locator__filter-block').append(competenceSelectTmp);
  }
} 




function makeDealerCard(i, item){
    function renderTemplate (i, item){
        let url = item['URL'] == '' ? ``:`<a class="vwd5-textlink dealer-card__link" href="${item['URL']}" target="_blank"><span class="icon icon-link-website vwd5-textlink__icon"></span>Перейти на сайт дилера</a>`;
        let dealerLocatorCardTemplate =`
            <div class="dealer-card js_dealer-card" id='${"dealerid_" + item['ID']}' m>
                <div class="dealer-card__fold-toggler hasicon icon-arrow-down js_dealer-card__fold-toggler"></div>
                <div class="dealer-card__above-the-fold">
                    <div class="dealer-card__name js_dealer-card__name">${item['NAME']}</div>
                    <div class="dealer-card__address">${item['CITY']}, ${item['ADDRESS']}</div>
                </div>
                <div class="dealer-card__below-the-fold js_dealer-card__below-the-fold">
                    <a class="dealer-card__phone" href="tel:${parsePhone(item['PHONE'][0])}">${item['PHONE'][0]}</a>
                    <div class="dealer-card__hours">Ежедневно с 08:00 до 21:00</div>
                    <a class="vwd5-textlink dealer-card__link" href="${item['DETAIL_PAGE']}"><span class="icon icon-link-view vwd5-textlink__icon"></span>Подробнее о дилере</a>
                    ${url}
                </div>
            </div>
        `
        return dealerLocatorCardTemplate
    }

    let $card = $(jQuery.parseHTML(renderTemplate(i, item)));
    $card.data({'dealerID': item['XML_ID'],
                'city': item['cityId'],
                'lat': item['LATITUDE'],
                'lng': item['LONGITUDE'],
                'index': i})
    if (item.compOK ==false) {
        $card.addClass('is_hidden');
    }
    return $card;
 }

 function makeDealerCards(arr){
    dealersVisible = [];
    $('#js_dealer-cards-block--inner').empty();
    $.each(arr,function(i, item){
        if (item.cityOK){
            dealersVisible.push(item)
        }
    })

     $.each(dealersVisible,function(i, item){
        $('#js_dealer-cards-block--inner').append(makeDealerCard(i, item))
    
    })
 }

function updateDealerCards(){
     filterDealersByCity(dealersArr, filterValues['cityId'])
     filterDealersByCompetnece(dealersArr, filterValues['competences'])
     //filterDealersByBounds(dealersArr) // пока убираем

     makeDealerCards(dealersArr)
}

function updateDealerMarkers(){
    markerCluster.clearMarkers();
    markersAll = [];
    makeDealerMarkers(map);
    markerCluster.addMarkers(markersAll)
    updateClusters()
}

function updateClusters(){
    setTimeout(function(){
    $.each(markerCluster.clusters_,function(i, cluster){
       let counter = 0;
        $.each(cluster.markers_, function(i,item){
            if (!item.compOK){
                counter++;
            }
        })
        if( counter == cluster.markers_.length ){
           cluster.clusterIcon_.div_.className='cluster-disabled'


        }
        else{
           cluster.clusterIcon_.div_.className='cluster'
        }
    })
    },200)
}

 function updateFilterValues(){

    filterValues['cityId'] = $('#dealerCitiesSelect').val();
    filterValues['competences'] = $('#js_dealersCompetenceSelect .js_select_drop_vals').data('value');
 }

function filterDealersByCity(arr, cityId){
    if (cityId !='all'){
        $.each(arr, function(i, item){
            if (item.cityId != cityId){
                item.cityOK = false;
            }
            else{
                item.cityOK = true;
            }
        })
    }
    else{
            $.each(arr, function(i, item){
                item.cityOK = true;
            
        })
    }
}
function filterDealersByCompetnece(arr, competenceArr){
    for (let i = 0; i < arr.length; i++){
        let item = arr[i];
        if (_.difference(competenceArr, item['COMPETENCE']['VALUE_ENUM_ID']).length == 0){
            item.compOK = true;
        }
    
        else{
            item.compOK = false;
        }
    }
}
/*
function filterDealersByBounds(arr){
    $.each(arr, function(i, item){
        let itemLatLng = new google.maps.LatLng(item.LATITUDE,item.LONGITUDE);
        if (map.getBounds().contains(itemLatLng)){
            return(item.XML_ID)
        }
    })
}
*/

 $(document).on('filterUpdate', function(){
    updateFilterValues();
    updateDealerCards();
    updateDealerMarkers();

 })



 $(document).on('change','#js_dealersCompetenceSelect .js_select_drop_vals', function(){
        $(document).trigger('filterUpdate');

})

 $(document).on('change','#dealerCitiesSelect',function(){
    $(document).trigger('filterUpdate');
    setMapForCity($(this).val());
})


function parsePhone(target){
    return target.toString().replace(/-/g, '').replace(/ /g, '').replace(/\(/g,"").replace(/\)/g,"")
}


/*RUSV00243*/