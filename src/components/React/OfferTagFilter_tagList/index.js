import React from 'react';
import Select from 'react-select';

const shortid = require('shortid');
const classNames = require('classnames');


class OfferTagFilterTagList extends React.Component {

    constructor(props) {
        super(props);
    }

    handleOnTagRemoveClick = (value) => {
        this.props.onRemoveClick(value);
    }
    handleOnTagChangeClick = (e) => {
        this.props.onChange(e);
    }

    render() {
        const tags = this.props.availableTags;
        const offerTagsCountObj = this.props.offerTagsCount;


        let content = tags.map((tag, i) => {
            const tagCount = offerTagsCountObj[tag.value];
            const isDisabled = tagCount > 0 ? false : true;

            return (
                <div
                    key = {shortid.generate()}
                    className="tagFilter__tag-item">
                    <label>
                        <input type="checkbox"
                               id={tag.value}
                               value={tag.value}
                               checked = {tag.isChecked}
                               onChange = {(e)=>{this.handleOnTagChangeClick(e)}}
                               disabled={isDisabled}/>
                        <div className="tagFilter__tag-item-presentation">
                            {tag.label} {tagCount}
                        </div>
                    </label>
                    <div className="tagFilter__tag-item-remove hasicon icon-base-close" onClick={() => {
                        this.handleOnTagRemoveClick(tag.value)
                    }}></div>
                </div>

            );
        });

        return (
            <div className="tagFilter__tagList">
                {content}
            </div>
        );
    }
}


export default OfferTagFilterTagList;
