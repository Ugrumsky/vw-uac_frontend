require('perfect-scrollbar/jquery')($);

$(function(){
	$('#top-menu').perfectScrollbar();
	setPosToActive();
})


function setPosToActive (){
	let menu = $('#top-menu');
	let activeItem = menu.find('.top-menu__item.is_active');
	let activeLeft = activeItem.position().left;
	let menuWidth = menu.width();

   	activeLeft=  activeLeft-(menuWidth/2) + (activeItem.width()/2);
	menu[0].scrollLeft =  activeLeft;
}

let timer;

$(window).on('resize', function (){
	clearTimeout(timer);
	timer = setTimeout(function(){
		setPosToActive(); 
	},300)
})