import {fetchData} from './index';


const fetchRequestsDataErrored = (bool) => {
    return {
        type: 'REQUESTS_ERRORED',
        hasErrored: bool
    };
}

const fetchRequestsDataLoading = (bool) => {
    return {
        type: 'REQUESTS_LOADING',
        isLoading: bool
    };
}

const fetchRequestsData = (items) => {
    return {
        type: 'REQUESTS_FETCH_DATA_SUCCESS',
        items
    };
}
export const requestsFetchData = (url, fetchParams) => {
    return fetchData(fetchRequestsData,fetchRequestsDataErrored,fetchRequestsDataLoading, url, fetchParams)
}