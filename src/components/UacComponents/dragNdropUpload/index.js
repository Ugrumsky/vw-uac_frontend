var Dropzone = require("dropzone");

Dropzone.autoDiscover = false;

document.addEventListener('DOMContentLoaded', (event) => {
  initDndUpload();
});

export let initDndUpload = function(){
  let elements = $('.js_dragNdropUpload');

  elements.each(function(){
    if(!$(this).hasClass('inited')){
      $(this).addClass('inited');
      $(this).dropzone({
        autoProcessQueue: false,
        url: $(this).data('url'),
        paramName: $(this).data('paramname'),
        maxFiles: $(this).data('maxfiles'),
        maxFilesize: 10,
        acceptedFiles: $(this).data('acceptedfiles'),
        addRemoveLinks: true,
        thumbnailWidth: null,
        thumbnailHeight: null,
        dictFileTooBig: "Файлы объемом более {{maxFilesize}}Мб не были загружены.",
        dictInvalidFileType: "Файлы данного типа не поддерживаются.",
        dictCancelUpload: "Загрузка отменена",
        dictCancelUploadConfirmation: "Уверены, что хотите отменить загрузку?",
        dictRemoveFile: "Удалить файл",
        dictRemoveFileConfirmation: null,
        dictMaxFilesExceeded: "Достигнут лимит в 6 файлов.",
        init: function() {
          /*this.on("maxfilesexceeded", function(file) {
            this.removeFile(file);
            //this.addFile(file);
          });*/
          this.on("maxfilesreached", function(file) {
            $(this.element).parent().addClass('show-error ');
            $(this.element).parent().find('.js_addFileDnDUpload').addClass('disabled');
          });
          this.on("removedfile", function(file) {
            if (this.files.length < this.options.maxFiles){
              $(this.element).parent().removeClass('show-error');
              $(this.element).parent().find('.js_addFileDnDUpload').removeClass('disabled');
            }
          });
          this.on("error", function(file, message){
            if (!file.accepted){
              this.removeFile(file);
              let newMessage = `<div>${message}</div>`
              let messageContainer = $(this.element).parent().find('.js_dragNdropUpload__error-message');
              messageContainer.empty();
              messageContainer.html(messageContainer.html() + newMessage)
              $(this.element).parent().addClass('show-error');
            } 
          });
        } 
      });
    }
  })
} 


$(document).on('click','.js_addFileDnDUpload',function(e) {
  e.stopPropagation();
  e.stopImmediatePropagation();
  $(this).parent().find('.js_dragNdropUpload').trigger('click');
})