import $ from 'jquery';


//accordion
$(function() {
  $(document).on('click', '.js_accor_toggler', function() {

  	if($(this).hasClass('active')){
  		$(this).removeClass('active');
  		$(this).parent().find(' > .js_accor_content').slideUp(300);
  	}
  	    else if( $(this).hasClass('car-features__accor-title-lvl1')){
    	  	if($(this).hasClass('active')){
		  		$(this).removeClass('active');
		  		$(this).parent().find(' > .js_accor_content').slideUp(300);
		  	}
	  	    else{
		    	$('.js_accor_toggler.car-features__accor-title-lvl1').removeClass('active');
		    	$('.js_accor_content.car-features__accor-content-lvl1').slideUp(300);
		  		$(this).addClass('active');
		  		$(this).parent().find(' > .js_accor_content').slideDown(300);
		    }
    }
    else{
    	$('.js_accor_toggler:not(.car-features__accor-title-lvl1)').removeClass('active');
    	$('.js_accor_content:not(.car-features__accor-content-lvl1)').slideUp(300);
  		$(this).addClass('active');
  		$(this).parent().find(' > .js_accor_content').slideDown(300);
    }
  });
})



