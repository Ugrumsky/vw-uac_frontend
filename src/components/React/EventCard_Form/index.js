import React from 'react';

const EventCardForm = (props) => {
    return (
        <div className="eventCard__form">
            {props.children}
        </div>
    );
};


export default EventCardForm;
