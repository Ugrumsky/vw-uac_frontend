/**
 * Created by ufs on 3/17/2017.
 */
// Import styles
import './index.scss';
import wrapper from './index.pug';

class Disclaimer {
  constructor(element, controller) {
    this.element = element;
    this.content = element.html();
    this.wrapper = $(wrapper({
      data: this.content,
    }));
    this.controller = controller;
    this.init();
  }

  init() {
    const self = this;
    this.element.parent().click(function(e) {
      e.preventDefault();
      if (self.controller.opened === false) {
        $('body').append(self.wrapper);
        self.controller.opened = true;
        self.wrapper.find('.m644__close').one('click', function() {
          self.wrapper.removeClass('is-opened');
          self.wrapper.one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',
            function() {
              self.wrapper.off('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend');
              self.wrapper.detach();
              self.controller.opened = false;
            });
        });
        setTimeout(() => self.wrapper.addClass('is-opened'), 100);
      }
    });
  }
}

class DisclaimerController {
  constructor() {
    const self = this;
    this.els = $('.m644-content');
    this.disclaimers = this.els.map((i, el) => new Disclaimer($(el), self));
    this.opened = false;
  }
}


$(() => new DisclaimerController());
