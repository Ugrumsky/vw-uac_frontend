import $ from 'jquery';
import _throttle from 'lodash/throttle';
import Component, {findComponent} from '../../../globals/js/Libraries/component';
import Page from '../../../globals/js/Libraries/page';


class DealerIdBlock extends Component {
  constructor(el) {
    super(el);
    let self = this;

    this.container = $(el).find('.mk504__container');
    this.title = this.container.find('.mk504__title');
    this.phones = this.container.find('.mk504__phones');
    this.color = false;

    $(document).ready(() => self.changeTitleWidth());
    $(window).resize(_throttle(() => self.changeTitleWidth(), 50));

    this.initComponent();
    Page.setProp('DealerIdBlock', this);
  }

  changeTitleWidth() {
    let innerWidth = this.container.width();
    let phonesWidth = this.phones.outerWidth();
    this.title.css('width', Math.floor(innerWidth - phonesWidth - 1));
  }
}

findComponent('.mk504', DealerIdBlock);
