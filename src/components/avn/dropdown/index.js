import $ from 'jquery'
import * as Media from 'globals/js/media.js';


//dropdowns
//fake select dropdown
$(document).on('click', '.js_select_drop', function(e) {
  if ($(this).parent().hasClass('is_active')){
    $(this).toggleClass('focused');
    $(this).parent().toggleClass('is_active');
    $(this).parent().find('.js_select_drop_item').toggle();
  }
  else if (!$(this).hasClass('disabled')) {
    $('.js_select_drop_item').hide();
    $('.js_select_drop').removeClass('focused');
    $('.js_select_drop').parent().removeClass('is_active');
    $(this).toggleClass('focused');
    $(this).parent().toggleClass('is_active');
    $(this).parent().find('.js_select_drop_item').toggle();
  }
  
  e.preventDefault();
});

//--hide dropdown on outer click
$(document).on('click', function(e) {
  if (($(e.target).closest('.js_select_drop_item').length == 0) && ($(e.target).closest(".js_select_drop").length == 0)) {
    $('.js_select_drop_item').hide();
    $('.js_select_drop').removeClass('focused');
    $('.js_select_drop').parent().removeClass('is_active');
  }
});

$(document).on('scroll',function(){
      $('.js_select_drop_item').hide();
    $('.js_select_drop').removeClass('focused');
    $('.js_select_drop').parent().removeClass('is_active');
})
//checkboxes select dropdown
$(function() {
  $('.js_check_select').each(function() {
    var dataArr = [];
    $(this).find('input:checked').each(function() {
      var data = $(this).is('[data-title]') ? $(this).data('title') : $(this).val();
      dataArr.push(data);
    });

    var arrStr = dataArr.join(', ');
    var resultWrap = $(this).parent().find('.js_result_wrap');
    if (dataArr.length > 0) {
      resultWrap.html(arrStr).removeClass('placeholder');
    } else {
      resultWrap.html(resultWrap.data('placeholder')).addClass('placeholder');
    }

  })
})


$(document).on('change', '.js_check_select input', function() {
  var data = $(this).val();

  var dataArr = [];
  var allData = $(this).parents('.js_check_select').find('input:checked').each(function() {
    var data = $(this).is('[data-title]') ? $(this).data('title') : $(this).val();
    dataArr.push(data);
  });

  var arrStr = dataArr.join(', ');
  var resultWrap = $(this).parents('.js_check_select').parent().find('.js_result_wrap');
  if (dataArr.length > 0) {
    resultWrap.html(arrStr).removeClass('placeholder');
  } else {
    resultWrap.html(resultWrap.data('placeholder')).addClass('placeholder');
  }
});