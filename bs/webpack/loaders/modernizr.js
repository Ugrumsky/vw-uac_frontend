
module.exports = function (CONFIG) {
  return [
    {
      test: /\.modernizrrc.js$/,
      use: [{
        loader: "modernizr-loader"
      }]
    },
    {
      test: /\.modernizrrc(\.json)?$/,
      use: [{
        loader: "modernizr-loader"
      }, {
        loader: "json-loader"
      }]
    }
  ];
}
