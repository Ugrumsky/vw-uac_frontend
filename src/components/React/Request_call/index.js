import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../../actions/index';

import EventCard from '../EventCard/index';
import EventCardAboveTheFold from '../EventCard_AboveTheFold/index';
import EventCardBelowTheFold from '../EventCard_BelowTheFold/index';
import EventCardBasicInfo from '../EventCard_BasicInfo/index';
import EventCardAdditionalInfo from '../EventCard_AdditionalInfo/index';
import EventCardDecisionIndicator from '../EventCard_DecisionIndicator/index';
import EventCardHistory from '../EventCard_History/index';
import EventCardDecisionBlock from '../EventCard_DecisionBlock/index';
import EventCardForm from '../EventCard_Form/index';


let classNames = require('classnames');


class RequestVisit extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showDeclineCause: false,
            userDecision: 'success',
            declineCause: false
        };
    }

    resolveStep = (data) => {
        this.props.resolveStep(data);
    };

    handleResetDecision = (e) => {
        this.setState({
            userDecision: 'unresolved',
        });

        this.resolveStep({
            dealerId: this.props.requestData.requestOwnDealer,
            requestId: this.props.requestData.requestId,
            userDecision: 'unresolved',
            declineCause: false,
        });
    };

    render() {
        const requestData = this.props.requestData;
        const requestInfo = requestData.requestInfo;
        const requestHistory = requestData.requestHistory;

        let cardTitles ={
            'initial': 'Мы бы хотели связаться с вами по телефону',
            'timeSuggestion': 'Мы бы хотели связаться с вами по телефону'
        }

        let content = null;
        switch (requestInfo.requestStep) {
            case 'initial': {
                content = '123'
                break;
            }
            case 'timeSuggestion': {
                switch (requestInfo.requestStepStatus) {
                    case 'unresolved' : {
                        content =  <Wrap {...requestData} cardTitles = {cardTitles}>
                            <EventCardAdditionalInfo>
                                <EventCardDecisionBlock
                                    radios= {{
                                        name: 'declineCause',
                                        btns:[
                                            {
                                                value:'1',
                                                title:'Нужно перенести дату и время',
                                            },
                                            {
                                                value:'2',
                                                title:'Неверные дата и время',
                                            },
                                            {
                                                value:'3',
                                                title:'В данный момент меня это не интересует',
                                            }
                                        ]
                                    }}
                                    title="Пожалуйста, оставьте комментарий о причине отказа"
                                    dealerId = {this.props.requestData.requestOwnDealer}
                                    requestId = {this.props.requestData.requestId}
                                    resolveStep = {this.resolveStep}
                                />
                            </EventCardAdditionalInfo>
                        </Wrap>;
                        break;
                    }
                    case 'success' : {
                        content =  <Wrap {...requestData} cardTitles = {cardTitles}>
                            <EventCardAdditionalInfo>
                                <EventCardForm>
                                    <EventCardDecisionIndicator
                                        status = {requestInfo.requestStepStatus}
                                        text='Дата и время подтверждены'
                                        onClick={this.handleResetDecision}/>
                                </EventCardForm>
                            </EventCardAdditionalInfo>
                        </Wrap>;
                        break
                    }
                    case 'fail' : {
                        content =  <Wrap {...requestData} cardTitles = {cardTitles}>
                            <EventCardAdditionalInfo>
                                <EventCardForm>
                                    <EventCardDecisionIndicator
                                        status = {requestInfo.requestStepStatus}
                                        text='Вы отказались от звонка'/>
                                </EventCardForm>
                            </EventCardAdditionalInfo>
                        </Wrap>
                        break
                    }
                }
                break;
            }

        }

        return (
            <div>{content}</div>
        );
    }
}

const Wrap = (props) => {
    const cardTitles = props.cardTitles;
    const requestInfo = props.requestInfo;
    const basicInfo = requestInfo.requestBasicInfo;
    const requestHistory = props.requestHistory;
    return (
        <EventCard>
            <EventCardAboveTheFold cardTitle={cardTitles[requestInfo.requestStep]}>
                <EventCardBasicInfo basicInfo = {basicInfo}/>
                {props.children}
            </EventCardAboveTheFold>
            <EventCardBelowTheFold>
                <EventCardHistory historyData = {requestHistory} />
            </EventCardBelowTheFold>
        </EventCard>
    );
};

const mapActionsToProps = (dispatch) => {
    return {
        resolveStep: (data) => {
            dispatch(actions.resolveStep(data));
        },
    };
}

RequestVisit = connect(null, mapActionsToProps)(RequestVisit);

export default RequestVisit;
