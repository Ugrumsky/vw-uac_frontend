import Component, {findComponent} from '../../../globals/js/Libraries/component';
// Import styles
import './index.scss';

class EditorialStage extends Component {
  constructor(el) {
    super(el);
    this.color = false;
    this.initComponent();
  }
}

findComponent('.m310_es', EditorialStage);
