/**
 * Created by ufs on 3/17/2017.
 */
import Component, {findComponent} from '../../../globals/js/Libraries/component';
// Import styles
import './index.scss';

export default class CarlaNav extends Component {
  constructor(el) {
    super(el);
    this.color = false;
    this.$el = $(el);
    this.$prev = this.$el.find('.mk572__unit_back');
    this.$next = this.$el.find('.mk572__unit_forward');
    this.prevFn = () => {
      console.warn('.mk572 prevFn', 'you can reassign me');
    };
    this.nextFn = () => {
      console.warn('.mk572 nextFn', 'you can reassign me');
    };
    this.initComponent();
    this.init();
  }

  init() {
    this.$prev.click(()=> {
      this.prevFn();
    });
    this.$next.click(()=> {
      if(!this.$next.hasClass('disabled')) {
        this.nextFn();
      }
    });
  }

  setNextText(text) {
    $('.btn__text', this.$next).text(text);
  }
}

findComponent('.mk572', CarlaNav);
