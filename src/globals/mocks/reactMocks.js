export let dealerList = {

    'd00001': {
        dealerCoords: {lat: 55.714916, lng: 37.718094},
        dealerName: 'Менеджер',
        dealerAddress: 'Санкт-Петербург, Волгоградский пр-т., д. 41 ',
        dealerPhone: '+7 (492) 399 48 41',
        dealerManagerName: 'Константин Константинопольский',
        dealerManagerPhone: '+7 (492) 399 48 41',
        dealerInfoBtns: [{
            btnText: 'Написать сообщение',
            btnLink: '#',
        }, {
            btnText: 'Заказать звонок',
            btnLink: '#',
        }, {
            btnText: 'Онлайн-звонок',
            btnLink: '#',
        }],

    },
    'd00002': {
        dealerCoords: {lat: 55.714716, lng: 37.728294},
        dealerName: 'Тест-драйв',
        dealerAddress: 'Санкт-Петербург, Волгоградский пр-т., д. 41 ',
        dealerPhone: '+7 (492) 399 48 41',
        dealerManagerName: 'Константин Константинопольский',
        dealerManagerPhone: '+7 (492) 399 48 41',
        dealerInfoBtns: [{
            btnText: 'Написать сообщение',
            btnLink: '#'
        }, {
            btnText: 'Заказать звонок',
            btnLink: '#'
        }, {
            btnText: 'Онлайн-звонок',
            btnLink: '#'
        }],
        /***************rqlist***********************/
    },
    'd00003': {
        dealerCoords: {lat: 55.714716, lng: 37.728294},
        dealerName: 'Звонок',
        dealerAddress: 'Санкт-Петербург, Волгоградский пр-т., д. 41 ',
        dealerPhone: '+7 (492) 399 48 41',
        dealerManagerName: 'Константин Константинопольский',
        dealerManagerPhone: '+7 (492) 399 48 41',
        dealerInfoBtns: [{
            btnText: 'Написать сообщение',
            btnLink: '#'
        }, {
            btnText: 'Заказать звонок',
            btnLink: '#'
        }, {
            btnText: 'Онлайн-звонок',
            btnLink: '#'
        }],
        /***************rqlist***********************/
    },
    'd00004': {
        dealerCoords: {lat: 55.714716, lng: 37.728294},
        dealerName: 'Приглашение',
        dealerAddress: 'Санкт-Петербург, Волгоградский пр-т., д. 41 ',
        dealerPhone: '+7 (492) 399 48 41',
        dealerManagerName: 'Константин Константинопольский',
        dealerManagerPhone: '+7 (492) 399 48 41',
        dealerInfoBtns: [{
            btnText: 'Написать сообщение',
            btnLink: '#'
        }, {
            btnText: 'Заказать звонок',
            btnLink: '#'
        }, {
            btnText: 'Онлайн-звонок',
            btnLink: '#'
        }],
        /***************rqlist***********************/
    },
    'd00005': {
        dealerCoords: {lat: 55.714716, lng: 37.728294},
        dealerName: 'Кредит',
        dealerAddress: 'Санкт-Петербург, Волгоградский пр-т., д. 41 ',
        dealerPhone: '+7 (492) 399 48 41',
        dealerManagerName: 'Константин Константинопольский',
        dealerManagerPhone: '+7 (492) 399 48 41',
        dealerInfoBtns: [{
            btnText: 'Написать сообщение',
            btnLink: '#'
        }, {
            btnText: 'Заказать звонок',
            btnLink: '#'
        }, {
            btnText: 'Онлайн-звонок',
            btnLink: '#'
        }],
        /***************rqlist***********************/
    },
    'd00007': {
        dealerCoords: {lat: 55.714716, lng: 37.728294},
        dealerName: 'Трейд-ин',
        dealerAddress: 'Санкт-Петербург, Волгоградский пр-т., д. 41 ',
        dealerPhone: '+7 (492) 399 48 41',
        dealerManagerName: 'Константин Константинопольский',
        dealerManagerPhone: '+7 (492) 399 48 41',
        dealerInfoBtns: [{
            btnText: 'Написать сообщение',
            btnLink: '#'
        }, {
            btnText: 'Заказать звонок',
            btnLink: '#'
        }, {
            btnText: 'Онлайн-звонок',
            btnLink: '#'
        }],
        /***************rqlist***********************/
    },
    'd00008': {
        dealerCoords: {lat: 55.714716, lng: 37.728294},
        dealerName: 'Предложение',
        dealerAddress: 'Санкт-Петербург, Волгоградский пр-т., д. 41 ',
        dealerPhone: '+7 (492) 399 48 41',
        dealerManagerName: 'Константин Константинопольский',
        dealerManagerPhone: '+7 (492) 399 48 41',
        dealerInfoBtns: [{
            btnText: 'Написать сообщение',
            btnLink: '#'
        }, {
            btnText: 'Заказать звонок',
            btnLink: '#'
        }, {
            btnText: 'Онлайн-звонок',
            btnLink: '#'
        }],
        /***************rqlist***********************/
    },
    'd00009': {
        dealerCoords: {lat: 55.714716, lng: 37.728294},
        dealerName: 'Теги',
        dealerAddress: 'Санкт-Петербург, Волгоградский пр-т., д. 41 ',
        dealerPhone: '+7 (492) 399 48 41',
        dealerManagerName: 'Константин Константинопольский',
        dealerManagerPhone: '+7 (492) 399 48 41',
        dealerInfoBtns: [{
            btnText: 'Написать сообщение',
            btnLink: '#'
        }, {
            btnText: 'Заказать звонок',
            btnLink: '#'
        }, {
            btnText: 'Онлайн-звонок',
            btnLink: '#'
        }],
        /***************rqlist***********************/
    }
}


export let requestList = {
    /*manager*/
    7612345: {
        requestId: 7612345,
        requestOwnDealer: 'd00001',
        requestInfo: {
            requestType: 'manager',
            requestStep: 'managerInfo',
            requestStepStatus: 'unresolved',
            requestDate: 1502468970,
            requestHeadText: 'Дилер такой-то',
            requestManagerInfo: {
                managerAvatar: '/images/profile.jpg',
                managerName: 'Константин Константинопольский',
                managerPhone: '+7 948 000 00 00'
            },
        },
    },
    7612345123123: {
        requestId: 7612345123123,
        requestOwnDealer: 'd00001',
        requestInfo: {
            requestType: 'manager',
            requestStep: 'initial',
            requestStepStatus: 'unresolved',
            requestDate: 1502468970,
            requestHeadText: 'Дилер такой-то',
            requestRating: false,
            requestManagerInfo: {
                managerAvatar: '/images/profile.jpg',
                managerName: 'Константин Константинопольский',
                managerPhone: '+7 948 000 00 00'
            },
        },
    },
    /*test-drive*/
    98761234512: {
        requestId: 98761234512,
        requestOwnDealer: 'd00002',
        requestInfo: {
            requestType: 'testdrive',
            requestStep: 'initial',
            requestStepStatus: 'success',
            requestDate: 1491696000,
            requestHeadText: 'Дилер такой-то',
        },
    },
    987612345: {
        requestId: 987612345,
        requestOwnDealer: 'd00002',
        requestInfo: {
            requestType: 'testdrive',
            requestStep: 'invitation',
            requestStepStatus: 'unresolved',
            requestDate: 1499558410,
            requestHeadText: 'Дилер такой-то',
            requestRating: false,
            requestBasicInfo: [{
                title: 'Дата и время',
                text: '24 сентября, 12:00',
            }, {
                title: 'Автомобиль',
                text: 'Tiguan R-Line Executive',
            }],
        },
        requestHistory: [{
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }],
    },
    9876123451: {
        requestId: 9876123451,
        requestOwnDealer: 'd00002',
        requestInfo: {
            requestType: 'testdrive',
            requestStep: 'invitation',
            requestStepStatus: 'success',
            requestDate: 1499558420,
            requestHeadText: 'Дилер такой-то',
            requestRating: false,
            requestBasicInfo: [{
                title: 'Дата и время',
                text: '24 сентября, 12:00',
            }, {
                title: 'Автомобиль',
                text: 'Tiguan R-Line Executive',
            }],
        },
        requestHistory: [{
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }, {
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }],
    },
    98761234514: {
        requestId: 98761234514,
        requestOwnDealer: 'd00002',
        requestInfo: {
            requestType: 'testdrive',
            requestStep: 'invitation',
            requestStepStatus: 'fail',
            requestDate: 1499558520,
            requestHeadText: 'Дилер такой-то',
            requestRating: false,
            requestBasicInfo: [{
                title: 'Дата и время',
                text: '24 сентября, 12:00',
            }, {
                title: 'Автомобиль',
                text: 'Tiguan R-Line Executive',
            }],
        },
        requestHistory: [{
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }, {
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }],
    },
    987612345134: {
        requestId: 987612345134,
        requestOwnDealer: 'd00002',
        requestInfo: {
            requestType: 'testdrive',
            requestStep: 'passed',
            requestStepStatus: 'unresolved',
            requestDate: 1499558520,
            requestHeadText: 'Дилер такой-то',
            requestRating: false,
            requestBasicInfo: [{
                title: 'Дата и время',
                text: '24 сентября, 12:00',
            }, {
                title: 'Автомобиль',
                text: 'Tiguan R-Line Executive',
            }],
        },
        requestHistory: [{
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }],
    },
    9876123451341: {
        requestId: 9876123451341,
        requestOwnDealer: 'd00002',
        requestInfo: {
            requestType: 'testdrive',
            requestStep: 'passed',
            requestStepStatus: 'success',
            requestDate: 1499558520,
            requestHeadText: 'Дилер такой-то',
            requestRating: 4,
            requestBasicInfo: [{
                title: 'Дата и время',
                text: '24 сентября, 12:00',
            }, {
                title: 'Автомобиль',
                text: 'Tiguan R-Line Executive',
            }],
        },
        requestHistory: [{
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }],
    },
    /*call*/
    1234567: {
        requestId: 1234567,
        requestOwnDealer: 'd00003',
        requestInfo: {
            requestType: 'call',
            requestStep: 'timeSuggestion',
            requestStepStatus: 'unresolved',
            requestDate: 1502236800,
            requestHeadText: 'Дилер',
            requestBasicInfo: [{
                title: 'Дата и время',
                text: '24 сентября, 12:00',
            }]
        },
        requestHistory: [{
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }],
    },
    12345678: {
        requestId: 12345678,
        requestOwnDealer: 'd00003',
        requestInfo: {
            requestType: 'call',
            requestStep: 'timeSuggestion',
            requestStepStatus: 'success',
            requestDate: 1502236800,
            requestHeadText: 'Дилер',
            requestBasicInfo: [{
                title: 'Дата и время',
                text: '24 сентября, 12:00',
            }]
        },
        requestHistory: [{
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }],
    },
    123456789: {
        requestId: 123456789,
        requestOwnDealer: 'd00003',
        requestInfo: {
            requestType: 'call',
            requestStep: 'timeSuggestion',
            requestStepStatus: 'fail',
            requestDate: 1502236800,
            requestHeadText: 'Дилер',
            requestBasicInfo: [{
                title: 'Дата и время',
                text: '24 сентября, 12:00',
            }]
        },
        requestHistory: [{
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }],
    },
    /*visit*/
    6123415: {
        requestId: 6123415,
        requestOwnDealer: 'd00004',
        requestInfo: {
            requestType: 'visit',
            requestStep: 'invitation',
            requestStepStatus: 'unresolved',
            requestDate: 1499558400,
            requestHeadText: 'Какой-то дилер',
            requestBasicInfo: [{
                title: 'Дата и время',
                text: '24 сентября, 12:00',
            }]
        },
        requestHistory: [{
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }],
    },
    61234151: {
        requestId: 61234151,
        requestOwnDealer: 'd00004',
        requestInfo: {
            requestType: 'visit',
            requestStep: 'invitation',
            requestStepStatus: 'success',
            requestDate: 1499558400,
            requestHeadText: 'Какой-то дилер',
            requestBasicInfo: [{
                title: 'Дата и время',
                text: '24 сентября, 12:00',
            }]
        },
        requestHistory: [{
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }],
    },
    61233415: {
        requestId: 61233415,
        requestOwnDealer: 'd00004',
        requestInfo: {
            requestType: 'visit',
            requestStep: 'invitation',
            requestStepStatus: 'fail',
            requestDate: 1499558400,
            requestHeadText: 'Какой-то дилер',
            requestBasicInfo: [{
                title: 'Дата и время',
                text: '24 сентября, 12:00',
            }]
        },
        requestHistory: [{
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }],
    },
    /*credit*/
    87612345: {
        requestId: 87612345,
        requestOwnDealer: 'd00005',
        requestInfo: {
            requestType: 'credit',
            requestStep: 'details',
            requestDate: 1499558400,
            requestHeadText: 'Дилер сякой-то',
            requestBasicInfo: [{
                title: 'Ежемес. платеж',
                text: '29 000 руб.',
            }, {
                title: 'Срок',
                text: '24 месяца',
            }, {
                title: 'Автомобиль',
                text: 'Tiguan R-Line Executive',
            },],
            requestSpecTableInfo: [{
                title: 'Банк',
                text: 'Русфинанс Банк',
                link: false,
            }, {
                title: 'Тип программы',
                text: 'Кредит на приобретение автомобиля',
                link: '#',
            },
                {
                    title: 'Стоимость автомобиля',
                    text: '1 300 000 руб.',
                    link: false,
                },
                {
                    title: 'Первоначальный взнос',
                    text: '600 000 руб.',
                    link: false,
                }, {
                    title: 'Срок кредитования',
                    text: '29 000 руб.',
                    link: false,
                },
                {
                    title: 'Первоначальный взнос',
                    text: '24 месяца',
                    link: false,
                }]
        },
        requestHistory: [{
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }, {
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }],
    },
    /*chat*/
    /**/
    /*trade-in*/
    1987612345: {
        requestId: 1987612345,
        requestOwnDealer: 'd00007',
        requestInfo: {
            requestType: 'tradein',
            requestStep: 'initial',
            requestStepStatus: 'unresolved',
            requestDate: 1491696000,
            requestHeadText: 'Дилер',
        }
    },
    21987612345: {
        requestId: 21987612345,
        requestOwnDealer: 'd00007',
        requestInfo: {
            requestType: 'tradein',
            requestStep: 'preEstimation',
            requestStepStatus: 'unresolved',
            requestDate: 1491696000,
            requestHeadText: 'Дилер',
            requestBasicInfo: [{
                title: 'Выкуп',
                text: '400 000 руб.',
            }, {
                title: 'Скидка на покупку',
                text: '30 000 руб.',
            },],
            requestSpecTableInfo: [
                {
                    title: 'Марка',
                    text: 'Volkswagen',
                    link: false,
                }, {
                    title: 'Модель',
                    text: 'Passat',
                    link: false,
                },
                {
                    title: 'Год выпуска',
                    text: '2015',
                    link: false,
                },
                {
                    title: 'Трансмиссия',
                    text: 'АКПП',
                    link: false,
                }, {
                    title: 'Пробег, км',
                    text: '200 000',
                    link: false,
                },
                {
                    title: 'Мощность л/c',
                    text: '210',
                    link: false,
                },
                {
                    title: 'Объем',
                    text: '210',
                    link: false,
                }],
            requestSpecTablePreviews: ['https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png']
        },
        requestHistory: [{
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }, {
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }],
    },
        321987612345: {
            requestId: 321987612345,
            requestOwnDealer: 'd00007',
            requestInfo: {
                requestType: 'tradein',
                requestStep: 'preEstimation',
                requestStepStatus: 'success',
                requestDate: 1491696000,
                requestHeadText: 'Дилер',
                requestBasicInfo: [{
                    title: 'Выкуп',
                    text: '400 000 руб.',
                }, {
                    title: 'Скидка на покупку',
                    text: '30 000 руб.',
                },],
                requestSpecTableInfo: [
                    {
                        title: 'Марка',
                        text: 'Volkswagen',
                        link: false,
                    }, {
                        title: 'Модель',
                        text: 'Passat',
                        link: false,
                    },
                    {
                        title: 'Год выпуска',
                        text: '2015',
                        link: false,
                    },
                    {
                        title: 'Трансмиссия',
                        text: 'АКПП',
                        link: false,
                    }, {
                        title: 'Пробег, км',
                        text: '200 000',
                        link: false,
                    },
                    {
                        title: 'Мощность л/c',
                        text: '210',
                        link: false,
                    },
                    {
                        title: 'Объем',
                        text: '210',
                        link: false,
                    }],
                requestSpecTablePreviews: ['https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png']
            },
            requestHistory: [{
                requestHistoryItemDate: '20.10.17 14:00',
                requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
            }, {
                requestHistoryItemDate: '20.10.17 14:00',
                requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
            }],
        },
        421987612345: {
            requestId: 421987612345,
            requestOwnDealer: 'd00007',
            requestInfo: {
                requestType: 'tradein',
                requestStep: 'preEstimation',
                requestStepStatus: 'fail',
                requestDate: 1491696000,
                requestHeadText: 'Дилер',
                requestBasicInfo: [{
                    title: 'Выкуп',
                    text: '400 000 руб.',
                }, {
                    title: 'Скидка на покупку',
                    text: '30 000 руб.',
                },],
                requestSpecTableInfo: [
                    {
                        title: 'Марка',
                        text: 'Volkswagen',
                        link: false,
                    }, {
                        title: 'Модель',
                        text: 'Passat',
                        link: false,
                    },
                    {
                        title: 'Год выпуска',
                        text: '2015',
                        link: false,
                    },
                    {
                        title: 'Трансмиссия',
                        text: 'АКПП',
                        link: false,
                    }, {
                        title: 'Пробег, км',
                        text: '200 000',
                        link: false,
                    },
                    {
                        title: 'Мощность л/c',
                        text: '210',
                        link: false,
                    },
                    {
                        title: 'Объем',
                        text: '210',
                        link: false,
                    }],
                requestSpecTablePreviews: ['https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png']
            },
            requestHistory: [{
                requestHistoryItemDate: '20.10.17 14:00',
                requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
            }, {
                requestHistoryItemDate: '20.10.17 14:00',
                requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
            }],
        },
    5421987612345: {
        requestId: 5421987612345,
        requestOwnDealer: 'd00007',
        requestInfo: {
            requestType: 'tradein',
            requestStep: 'instrumentalEstimation',
            requestStepStatus: 'unresolved',
            requestDate: 1491696000,
            requestHeadText: 'Дилер',
            requestBasicInfo: [{
                title: 'Дата и время',
                text: '24 сентября, 12:00',
            }],
            requestSpecTableInfo: [
                {
                    title: 'Марка',
                    text: 'Volkswagen',
                    link: false,
                }, {
                    title: 'Модель',
                    text: 'Passat',
                    link: false,
                },
                {
                    title: 'Год выпуска',
                    text: '2015',
                    link: false,
                },
                {
                    title: 'Трансмиссия',
                    text: 'АКПП',
                    link: false,
                }, {
                    title: 'Пробег, км',
                    text: '200 000',
                    link: false,
                },
                {
                    title: 'Мощность л/c',
                    text: '210',
                    link: false,
                },
                {
                    title: 'Объем',
                    text: '210',
                    link: false,
                }],
            requestSpecTablePreviews: ['https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png']
        },
        requestHistory: [{
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }, {
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }],
    },
    4219876123453: {
        requestId: 4219876123453,
        requestOwnDealer: 'd00007',
        requestInfo: {
            requestType: 'tradein',
            requestStep: 'instrumentalEstimation',
            requestStepStatus: 'success',
            requestDate: 1491696000,
            requestHeadText: 'Дилер',
            requestBasicInfo: [{
                title: 'Дата и время',
                text: '24 сентября, 12:00',
            }],
            requestSpecTableInfo: [
                {
                    title: 'Марка',
                    text: 'Volkswagen',
                    link: false,
                }, {
                    title: 'Модель',
                    text: 'Passat',
                    link: false,
                },
                {
                    title: 'Год выпуска',
                    text: '2015',
                    link: false,
                },
                {
                    title: 'Трансмиссия',
                    text: 'АКПП',
                    link: false,
                }, {
                    title: 'Пробег, км',
                    text: '200 000',
                    link: false,
                },
                {
                    title: 'Мощность л/c',
                    text: '210',
                    link: false,
                },
                {
                    title: 'Объем',
                    text: '210',
                    link: false,
                }],
            requestSpecTablePreviews: ['https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png']
        },
        requestHistory: [{
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }, {
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }],
    },
    42198761234532: {
        requestId: 42198761234532,
        requestOwnDealer: 'd00007',
        requestInfo: {
            requestType: 'tradein',
            requestStep: 'instrumentalEstimation',
            requestStepStatus: 'fail',
            requestDate: 1491696000,
            requestHeadText: 'Дилер',
            requestBasicInfo: [{
                title: 'Дата и время',
                text: '24 сентября, 12:00',
            }],
            requestSpecTableInfo: [
                {
                    title: 'Марка',
                    text: 'Volkswagen',
                    link: false,
                }, {
                    title: 'Модель',
                    text: 'Passat',
                    link: false,
                },
                {
                    title: 'Год выпуска',
                    text: '2015',
                    link: false,
                },
                {
                    title: 'Трансмиссия',
                    text: 'АКПП',
                    link: false,
                }, {
                    title: 'Пробег, км',
                    text: '200 000',
                    link: false,
                },
                {
                    title: 'Мощность л/c',
                    text: '210',
                    link: false,
                },
                {
                    title: 'Объем',
                    text: '210',
                    link: false,
                }],
            requestSpecTablePreviews: ['https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png']
        },
        requestHistory: [{
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }, {
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }],
    },
    321987612343135: {
        requestId: 321987612343135,
        requestOwnDealer: 'd00007',
        requestInfo: {
            requestType: 'tradein',
            requestStep: 'finalEstimation',
            requestStepStatus: 'unresolved',
            requestDate: 1491696000,
            requestHeadText: 'Дилер',
            requestBasicInfo: [{
                title: 'Выкуп',
                text: '420 000 руб.',
            }, {
                title: 'Скидка на покупку',
                text: '40 000 руб.',
            },],
            requestSpecTableInfo: [
                {
                    title: 'Марка',
                    text: 'Volkswagen',
                    link: false,
                }, {
                    title: 'Модель',
                    text: 'Passat',
                    link: false,
                },
                {
                    title: 'Год выпуска',
                    text: '2015',
                    link: false,
                },
                {
                    title: 'Трансмиссия',
                    text: 'АКПП',
                    link: false,
                }, {
                    title: 'Пробег, км',
                    text: '200 000',
                    link: false,
                },
                {
                    title: 'Мощность л/c',
                    text: '210',
                    link: false,
                },
                {
                    title: 'Объем',
                    text: '210',
                    link: false,
                }],
            requestSpecTablePreviews: ['https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png']
        },
        requestHistory: [{
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }, {
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }],
    },
    32331987612345: {
        requestId: 32331987612345,
        requestOwnDealer: 'd00007',
        requestInfo: {
            requestType: 'tradein',
            requestStep: 'finalEstimation',
            requestStepStatus: 'success',
            requestDate: 1491696000,
            requestHeadText: 'Дилер',
            requestBasicInfo: [{
                title: 'Выкуп',
                text: '420 000 руб.',
            }, {
                title: 'Скидка на покупку',
                text: '40 000 руб.',
            },],
            requestSpecTableInfo: [
                {
                    title: 'Марка',
                    text: 'Volkswagen',
                    link: false,
                }, {
                    title: 'Модель',
                    text: 'Passat',
                    link: false,
                },
                {
                    title: 'Год выпуска',
                    text: '2015',
                    link: false,
                },
                {
                    title: 'Трансмиссия',
                    text: 'АКПП',
                    link: false,
                }, {
                    title: 'Пробег, км',
                    text: '200 000',
                    link: false,
                },
                {
                    title: 'Мощность л/c',
                    text: '210',
                    link: false,
                },
                {
                    title: 'Объем',
                    text: '210',
                    link: false,
                }],
            requestSpecTablePreviews: ['https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png']
        },
        requestHistory: [{
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }, {
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }],
    },
    32198327612345: {
        requestId: 32198327612345,
        requestOwnDealer: 'd00007',
        requestInfo: {
            requestType: 'tradein',
            requestStep: 'finalEstimation',
            requestStepStatus: 'fail',
            requestDate: 1491696000,
            requestHeadText: 'Дилер',
            requestBasicInfo: [{
                title: 'Выкуп',
                text: '420 000 руб.',
            }, {
                title: 'Скидка на покупку',
                text: '40 000 руб.',
            },],
            requestSpecTableInfo: [
                {
                    title: 'Марка',
                    text: 'Volkswagen',
                    link: false,
                }, {
                    title: 'Модель',
                    text: 'Passat',
                    link: false,
                },
                {
                    title: 'Год выпуска',
                    text: '2015',
                    link: false,
                },
                {
                    title: 'Трансмиссия',
                    text: 'АКПП',
                    link: false,
                }, {
                    title: 'Пробег, км',
                    text: '200 000',
                    link: false,
                },
                {
                    title: 'Мощность л/c',
                    text: '210',
                    link: false,
                },
                {
                    title: 'Объем',
                    text: '210',
                    link: false,
                }],
            requestSpecTablePreviews: ['https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Aspect-ratio-16x9.svg/2000px-Aspect-ratio-16x9.svg.png']
        },
        requestHistory: [{
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }, {
            requestHistoryItemDate: '20.10.17 14:00',
            requestHistoryItemText: 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!',
        }],
    },
    /*offers*/
    100000: {
        requestId: 100000,
        requestOwnDealer: 'd00008',
        requestInfo: {
            requestType: 'offer',
            requestStep: 1,
            requestStepStatus: 'unresolved',
            requestDate: 1489017600,
            requestHeadText: 'Дилер',
            requestBasicInfo: '',
        },
    },
    /*tags*/
    5122345: {
        requestId: 5122345,
        requestOwnDealer: 'd00009',
        requestInfo: {
            requestType: 'tags',
            requestDate: 1489017600,
            requestHeadText: 'Дилер',
            requestTagsAdded: ['Jetta', 'Новый автомобиль']
        },
    },
}


export let offerList = {
    '123': {
        offerCreationTime: 1501166089,
        offerId: '123',
        offerOwnDealer: 'd00008',
        offerOwnRequest: '100000',
        offerBookingStatus: 'dealer',
        offerBookingDate: 'бронь до 29.09.17',
        offerModelName: 'Passat Alltrack1',
        offerPrice: 1903781,
        offerDiscountPrice: 2000900,
        offerPriceTooltipDetails: [
            {
                title: 'Бонус за трейд-ин',
                text: 100000,
                href: false
            },
            {
                title: 'Скидка дилера',
                text: 50000,
                href: false
            },
            {
                title: 'Семейный автомобиль',
                text: 50000,
                href: '#34'
            }
        ],
        offerImgSrc: 'https://cars.volkswagen.ru/upload/resize_cache/iblock/1c9/1300_500_140cd750bba9870f18aada2478b24840a/polo.png',
        offerUserDecisionStatus: 'fail',
        offerTags: ['pineApple'],
        belowTheFoldShown: false,
    },
    '1234': {
        offerCreationTime: 1501252489,
        offerId: '1234',
        offerOwnDealer: 'd00008',
        offerOwnRequest: '100000',
        offerBookingStatus: 'warehouse',
        offerBookingDate: 'бронь до 29.09.17',
        offerModelName: 'Passat Alltrack2',
        offerPrice: 2303780,
        offerDiscountPrice: 2000900,
        offerPriceTooltipDetails: [
            {
                title: 'Бонус за трейд-ин',
                text: 100000,
                href: false
            },
            {
                title: 'Скидка дилера',
                text: 50000,
                href: false
            },
            {
                title: 'Семейный автомобиль',
                text: 50000,
                href: '#34'
            }
        ],
        offerImgSrc: 'https://cars.volkswagen.ru/upload/resize_cache/iblock/1c9/1300_500_140cd750bba9870f18aada2478b24840a/polo.png',
        offerUserDecisionStatus: 'unresolved',
        offerTags: ['grapes', 'goa'],
        belowTheFoldShown: false,
    },
    '123455': {
        offerCreationTime: 1501252489,
        offerId: '123455',
        offerOwnDealer: 'd00008',
        offerOwnRequest: '100000',
        offerBookingStatus: 'warehouse',
        offerBookingDate: 'бронь до 29.09.17',
        offerModelName: 'Passat Alltrack66',
        offerPrice: 1903782,
        offerDiscountPrice: 2000900,
        offerPriceTooltipDetails: [
            {
                title: 'Бонус за трейд-ин',
                text: 100000,
                href: false
            },
            {
                title: 'Скидка дилера',
                text: 50000,
                href: false
            },
            {
                title: 'Семейный автомобиль',
                text: 50000,
                href: '#34'
            }
        ],
        offerImgSrc: 'https://cars.volkswagen.ru/upload/resize_cache/iblock/1c9/1300_500_140cd750bba9870f18aada2478b24840a/polo.png',
        offerUserDecisionStatus: 'unresolved',
        offerTags: ['apple', 'grapes', 'goa', 'banana'],
        belowTheFoldShown: false,
    },
    '12345': {
        offerCreationTime: 1501252489,
        offerId: '12345',
        offerOwnDealer: '1w1q12345',
        offerOwnRequest: '12345',
        offerBookingStatus: 'warehouse-pending',
        offerBookingDate: 'бронь до 29.09.17',
        offerModelName: 'Passat Alltrack3',
        offerPrice: 1903783,
        offerDiscountPrice: 2000900,
        offerPriceTooltipDetails: [
            {
                title: 'Бонус за трейд-ин',
                text: 100000,
                href: false
            },
            {
                title: 'Скидка дилера',
                text: 50000,
                href: false
            },
            {
                title: 'Семейный автомобиль',
                text: 50000,
                href: '#34'
            }
        ],
        offerImgSrc: 'https://cars.volkswagen.ru/upload/resize_cache/iblock/1c9/1300_500_140cd750bba9870f18aada2478b24840a/polo.png',
        offerUserDecisionStatus: 'unresolved',
        offerTags: ['apple', 'mango'],
        belowTheFoldShown: false,
    },
    '123456': {
        offerCreationTime: 1501166089,
        offerId: '123456',
        offerOwnDealer: '1w1q12345',
        offerOwnRequest: '12345',
        offerBookingStatus: 'assembly',
        offerBookingDate: 'бронь до 29.09.17',
        offerModelName: 'Passat Alltrack4',
        offerPrice: 1903784,
        offerDiscountPrice: 2000900,
        offerPriceTooltipDetails: [
            {
                title: 'Бонус за трейд-ин',
                text: 100000,
                href: false
            },
            {
                title: 'Скидка дилера',
                text: 50000,
                href: false
            },
            {
                title: 'Семейный автомобиль',
                text: 50000,
                href: '#34'
            }
        ],
        offerImgSrc: 'https://cars.volkswagen.ru/upload/resize_cache/iblock/1c9/1300_500_140cd750bba9870f18aada2478b24840a/polo.png',
        offerUserDecisionStatus: 'unresolved',
        offerTags: ['banana', 'mango'],
        belowTheFoldShown: false,
    },
}

export const tagList = [
    {label: 'Banana', value: 'banana', isSelected: true,},
    {label: 'Apple', value: 'apple'},
    {label: 'Mango', value: 'mango'},
    {label: 'Goa', value: 'goa', isSelected: true},
    {label: 'Grapes', value: 'grapes'},
    {label: 'Pine Apple', value: 'pineApple'},
]