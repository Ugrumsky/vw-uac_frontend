/**
 * Created by UFS on 3/11/2017.
 */
// Common packages
import path from 'path';

// Utility packages
import _sortBy from 'lodash/sortBy';
import _map from 'lodash/map';
import glob from 'glob';



export default function (CONFIG) {
    return _sortBy(_map(glob.sync( CONFIG.paths.pages + '**/*.@(pug|html)', {
        cwd: path.resolve(__dirname, '../../../')
    }), function (filePath) {
        const parsedPath = path.parse(filePath);
        const publicPath = parsedPath.dir.replace(CONFIG.paths.pages, '');
        //console.log(parsedPath);
        return {
            name: publicPath,
            publicPath: CONFIG.paths.publicPath + CONFIG.paths.assets.templates + publicPath + '.html',
            filename: `${CONFIG.paths.assets.templates}${publicPath}.html`,
            template: filePath,
            chunks: ['commons', parsedPath.name],
        };
    }), (v) => v.name);
}
