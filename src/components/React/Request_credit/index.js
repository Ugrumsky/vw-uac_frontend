import React from 'react';

import EventCard from '../EventCard/index';
import EventCardAboveTheFold from '../EventCard_AboveTheFold/index';
import EventCardBelowTheFold from '../EventCard_BelowTheFold/index';
import EventCardBasicInfo from '../EventCard_BasicInfo/index';
import EventCardSpecsTable from '../EventCard_SpecsTable/index';
import EventCardHistory from '../EventCard_History/index';
import EventCardBorderSeparator from '../EventCard_BorderSeparator/index';

const RequestCredit = (props) => {

    const requestInfo = props.requestData.requestInfo;
    const requestHistory = props.requestData.requestHistory;

    let content;

    switch (requestInfo.requestStep) {
        case 'initial': {
            content = 'Спасибо! Ваша заявка на кредит будет обработана в самое ближайшее время. Наши специалисты свяжутся с вами!'
        break;
        }
        case 'details' : {
            content = <EventCard>
                <EventCardAboveTheFold cardTitle="Вы интересовались кредитом">
                    <EventCardBasicInfo basicInfo={requestInfo.requestBasicInfo}/>
                </EventCardAboveTheFold>
                <EventCardBelowTheFold>
                    <EventCardSpecsTable specItems={requestInfo.requestSpecTableInfo} previewItems={false}/>
                    <EventCardBorderSeparator />
                    <EventCardHistory historyData={requestHistory}/>
                </EventCardBelowTheFold>
            </EventCard>
            break;
        }
    }


    return (
        <div>{content}</div>

    );
}

export default RequestCredit;





