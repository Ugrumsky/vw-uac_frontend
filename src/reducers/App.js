import {createStore, combineReducers, applyMiddleware} from 'redux';
import Main from './Main.js';
import thunk from 'redux-thunk';
import RequestList from './RequestList';
import OfferList from './OfferList';
import DealerList from './DealerList';
import TagFilter from './TagFilter';

const rootReducer = combineReducers({RequestList, OfferList, DealerList, Main, TagFilter});

export const defaultStore = createStore(rootReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(), applyMiddleware(thunk));
