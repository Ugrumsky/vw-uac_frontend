
var submenuTogglerDots = $('#js_toggle_submenu--dots');
var topBarNav = $('#js_top-bar__nav-list');
var topBarNavSub = $('#js_top-bar__nav-list--sub');
var topBarNavItems = topBarNav.find('.top-bar__nav-item');
var topBarNavSubItems = topBarNavSub.find('.top-bar__nav-item');
var topBarNavSubItems = topBarNavSub.find('.top-bar__nav-item');
var topBarRightPart = $('#js_top-bar__right-part');

function hideTopBarSubmenus(anchorItem){
    if (anchorItem){
        anchorItem.removeClass('is_active');
        $('#js_' + anchorItem.data('targettotoggle')).removeClass('is_visible');
    }
    else{
        $('.js_toggle_submenu').removeClass('is_active');
        $('.js_top-bar_submenu').removeClass('is_visible');
    }
}
function showTopBarSubmenu(anchorItem){
    var target = $('#js_' + anchorItem.data('targettotoggle'));
    anchorItem.addClass('is_active');
    
    target.addClass('is_visible');
}

$(document).on('click','.js_toggle_submenu',function(e){
    var $this = $(this);

    if ($this.hasClass('is_active')){
        hideTopBarSubmenus();
    } else {
        hideTopBarSubmenus();
        showTopBarSubmenu($this);
    }

    e.preventDefault();
})



function renderTopBarNav(){
    topBarNavItems.each(function(){
        if($(this).offset().left + $(this).width() + submenuTogglerDots.width() + 50 > topBarRightPart.offset().left){
            $(this).addClass('is_hidden');
            topBarNavSubItems.eq($(this).index()).addClass('is_visible');
        }
        else{
            $(this).removeClass('is_hidden');
            topBarNavSubItems.eq($(this).index()).removeClass('is_visible');
        }
    })
}
function renderDots(){
	var lastNotHidden = topBarNavItems.not('.is_hidden').last();
	var lastNotHiddenLeft;
	var lastNotHiddenWidth;
	if (lastNotHidden.length > 0){
	 	lastNotHiddenLeft = lastNotHidden.position().left
		lastNotHiddenWidth = lastNotHidden.width();
	   	submenuTogglerDots.css({
	    	'left': lastNotHiddenLeft + lastNotHiddenWidth + 30//double-margin
	    })
	   }
    if( topBarNavItems.filter('.is_hidden').length == 0){
        submenuTogglerDots.removeClass('is_visible');
        hideTopBarSubmenus(submenuTogglerDots)
    }
    else{
         submenuTogglerDots.addClass('is_visible');
    }
}

$(window).on('resize', function( ){
	if(window.matchMedia("(min-width: 480px)").matches){
	    renderTopBarNav();
	   	renderDots();
	}
})

$(function(){
    renderTopBarNav()
    //headerNav.addClass('is_visible');
    var currentActiveNavItem = topBarNav.find('.is_active');
    console.log(currentActiveNavItem)
    if (currentActiveNavItem.length > 0){
        topBarNavSubItems.eq(currentActiveNavItem.index()).addClass('is_active');
    }
    renderDots()
})
